/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  async headers() {
    return [
      {
        source: '/login',
        headers: [
          {
            key: 'detail',
            value: 'asdasd'
          }
        ]
      }
    ]
  },
  images: {
    domains: ['lh3.googleusercontent.com', '127.0.0.1', 'localhost', '35.198.36.42', 'avatars.githubusercontent.com']
  }
}

module.exports = nextConfig
