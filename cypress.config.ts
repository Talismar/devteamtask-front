import axios, { AxiosError } from 'axios'
import { defineConfig } from 'cypress'
import { dotenv } from 'cypress-plugin-dotenv'

export default defineConfig({
  e2e: {
    baseUrl: 'http://localhost:3000',
    viewportWidth: 1920,
    viewportHeight: 1080,
    experimentalInteractiveRunEvents: true,
    setupNodeEvents(on, config) {
      const newConfig = dotenv(config)
      const { USER_TOKEN_FOR_RESET_DB } = newConfig.env

      on('task', {
        async reset_database_by_service(service: 'PROJECT' | 'USER') {
          let BASE_URL = 'http://127.0.0.1:8001'

          if (service === 'USER') {
            BASE_URL = 'http://127.0.0.1:8002'
          }

          return axios
            .post(
              `${BASE_URL}/reset_database?secret=${USER_TOKEN_FOR_RESET_DB}`
            )
            .then((response) => {
              const { status, data } = response

              return { status, data }
            })
            .catch((error: AxiosError) => {
              const statusCode = error.response?.status
              const data = error.response?.data
              console.log(error)

              return {
                status: statusCode,
                data,
              }
            })
        },
        pause(ms) {
          return new Promise((resolve) => {
            // tasks should not resolve with undefined
            setTimeout(() => resolve(null), ms)
          })
        },
      })

      on('before:run', () => {
        // console.log('BEFORE RUN')
      })

      on('after:run', (result) => {
        // console.log('AFTER RUN', result)
      })

      return newConfig
    },
  },
})
