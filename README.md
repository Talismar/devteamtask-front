<h1 align="center">
  <br>
  <a href="http://35.198.36.42/"><img src="devTeamTaskLogo.png" alt="DevTeamTask" width="200" style="border-radius: 2px"></a>
  <br>
  DevTeamTask
  <br>
</h1>

<h4 align="center">A task management software for development team built on top of <a href="https://nextjs.org/" target="_blank">NextJS</a>.</h4>

<p align="center">
  <a href="https://nextjs.org/">
    <img src="https://img.shields.io/badge/next.js-000000?style=for-the-badge&logo=nextdotjs&logoColor=white&style=flat-square"
         alt="Logo Cookiecutter">
  </a>
  <a href="https://www.typescriptlang.org/"><img src="https://shields.io/badge/TypeScript-3178C6?logo=TypeScript&logoColor=FFF&style=flat-square"></a>
  <a href="https://nodejs.org/en"><img src="https://img.shields.io/badge/nodejs-green"></a></a>
  <a href="https://nodejs.org/en"><img src="https://img.shields.io/badge/StitchesJS-black"></a>
</p>

<p align="center">
  <a href="#key-features">Key Features</a> •
  <a href="#how-to-use">How To Use</a> •
  <a href="#credits">Credits</a> •
  <a href="#contact">Contact</a>
</p>

![screenshot](devteamtaskgif.gif)

## Key Features

- User control
  - With support for sending an email if the user wants to reset the password outside the application
- Socal authentication
  - Supports: Google and Github
- Project management
- Project status management
- Project tag management
- Project sprint management
- Project task management
- Event note management
- App routes secured with `JWT` authorization
- Encryption for sensitive information

## How To Use

To clone and run this application, you'll need [Git](https://git-scm.com), [Node 18.17.0](https://www.python.org/download/) and [Yarn](https://classic.yarnpkg.com/lang/en/docs/install/#debian-stable). From your command line:

```bash
# Clone this repository
$ git clone https://gitlab.com/Talismar/devteamtask-front.git

# Go into the repository
$ cd devteamtask-front

# Install dependency with yarn
$ yarn install

# Install dependency with npm if you prefer
$ npm install

# set environment variables according to .env.EXEMPLA

# run the project
$ yarn build && yarn start
```

> **Note**
> If you're using Linux Bash for Windows, [see this guide]().

## Credits

This software uses the following open source packages:

- [ReactJS](https://react.dev/)
- [NextJS](https://nextjs.org/)
- [React Sortablejs](https://www.npmjs.com/package/react-sortablejs)
- [Typescript](https://www.typescriptlang.org/)
- [NextAuth](https://next-auth.js.org/)
- [React Markdown Editor Lite](https://www.npmjs.com/package/react-markdown-editor-lite)
- [Stitches](https://stitches.dev/)
- [Axios](https://axios-http.com/)
- [Apexcharts](https://apexcharts.com/)
- [Formik](https://formik.org/)
- [Yup](https://github.com/jquense/yup)
- [React Toastify](https://fkhadra.github.io/react-toastify/introduction)

## You may also like...

- [DevTeamTask-CLI](https://github.com/Talismar/devteamtask-cli) - A task management software for development team
- [DevTeamTask-Backend](https://github.com/Talismar/devteamtask-back) - A task management software for development team
- [DevTeamTask-Figma](https://www.figma.com/file/93HpbAt9qbG8F41DQERB37/DevTeamTask-%7C-PI-02?type=design&mode=design&t=DoRfhoPhuCJtCq7Q-1) - A task management software for development team

## Contact

> GitHub [Talismar](https://github.com/Talismar)
> Facebook [Tali Fer Costa](https://www.facebook.com/tali.fercosta)
> Gmail [talismar788.una@gmail.com]()
