export function compareArray(x: object, y: object): boolean {
  return JSON.stringify(x) === JSON.stringify(y)
}
