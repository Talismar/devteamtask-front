import { GetServerSidePropsContext, NextApiResponse } from 'next'
import { setCookie } from 'nookies'

export function setAuthCookiesByContext(
  data: { access: string; refresh: string },
  ctx?: GetServerSidePropsContext
) {
  setCookie(ctx, '@dtt.accessToken', data.access, {
    maxAge: 30 * 24 * 60 * 60,
    path: '/',
  })
  setCookie(ctx, '@dtt.refreshToken', data.refresh, {
    maxAge: 30 * 24 * 60 * 60,
    path: '/',
  })
}

export function setAuthCookiesByResponse(
  data: { access: string; refresh: string; successMessage?: string },
  res: NextApiResponse
) {
  setCookie({ res }, '@dtt.accessToken', data.access, {
    maxAge: 30 * 24 * 60 * 60,
    path: '/',
    sameSite: false,
  })
  setCookie({ res }, '@dtt.refreshToken', data.refresh, {
    maxAge: 30 * 24 * 60 * 60,
    path: '/',
    sameSite: false,
  })
  setCookie({ res }, 'success', data.successMessage || '', {
    path: '/',
    sameSite: false,
  })
}
