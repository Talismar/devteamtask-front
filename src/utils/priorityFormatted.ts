export const priorityFormatted = [
  {
    label: "Low",
    value: 1
  },
  {
    label: "Medium",
    value: 2
  },
  {
    label: "High",
    value: 3
  }
]