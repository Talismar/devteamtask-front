import { config } from '@/styles'

export default function getPriorityColor(logicOperation: boolean) {
  return logicOperation
    ? config.theme.colors.red100
    : config.theme.colors.gray100
}
