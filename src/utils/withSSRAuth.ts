import {
  GetServerSideProps,
  GetServerSidePropsContext,
  GetServerSidePropsResult,
} from 'next'
import { parseCookies } from 'nookies'
// import { validateUserGroups } from './validateUserGroups'
import clearAllCookies from './clearAllCookies'

type WithSSRAuthOptionsType = {
  groups: string[]
}

/**
 * Validate the pages that the user needs to be authenticated
 * @param fn getServerSideProps function
 * @param url url redirect url
 * @param options object with groups
 * @returns getServerSideProps function or redirect to login page
 */
export function withSSRAuth<P extends Record<string, unknown>>(
  fn: GetServerSideProps<P>,
  url: '/login' | '/patient-access' | 'redirectToLogin',
  options: WithSSRAuthOptionsType,
) {
  return async (
    ctx: GetServerSidePropsContext,
  ): Promise<GetServerSidePropsResult<P>> => {
    const { thalocanTokenAccess: access, permissions } = parseCookies(ctx)

    if (!access || !permissions) {
      clearAllCookies(ctx)

      if (url === 'redirectToLogin') {
        const { form } = ctx.query

        return {
          redirect: {
            destination: `/patient-access?redirect=/form/${form}`,
            permanent: false,
          },
        }
      }

      return {
        redirect: {
          destination: url,
          permanent: false,
        },
      }
    }

    // crypto-js | NodeJS
    // const bytes = AES.decrypt(
    //   permissions,
    //   process.env.NEXT_PUBLIC_PASSWORD_CRYPTOJS_AES as string,
    // )

    // try {
    //   const cookiesGroups = JSON.parse(bytes.toString(enc.Utf8))

    //   const userHasValidPermissions = validateUserGroups({
    //     userGroups: cookiesGroups,
    //     groups: options.groups,
    //   })

    //   if (!userHasValidPermissions) {
    //     return {
    //       notFound: true,
    //     }
    //   }
    // } catch (error) {
    //   // Error: Malformed UTF-8 data

    //   clearAllCookies(ctx)

    //   return {
    //     redirect: {
    //       destination: url,
    //       permanent: false,
    //     },
    //   }
    // }

    // ctx.req.headers.referer

    return await fn(ctx)
  }
}
