import {
  GetServerSideProps,
  GetServerSidePropsContext,
  GetServerSidePropsResult
} from 'next'
import { destroyCookie, parseCookies } from 'nookies'

// Usamos esta funcão por pessoas que nao estao logadas
export function withSSRGuest<P extends Record<string, unknown>>(
  fn: GetServerSideProps<P>,
  url:
    | '/investigator/manage-team/'
    | '/investigator/manage-patients/'
    | '/form'
    | 'this'
) {
  return async (
    ctx: GetServerSidePropsContext
  ): Promise<GetServerSidePropsResult<P>> => {
    const { thalocanTokenAccess: access, permissions } = parseCookies(ctx)

    if (access && url !== 'this') {
      return {
        redirect: {
          destination: url,
          permanent: false
        }
      }
    }

    return await fn(ctx)
  }
}
