import { SprintTypes } from '@/@types/projects'

export default function findActiveSprint(sprint: SprintTypes[]) {
  return sprint.find((item) => item.state === 'IN PROGRESS')
}
