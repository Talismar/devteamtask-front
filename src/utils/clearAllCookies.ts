import { GetServerSidePropsContext } from 'next'
import { destroyCookie } from 'nookies'

const cookieNames = ['@dtt.accessToken', '@dtt.refreshToken']

/**
 * Clear all cookies
 * @param ctx Context to get the cookies
 * @returns void
 */
function clearAllCookies(ctx: GetServerSidePropsContext | undefined) {
  cookieNames.forEach((cookieName) => {
    destroyCookie(ctx, cookieName, {
      path: '/',
    })
  })
}

export default clearAllCookies
