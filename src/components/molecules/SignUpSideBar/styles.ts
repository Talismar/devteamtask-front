import { styled } from '@/styles'

export const SideBarContainer = styled('aside', {
  backgroundColor: '$blueDark400',
  width: 'min-content',
  padding: '2.625rem',
  borderRadius: 8,
  minHeight: 'calc(100vh - 2rem)',
  margin: '1rem',
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'flex-start',
})

export const WrappedLogo = styled('div', {
  display: 'flex',
  gap: '0.75rem',

  '& h2': {
    fontWeight: 400,
    color: '$white100',
  },
})

export const ProgressContainer = styled('section', {
  display: 'flex',
  gap: '0.5rem',
  flexDirection: 'column',
  marginTop: '6.125rem',

  '& div.vertical-line': {
    width: 2,
    height: 59,
    backgroundColor: '$white100',
    marginLeft: 13,
    borderRadius: 4,
  },
})

export const ProgressItem = styled('div', {
  color: '$white100',
  display: 'flex',
  gap: '1.25rem',

  '& .text strong': {
    fontWeight: 500,
  },

  '& .text p': {
    fontWeight: 300,
  },

  variants: {
    status: {
      inactive: {
        '& div.status-icon': {
          width: 21.13,
          height: 21.13,
          margin: 2,
          backgroundColor: '$white100',
          boxShadow: 'inset $blueDark400 0px 0px 0px 6px;',
          border: '2px solid white',
          borderRadius: '50%',
        },
      },
      active: {
        '& div.status-icon': {
          width: 21.13,
          height: 21.13,
          margin: 2,
          boxShadow: 'inset #FFF 0px 0px 0px 7px;',
          borderRadius: '50%',
        },
      },
    },
  },
})

export const SideBarDescription = styled('h3', {
  fontWeight: 400,
  fontSize: '1.5rem',
  textAlign: 'center',
  backgroundColor: '$blueDark300',
  padding: '1rem',
  color: '$white100',
  minWidth: '20.6875rem',
  borderRadius: 8,
  marginTop: 'auto',

  '& + .copyright': {
    marginTop: '6.8125rem',
    width: '100%',
    display: 'flex',
    justifyContent: 'center',
  },

  '& + .copyright span': {
    color: '$white100',
    fontSize: '300',
    lineHeight: '1.2rem',
    verticalAlign: 'top',
    marginLeft: '0.5rem',
  },
})
