import { CheckCircle, Copyright } from '@phosphor-icons/react'
import Image from 'next/image'
import LogoWhiteSmSvg from '../../../../public/icons/logo-white-sm.svg'
import { theme } from '../../../styles'

import {
  ProgressContainer,
  ProgressItem,
  SideBarContainer,
  SideBarDescription,
  WrappedLogo,
} from './styles'

interface SignUpSideBarProps {
  activeItem: 'detailsForm' | 'passwordForm'
  completedDetails: boolean
  goToPage(index: number): void
}

function SignUpSideBar({
  activeItem,
  completedDetails,
  goToPage,
}: SignUpSideBarProps) {
  function handleGoToPage(index: number): void {
    goToPage(index)
  }

  return (
    <SideBarContainer>
      <WrappedLogo>
        <Image src={LogoWhiteSmSvg} alt="DevTeamTask Icons" />
        <h2>DevTeamTask</h2>
      </WrappedLogo>
      <ProgressContainer>
        <ProgressItem
          status={activeItem === 'detailsForm' ? 'active' : 'inactive'}
          onClick={() => handleGoToPage(0)}
          css={{ cursor: 'pointer' }}
        >
          {completedDetails ? (
            <CheckCircle weight="fill" size={26} />
          ) : (
            <div className="status-icon" />
          )}

          <div className="text">
            <strong>Your Details</strong>
            <p>Provide your personal information</p>
          </div>
        </ProgressItem>

        <div className="vertical-line" />

        <ProgressItem
          status={activeItem === 'passwordForm' ? 'active' : 'inactive'}
        >
          <div className="status-icon" />

          <div className="text">
            <strong>Choose a password</strong>
            <p>Choose a secure password</p>
          </div>
        </ProgressItem>

        {/* <div className="vertical-line" /> */}

        {/* <ProgressItem status="inactive">
          <div className="status-icon" />

          <div className="text">
            <strong>Job title</strong>
            <p>Choose your job title</p>
          </div>
        </ProgressItem> */}
      </ProgressContainer>

      <SideBarDescription>
        A task management tool for software development team.
      </SideBarDescription>

      <section className="copyright">
        <Copyright size={20} color={String(theme.colors.white100)} />
        <span>DevTeamTask 2023</span>
      </section>
    </SideBarContainer>
  )
}

export default SignUpSideBar
