import Avatar from '@/components/atoms/Avatar'
import { AuthContext } from '@/contexts/AuthContext'
import { useSignOut } from '@/hooks/useSignOut'
import { config, styled } from '@/styles'
import { GearSix, SignOut } from '@phosphor-icons/react'
import { useRouter } from 'next/router'
import { useContext } from 'react'

function ModalProfile() {
  const router = useRouter()
  const { user, getProfileImage } = useContext(AuthContext)

  const handleGoToUserSettings = () => {
    router.push('/user/settings')
  }

  const handleLogOut = () => {
    useSignOut('/')
    // signOut({ callbackUrl: '/' })
    router.push('/')
  }

  return (
    <DivContainer>
      <DivHeaderWrapper>
        <h2>DevTeamTask</h2>

        <DivPhotoAndInfoWrapper>
          <Avatar
            src={getProfileImage()}
            size="lg"
            firstLetter={user.name.charAt(0).toUpperCase()}
          />

          <DivHeaderTextWrapper>
            <span>{user.name}</span>
            <span>{user.email}</span>
          </DivHeaderTextWrapper>
        </DivPhotoAndInfoWrapper>
      </DivHeaderWrapper>

      <DivContentWrapper>
        <DivItemContentWrapper onClick={handleGoToUserSettings}>
          <GearSix size={24} color={config.theme.colors.gray800} /> Settings
        </DivItemContentWrapper>
        <DivItemContentWrapper onClick={handleLogOut} color="red400">
          <SignOut size={24} /> Log out
        </DivItemContentWrapper>
      </DivContentWrapper>
    </DivContainer>
  )
}

export default ModalProfile

const DivContainer = styled('div', {
  position: 'absolute',
  listStyle: 'none',
  right: 0,
  borderRadius: 8,
  overflow: 'hidden',
  zIndex: 10,
  minWidth: 380,
  backgroundColor: '$white100',
  display: 'flex',
  flexDirection: 'column',
})

const DivContentWrapper = styled('div', {
  display: 'flex',
  flexDirection: 'column',
  gap: '$4',
  padding: '$2',
})

const DivItemContentWrapper = styled('div', {
  display: 'flex',
  alignItems: 'center',
  gap: '$2',
  padding: '$2',
  cursor: 'pointer',

  variants: {
    color: {
      red400: {
        color: '$red400',
      },
    },
  },

  '&:hover': {
    backgroundColor: '$blue200',
    borderRadius: 8,
    border: '1px dashed $blue400',
  },
})

const DivHeaderWrapper = styled('div', {
  backgroundColor: '$blue400',
  padding: '$4',
  color: '$white100',
  display: 'flex',
  flexDirection: 'column',
  gap: '$4',
})

const DivPhotoAndInfoWrapper = styled('div', {
  display: 'grid',
  gridTemplateColumns: 'auto 1fr',
  gap: '$4',
  alignItems: 'center',
})

const DivHeaderTextWrapper = styled('div', {
  justifySelf: 'flex-end',

  '& span:first-child': {
    display: 'block',
    marginBottom: '$4',
    fontWeight: 500,
  },
})
