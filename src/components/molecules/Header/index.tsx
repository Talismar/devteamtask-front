import { AuthContext } from '@/contexts/AuthContext'
import { styled, theme } from '@/styles'
import { BellSimple } from '@phosphor-icons/react'
import Image from 'next/image'
import { useContext, useState } from 'react'
import ModalNotification from '../ModalNotification'
import ModalProfile from '../ModalProfile'

interface HeaderProps {
  title: string
}

function Header({ title }: HeaderProps) {
  const { user, notifications, getProfileImage } = useContext(AuthContext)
  const user_avatar = getProfileImage()

  const [isActiveCollapse, setIsActiveCollapse] = useState({
    profile: false,
    notification: false,
  })

  const handleActiveCollapse = (collapse: 'profile' | 'notification') => {
    const newState = { profile: false, notification: false }
    newState[collapse] = true
    setIsActiveCollapse(newState)
  }

  const handleCloseCollapse = () => {
    setIsActiveCollapse({ profile: false, notification: false })
  }

  return (
    <WrappedHeader>
      <h1>{title}</h1>

      <Group>
        <StyledCollapseWrapper>
          <DivBellIconWrapper>
            <BellSimple
              size={32}
              color={String(theme.colors.gray400)}
              onClick={() => handleActiveCollapse('notification')}
              cursor="pointer"
            />
            {notifications.length > 0 && <p>{notifications.length}</p>}
          </DivBellIconWrapper>
          {isActiveCollapse.notification && <ModalNotification />}
        </StyledCollapseWrapper>
        <StyledCollapseWrapper>
          <CirclePhoto
            className="photo"
            onClick={() => handleActiveCollapse('profile')}
          >
            <span>
              {user_avatar ? (
                <Image src={user_avatar} fill alt="Avatar" />
              ) : (
                user.name?.charAt(0).toUpperCase()
              )}
            </span>
          </CirclePhoto>
          {isActiveCollapse.profile && <ModalProfile />}
        </StyledCollapseWrapper>
      </Group>
      {isActiveCollapse.notification && (
        <div onClick={handleCloseCollapse} className="overlay" />
      )}
      {isActiveCollapse.profile && (
        <div onClick={handleCloseCollapse} className="overlay" />
      )}
    </WrappedHeader>
  )
}

export default Header

const Group = styled('section', {
  display: 'flex',
  gap: '1.5rem',
  alignItems: 'center',
})

const WrappedHeader = styled('header', {
  display: 'flex',
  justifyContent: 'space-between',
  alignItems: 'center',
  padding: '0.75rem 1.5rem',
  borderBottom: '1px solid $gray100',
})

const CirclePhoto = styled('div', {
  backgroundColor: '$gray300',
  border: '1px solid $orange400',
  borderRadius: '50%',
  overflow: 'hidden',
  height: '3rem',
  width: '3rem',
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
  position: 'relative',
  cursor: 'pointer',

  '& span': {
    fontSize: '1.5rem',
    color: '$gray800',
  },

  '& ul.collapse-profile': {
    position: 'absolute',
  },
})

const StyledCollapseWrapper = styled('div', {
  position: 'relative',
})

const DivBellIconWrapper = styled('div', {
  '& p': {
    position: 'absolute',
    top: -10,
    right: -10,
    backgroundColor: '$white100',
    color: '$red400',
    fontWeight: 700,
    padding: '2px 8px',
    borderRadius: '50%',
  },
})
