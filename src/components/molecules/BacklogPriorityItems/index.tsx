import { styled } from '@/styles'
import getPriorityColor from '@/utils/getPriorityColor'
import { Circle } from '@phosphor-icons/react'

interface BacklogPriorityItemsProps {
  priorityNumber: number
}

function BacklogPriorityItems({ priorityNumber }: BacklogPriorityItemsProps) {
  return (
    <DivWrapper>
      <Circle
        weight="fill"
        size={12}
        color={getPriorityColor(priorityNumber >= 1)}
      />
      <Circle
        weight="fill"
        size={12}
        color={getPriorityColor(priorityNumber >= 2)}
      />
      <Circle
        weight="fill"
        size={12}
        color={getPriorityColor(priorityNumber === 3)}
      />
    </DivWrapper>
  )
}

export default BacklogPriorityItems

const DivWrapper = styled('div', {
  display: 'flex',
  gap: '0.25rem',
})
