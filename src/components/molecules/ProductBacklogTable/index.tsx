import { ProjectDetailsTypes } from '@/@types/projects'
import { TaskTypes } from '@/@types/tasks'
import Avatar from '@/components/atoms/Avatar'
import Table from '@/components/organisms/Table'
import { config, styled } from '@/styles'
import formatDate from '@/utils/dateFormatted'
import { priorityFormatted } from '@/utils/priorityFormatted'
import {
  ArrowFatUp,
  CalendarBlank,
  ClockClockwise,
  NotePencil,
  Tag,
  Trash,
} from '@phosphor-icons/react'
import Image from 'next/image'
import BacklogPriorityItems from '../BacklogPriorityItems'

import ModalConfirmation from '@/components/organisms/ModalConfirmation'
import { useState } from 'react'
import emtpyTableImage from '../../../../public/imgs/empty-table-image.jpg'

interface ProductBacklogTableProps {
  projectData: ProjectDetailsTypes
  changeTaskBacklog(
    taskId: number,
    to: 'product_backlog' | 'sprint_backlog'
  ): void
  deleteTask(taskId: number): void
  openDialog(
    dialog: 'newTask' | 'newSprint' | 'finishSprint',
    action?: 'update',
    data?: TaskTypes
  ): void
}

function ProductBacklogTable({
  projectData,
  changeTaskBacklog,
  openDialog,
  deleteTask,
}: ProductBacklogTableProps) {
  const tasks_set = projectData.project_data.tasks.filter(
    (item) => item.sprint_id === null && item
  )
  const [dialogDelete, setDialogDelete] = useState({ state: false, taskId: 0 })

  return tasks_set.length > 0 ? (
    <>
      <ModalConfirmation
        isOpen={dialogDelete.state}
        title="Delete Task"
        subTitle="Do you really want to delete this task now?"
        handleClose={() => setDialogDelete({ state: false, taskId: 0 })}
        handleSubmit={() => {
          deleteTask(dialogDelete.taskId)
          setDialogDelete({ state: false, taskId: 0 })
        }}
      />

      <Table.Root css={{ marginBottom: '6rem' }}>
        <Table.Thead>
          <Table.Tr backgroundColor="primary">
            <Table.Th css={{ width: '32px' }} />
            <Table.Th>Tags</Table.Th>
            <Table.Th>Task name</Table.Th>
            <Table.Th>Assingment</Table.Th>
            <Table.Th>Status</Table.Th>
            <Table.Th>Priority</Table.Th>
            <Table.Th />
          </Table.Tr>
        </Table.Thead>
        <Table.Tbody>
          {tasks_set.map((item) => {
            const assigned_to = projectData.users.find(
              (user) => user.id === item.assigned_to_user_id
            )

            return (
              <Table.Tr key={item.id} withBorderBottom>
                <Table.Td
                  title="Move to product backlog"
                  cursorPointer
                  onClick={() => changeTaskBacklog(item.id, 'sprint_backlog')}
                >
                  <ArrowFatUp
                    weight="fill"
                    color={config.theme.colors.orange400}
                  />
                </Table.Td>
                <Table.Td>
                  <DivTagContainer css={{ gap: '$2' }}>
                    {item.tags?.map((tag) => (
                      <Span key={tag.id} css={{ backgroundColor: '$blue200' }}>
                        {tag.name?.toLocaleUpperCase()}
                      </Span>
                    ))}
                  </DivTagContainer>
                  {item.tags?.length === 0 && (
                    <DivTagContainer
                      css={{
                        color: '$orange400',
                        backgroundColor: '$orange200',
                        width: 'min-content',
                        paddingLeft: '$2',
                      }}
                    >
                      <Tag />
                      <span>No Tag</span>
                    </DivTagContainer>
                  )}
                </Table.Td>
                <Table.Td flexColumn>
                  <DivWithFlexRow>
                    <CalendarBlank
                      size={20}
                      color={config.theme.colors.gray600}
                    />
                    <Span css={{ color: '$gray700' }}>
                      {formatDate(item.created_at)}
                    </Span>
                  </DivWithFlexRow>
                  <Span css={{ color: '$gray800' }}>
                    <strong>{item.id} </strong>- {item.name}
                  </Span>
                </Table.Td>
                <Table.Td>
                  <Div
                    css={{ display: 'flex', alignItems: 'center', gap: '$2' }}
                  >
                    {assigned_to ? (
                      <>
                        <Avatar
                          src={
                            assigned_to.avatar_url &&
                            `${
                              process.env.NEXT_PUBLIC_DTT_SERVICE_USER_URL
                            }/media/images/users/${assigned_to!.avatar_url}`
                          }
                          size="sm"
                          firstLetter={assigned_to.name}
                        />
                        <span>{assigned_to.name}</span>
                      </>
                    ) : (
                      <Span
                        css={{
                          backgroundColor: '$orange200',
                          color: '$orange400',
                          fontWeight: 500,
                          padding: '$2',
                          borderRadius: 4,
                        }}
                      >
                        Unassigned
                      </Span>
                    )}
                  </Div>
                </Table.Td>
                <Table.Td>
                  <DivWithFlexRow>
                    <ClockClockwise
                      size={20}
                      color={config.theme.colors.gray600}
                    />
                    <Span css={{ color: '$gray700' }}>
                      {formatDate(item.updated_at)}
                    </Span>
                  </DivWithFlexRow>

                  <Span
                    css={{
                      fontWeight: 500,
                      color: '$blue400',
                    }}
                  >
                    {item.status.name}
                  </Span>
                </Table.Td>
                <Table.Td>
                  <Div
                    css={{
                      display: 'flex',
                      flexDirection: 'column',
                      gap: '$2',
                    }}
                  >
                    <Span css={{ color: '$gray800' }}>
                      {
                        priorityFormatted.find(
                          (priority) => priority.value === item.priority
                        )?.label
                      }
                    </Span>

                    <BacklogPriorityItems priorityNumber={item.priority} />
                  </Div>
                </Table.Td>
                <Table.Td>
                  <NotePencil
                    size={24}
                    onClick={() => openDialog('newTask', 'update', item)}
                    color={config.theme.colors.gray800}
                    weight="bold"
                    cursor="pointer"
                  />
                  <Trash
                    weight="fill"
                    size={24}
                    color={config.theme.colors.red400}
                    onClick={() =>
                      setDialogDelete({ state: true, taskId: item.id })
                    }
                    cursor="pointer"
                  />
                </Table.Td>
              </Table.Tr>
            )
          })}
        </Table.Tbody>
      </Table.Root>
    </>
  ) : (
    <Div css={{ display: 'flex', justifyContent: 'center' }}>
      <Image src={emtpyTableImage} alt="Empty Table Image" width={320} />
    </Div>
  )
}

export default ProductBacklogTable

const DivTagContainer = styled('div', {
  display: 'flex',
  alignItems: 'center',
  borderRadius: 4,
  color: '$blue400',

  '& span': {
    padding: '$2',
    borderRadius: 4,
    fontWeight: 500,
  },
})

const DivWithFlexRow = styled('div', {
  display: 'flex',
  alignItems: 'center',
  gap: '0.25rem',
})

const Span = styled('span', {})
const Div = styled('div', {})
