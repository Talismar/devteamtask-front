import { styled } from '@/styles'
import { ReactElement } from 'react'

interface BackLogHeaderProps {
  title: string
  subTitle: string
  sprintDescription?: string
  button: ReactElement
}

function BackLogHeader({
  title,
  subTitle,
  sprintDescription,
  button,
}: BackLogHeaderProps) {
  return (
    <DivContainer>
      <DivLeftContentWrapper>
        <H3Title>{title}</H3Title>
        <DivTextWrapper>
          <p>{subTitle}</p>
          {sprintDescription && (
            <span title={sprintDescription}>{sprintDescription}</span>
          )}
        </DivTextWrapper>
      </DivLeftContentWrapper>
      {button}
    </DivContainer>
  )
}

export default BackLogHeader

const DivContainer = styled('div', {
  display: 'flex',
  justifyContent: 'space-between',
  alignItems: 'flex-start',

  button: {
    minWidth: 'max-content',
  },
})

const DivLeftContentWrapper = styled('div', {})

const H3Title = styled('h3', {})

const DivTextWrapper = styled('div', {
  display: 'grid',

  span: {
    overflow: 'hidden',
    whiteSpace: 'nowrap',
    textOverflow: 'ellipsis',
  },

  // '& span': {
  //   overflow: 'hidden',
  //   position: 'relative',
  //   display: 'inline-block',
  //   textOverflow: 'ellipsis',
  // },
})
