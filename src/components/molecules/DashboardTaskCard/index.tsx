import { styled } from '@/styles'
import { ReactElement } from 'react'

interface DashboardTaskCardProps {
  title: string
  value: string | number
  color: 'green' | 'gray'
  icon: ReactElement
}

function DashboardTaskCard({
  title,
  value,
  icon,
  color,
}: DashboardTaskCardProps) {
  return (
    <DivCardWrapper backgroundColor={color}>
      <DivCardHeader>
        <p>{title}</p>
        {icon}
      </DivCardHeader>
      <strong>{value}</strong>
    </DivCardWrapper>
  )
}

export default DashboardTaskCard

const DivCardWrapper = styled('div', {
  display: 'flex',
  flexDirection: 'column',
  justifyContent: 'space-between',
  gap: '$4',
  padding: '$4',
  borderRadius: 8,
  // minWidth: '19.8rem',
  width: '100%',
  height: '10rem',
  fontWeight: 500,

  '& strong': {
    fontSize: '2.25rem',
  },

  variants: {
    backgroundColor: {
      green: {
        backgroundColor: '$green400',
        color: '$white100',
      },
      gray: {
        backgroundColor: '$gray100',

        '& p': {
          color: '$gray400',
        },

        '& strong': {
          color: '$gray600',
        },
      },
    },
  },
})

const DivCardHeader = styled('div', {
  display: 'flex',
  justifyContent: 'space-between',
  alignItems: 'center',
})
