import Avatar from '@/components/atoms/Avatar'
import { styled } from '@/styles'
import { CloudArrowUp } from '@phosphor-icons/react'

interface FileInputWithPreviewProps {
  labelText: string
  src?: string | File | null
  name: string
  formikSetFieldValue(
    field: string,
    value: unknown,
    shouldValidate?: boolean | undefined
  ): Promise<void> | Promise<unknown>
}

function FileInputWithPreview({
  labelText,
  src,
  name,
  formikSetFieldValue,
}: FileInputWithPreviewProps) {
  return (
    <StyledContainer>
      <Avatar size="lg" src={src} />
      <label htmlFor={name}>
        {' '}
        <CloudArrowUp size={24} weight="bold" /> {labelText}
      </label>
      <input
        type="file"
        name={name}
        id={name}
        onChange={(e) => {
          if (e.target.files) {
            formikSetFieldValue(name, e.target.files[0])
          }
        }}
      />
    </StyledContainer>
  )
}

export default FileInputWithPreview

const StyledContainer = styled('div', {
  display: 'flex',
  alignItems: 'flex-end',
  gap: '$6',

  '& label': {
    backgroundColor: '$blue200',
    display: 'flex',
    gap: '$2',
    alignItems: 'center',
    padding: '$2',
    borderRadius: '8px',
    fontWeight: 500,
    color: '$blue400',
    cursor: 'pointer',
  },

  '& input': {
    opacity: 0,
    position: 'absolute',
    zIndex: -1,
  },
})
