import { AuthContext } from '@/contexts/AuthContext'
import { styled } from '@/styles'
import { Check } from '@phosphor-icons/react'
import { useContext } from 'react'

function ModalNotification() {
  const { notifications, markNotificationAsRead, user } =
    useContext(AuthContext)

  return (
    <UlContainer>
      {user.notification_state &&
        notifications?.map((item) => (
          <LiWrapper key={item.id}>
            <DivItemHeaderWrapper>
              <strong>{item.title}</strong>
              <span>{item.description}</span>
            </DivItemHeaderWrapper>
            <DivIconWrapper
              title="Mark as read"
              onClick={() => markNotificationAsRead(item.id)}
            >
              <Check size={24} weight="bold" />
            </DivIconWrapper>
          </LiWrapper>
        ))}
      {(notifications.length === 0 || !user.notification_state) && (
        <li style={{ margin: 'auto' }}>
          {!user.notification_state
            ? 'You are not receiving notifications due to your notifications status settings being disabled'
            : "You don't have any notification yet"}
        </li>
      )}
    </UlContainer>
  )
}

export default ModalNotification

const UlContainer = styled('ul', {
  position: 'absolute',
  listStyle: 'none',
  right: 0,
  borderRadius: 8,
  overflow: 'hidden',
  zIndex: 10,
  minWidth: 380,
  backgroundColor: '$white100',
  padding: '$4',
  display: 'flex',
  flexDirection: 'column',
  gap: '$2',
})

const LiWrapper = styled('li', {
  display: 'flex',
  gap: '$4',
  justifyContent: 'space-between',
  alignItems: 'center',
  padding: '$2',
  borderWidth: 1,
  borderColor: '$blue400',
  borderStyle: 'dashed',
  bg: '$blue200',
  borderRadius: 8,
})

const DivItemHeaderWrapper = styled('div', {
  strong: {
    display: 'block',
  },
})

const DivIconWrapper = styled('div', {
  bg: '$green400',
  padding: '$2',
  borderRadius: 8,
  color: '$white100',
  cursor: 'pointer',
})
