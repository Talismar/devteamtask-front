import { styled } from "@/styles";
import React from "react";

interface LoadingSpinnerProps {
  size?: "sm" | "md" | "lg";
}

export default function LoadingSpinner({ size }: LoadingSpinnerProps) {
  return (
    <StyledSpinnerContainer >
      <StyledLoadingSpinner size={size} />
    </StyledSpinnerContainer>
  );
}

const StyledLoadingSpinner = styled("div", {
  borderRadius: "50%",
  animation: "spinner 1.5s linear infinite",

  variants: {
    size: {
      sm: {
        width: "50px",
        height: "50px",
        border: "5px solid $gray100",
        borderTop: "5px solid $blue400",
      },
      md: {
        width: "100px",
        height: "100px",
        border: "8px solid $gray100",
        borderTop: "8px solid $blue400",
      },
      lg: {
        width: "150px",
        height: "150px",
        border: "10px solid $gray100",
        borderTop: "10px solid $blue400",
      }
    }
  },

  defaultVariants: {
    size: "md"
  }
})

const StyledSpinnerContainer = styled("div", {
  display: "grid",
  justifyContent: "center",
  alignItems: "center",
  height: "350px",
});