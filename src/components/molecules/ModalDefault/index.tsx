import { config, styled } from '@/styles'
import { X } from '@phosphor-icons/react'
import { ReactNode } from 'react'

interface ModalDefaultProps {
  handleClose: () => void
  state: boolean
  title: string
  subTitle: string
  content: ReactNode
}

function ModalDefault({
  handleClose,
  state,
  title,
  subTitle,
  content,
}: ModalDefaultProps) {
  return (
    <>
      <StyledDialogContainer open={state}>
        <div>
          <StyledHeaderWrapped>
            <section>
              <h4>{title}</h4>
              <p>{subTitle}</p>
            </section>
            <X
              size={24}
              color={config.theme.colors.red400}
              onClick={handleClose}
            />
          </StyledHeaderWrapped>

          <main>{content}</main>
        </div>
      </StyledDialogContainer>

      {state && <div onClick={handleClose} className="overlay" />}
    </>
  )
}

export default ModalDefault

const StyledDialogContainer = styled('dialog', {
  backgroundColor: '$white100',
  border: 'none',
  padding: '1.5rem',
  borderRadius: '.5rem',
  position: 'fixed',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  zIndex: 10,
})

const StyledHeaderWrapped = styled('header', {
  display: 'flex',
  gap: '1rem',
  justifyContent: 'space-between',
  borderBlockEnd: '1px solid $gray100',
  marginBottom: '1.5rem',

  section: {
    marginBottom: '0.75rem',
  },

  '& section h4': {
    color: '$gray800',
    fontWeight: '500',
    marginBottom: '0.250rem',
  },

  '& section p': {
    color: '$gray600',
  },

  '& svg': {
    cursor: 'pointer',
  },
})
