import { theme } from '@/styles'
import { ChartLine, DiamondsFour } from '@phosphor-icons/react'
import Image from 'next/image'
import { useRouter } from 'next/router'
import logoWhiteSvg from '../../../../public/icons/logo-white-sm.svg'

import { NavRootList, WrappedSystemNavBar } from './styles'

interface SystemNavBarProps {
  pathActive: 'dashboard' | 'projects'
}

function SystemNavBar({ pathActive }: SystemNavBarProps) {
  const router = useRouter()

  function handleGo(path: typeof pathActive) {
    router.push(`/${path}`)
  }

  return (
    <WrappedSystemNavBar>
      <Image src={logoWhiteSvg} alt="" onClick={() => handleGo('dashboard')} />

      <nav>
        <NavRootList>
          <li
            className={pathActive === 'dashboard' ? 'active' : ''}
            onClick={() => handleGo('dashboard')}
          >
            <ChartLine
              size={24}
              color={String(theme.colors.white100)}
              weight="bold"
            />
            <span>Dashboard</span>
          </li>
          <li
            className={pathActive === 'projects' ? 'active' : ''}
            onClick={() => handleGo('projects')}
          >
            <DiamondsFour
              size={24}
              color={String(theme.colors.white100)}
              weight="bold"
            />
            <span>Projects</span>
          </li>
        </NavRootList>
      </nav>
    </WrappedSystemNavBar>
  )
}

export default SystemNavBar
