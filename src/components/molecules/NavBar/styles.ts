import { styled } from '@/styles'

export const WrappedSystemNavBar = styled('aside', {
  backgroundColor: '$blueDark400',
  padding: '1.5rem',

  '& > img': {
    marginBottom: '6.25rem',
    cursor: 'pointer',
  },

  variants: {
    color: {
      red: {
        backgroundColor: 'red',
      },
      green: {
        backgroundColor: 'green',
      },
    },
  },
})

export const InfoProjectSprint = styled('section', {
  color: '$white100',
  marginBottom: '2rem',

  h3: {
    marginBottom: '0.5rem',
    fontWeight: 'bold',
    fontSize: '1.5rem',
  },

  strong: {
    fontWeight: 500,
  },
})

export const WrappedProjectNavBar = styled('aside', {
  backgroundColor: '$blueDark400',
  padding: '1.5rem',

  '& > img': {
    marginBottom: '2rem',
    cursor: 'pointer',
  },
})

export const NavRootList = styled('ul', {
  display: 'flex',
  flexDirection: 'column',
  gap: '1.5rem',

  li: {
    display: 'flex',
    alignItems: 'center',
    gap: '0.5rem',
    minWidth: '13rem',
    padding: '0.5rem',
    cursor: 'pointer',

    borderRadius: 4,

    '&:hover': {
      boxShadow: 'inset $blueDark300 0 0 0 2px',
    },
  },

  'li span': {
    color: '$white100',
  },

  'li.active': {
    backgroundColor: '$blueDark300',

    span: {
      fontWeight: 500,
    },
  },

  'li.event-notes': {
    flexDirection: 'column',
    alignItems: 'flex-start',
  },

  'li.event-notes > .wrapped-item': {
    minWidth: '100%',
    alignItems: 'center',
    display: 'flex',
    justifyContent: 'space-between',

    span: {
      marginLeft: '0.5rem',
      marginRight: 'auto',
    },
  },
})

export const NavEventNotesList = styled(NavRootList, {
  gap: '0.75rem',
  paddingLeft: '2rem',
  marginTop: 8,

  li: {
    padding: 0,
    gap: '0.25rem',
  },

  span: {
    fontSize: '0.75rem',
  },
})
