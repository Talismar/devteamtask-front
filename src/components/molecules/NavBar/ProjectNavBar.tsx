import { AuthContext } from '@/contexts/AuthContext'
import { ProjectContext } from '@/contexts/ProjectContext'
import { theme } from '@/styles'
import {
  ArrowsCounterClockwise,
  CaretRight,
  Clock,
  GearFine,
  IconContext,
  Kanban,
  ListMagnifyingGlass,
  Notebook,
  SquaresFour,
  Table,
  UsersThree,
} from '@phosphor-icons/react'
import Image from 'next/image'
import { useRouter } from 'next/router'
import { useContext } from 'react'
import logoWhiteSvg from '../../../../public/icons/logo-white-sm.svg'
import {
  InfoProjectSprint,
  NavEventNotesList,
  NavRootList,
  WrappedProjectNavBar,
} from './styles'
import findActiveSprint from '@/utils/findActiveSprint'
import { toast } from 'react-toastify'

type SubPathEventNotesTypes = 'planning' | 'retrospective' | 'review' | 'daily'

interface ProjectNavBarProps {
  pathActive?:
    | 'boards'
    | 'backlog'
    | 'collaborators'
    | 'event-notes'
    | 'settings'
  projectId: string | number
}

function ProjectNavBar({
  pathActive = 'boards',
  projectId,
}: ProjectNavBarProps) {
  const { projectData, activeSprint } = useContext(ProjectContext)
  const { user } = useContext(AuthContext)
  const router = useRouter()

  function handleGo(path: typeof pathActive, subpath?: SubPathEventNotesTypes) {
    if (typeof subpath !== 'undefined' && typeof activeSprint === 'undefined') {
      toast.warn('Please create a sprint.')
    } else {
      router.push(
        `/projects/${projectId}/${path}/${
          subpath ? subpath + `?sprint_id=${activeSprint?.id ?? '-'}` : ''
        }`
      )
    }
  }

  function handleGoToDashboard() {
    router.push('/dashboard')
  }

  return (
    <IconContext.Provider
      value={{ color: String(theme.colors.white100), size: 24, weight: 'fill' }}
    >
      <WrappedProjectNavBar>
        <Image
          src={logoWhiteSvg}
          alt="Logo system"
          onClick={handleGoToDashboard}
        />

        <InfoProjectSprint>
          <h3>{projectData?.project_data.name}</h3>
          <strong>
            {findActiveSprint(projectData?.project_data.sprints ?? [])?.name ??
              ''}
          </strong>
        </InfoProjectSprint>

        <nav>
          <NavRootList>
            <li
              className={pathActive === 'boards' ? 'active' : ''}
              onClick={() => handleGo('boards')}
            >
              <Kanban />
              <span>Boards</span>
            </li>
            <li
              className={pathActive === 'backlog' ? 'active' : ''}
              onClick={() => handleGo('backlog')}
            >
              <Table />
              <span>Backlog</span>
            </li>
            {projectData?.project_data.leader_id === user.id ? (
              <>
                <li
                  className={pathActive === 'collaborators' ? 'active' : ''}
                  onClick={() => handleGo('collaborators')}
                >
                  <UsersThree />
                  <span>Collaborators</span>
                </li>
                <li
                  className={
                    pathActive === 'event-notes'
                      ? 'active event-notes'
                      : 'event-notes'
                  }
                >
                  <div className="wrapped-item">
                    <Clock />
                    <span>Event notes</span>

                    <CaretRight weight="bold" size={18} />
                  </div>
                  <NavEventNotesList>
                    <li onClick={() => handleGo('event-notes', 'planning')}>
                      <SquaresFour size={14} />
                      <span>Planning</span>
                    </li>
                    <li onClick={() => handleGo('event-notes', 'daily')}>
                      <Notebook size={14} />
                      <span>Daily</span>
                    </li>
                    <li onClick={() => handleGo('event-notes', 'review')}>
                      <ListMagnifyingGlass size={14} />
                      <span>Review</span>
                    </li>
                    <li
                      onClick={() => handleGo('event-notes', 'retrospective')}
                    >
                      <ArrowsCounterClockwise size={14} />
                      <span>Retrospective</span>
                    </li>
                  </NavEventNotesList>
                </li>
                <li
                  className={pathActive === 'settings' ? 'active' : ''}
                  onClick={() => handleGo('settings')}
                >
                  <GearFine />
                  <span>Settings</span>
                </li>
              </>
            ) : (
              <li
                className={pathActive === 'collaborators' ? 'active' : ''}
                onClick={() => handleGo('collaborators')}
              >
                <UsersThree />
                <span>Collaborators</span>
              </li>
            )}
          </NavRootList>
        </nav>
      </WrappedProjectNavBar>
    </IconContext.Provider>
  )
}

export default ProjectNavBar
