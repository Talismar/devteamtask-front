import { TagTypes } from '@/@types/projects'
import { UserTypes } from '@/@types/users'
import Avatar from '@/components/atoms/Avatar'
import Label from '@/components/atoms/Label'
import { config, styled } from '@/styles'
import { Tag, Trash } from '@phosphor-icons/react'

interface CardBoardProps {
  assingedTo?: UserTypes
  taskName: string
  taskDescription?: string
  taskId: number
  taskTags: TagTypes[]
  openDialogDeleteTask(taskId: number): void
}

function CardBoard({
  taskId,
  assingedTo,
  taskName,
  taskDescription,
  taskTags,
  openDialogDeleteTask,
}: CardBoardProps) {
  return (
    <StyledCardWrapped id={`task-${taskId}`}>
      <StyledCardHeader>
        <div className="info-author">
          <Avatar
            size="sm"
            src={
              assingedTo?.avatar_url &&
              `${process.env.NEXT_PUBLIC_DTT_SERVICE_USER_URL}/media/images/users/${assingedTo.avatar_url}`
            }
            firstLetter={assingedTo?.name.charAt(0).toUpperCase() ?? 'U'}
          />
          <h5>{assingedTo?.email ?? 'Unassigned'}</h5>
        </div>

        <div className="group-icons">
          <Trash
            size={20}
            color={config.theme.colors.gray500}
            onClick={() => openDialogDeleteTask(taskId)}
          />
        </div>
      </StyledCardHeader>

      <StyledCardContent>
        <strong>{taskName}</strong>
        <p>{taskDescription}</p>
      </StyledCardContent>

      <StyledCardFooter>
        {taskTags.length === 0 && (
          <Label
            css={{
              fontSize: '0.75rem',
              bg: '$orange200',
              borderRadius: 4,
              fontWeight: 'bold',
              px: '0.5rem',
              py: '0.3rem',
              color: '$orange400',
            }}
          >
            <Tag /> No Tag
          </Label>
        )}
        {taskTags.map((tag) => (
          <p key={tag.id}>{tag.name?.toUpperCase()}</p>
        ))}
      </StyledCardFooter>
    </StyledCardWrapped>
  )
}

export default CardBoard

const StyledCardWrapped = styled('div', {
  width: '22rem',
  padding: '1.5rem',
  borderRadius: 8,
  display: 'flex',
  flexDirection: 'column',
  gap: '0.625rem',
  boxShadow: 'rgba(71, 178, 228, 0.35) 0px 0 15px 4px',
  cursor: 'pointer',
  backgroundColor: '$white100',

  '&:active': {
    cursor: 'grabbing',
  },
})

const StyledCardHeader = styled('header', {
  display: 'grid',
  gridTemplateColumns: 'auto 1fr',
  justifyContent: 'space-between',
  justifyItems: 'end',
  alignItems: 'center',
  gap: '$2',

  '& div.info-author': {
    maxWidth: 280,
    display: 'flex',
    gap: '0.5rem',
    overflow: 'hidden',
    alignItems: 'center',
  },

  '& h5': {
    fontWeight: 400,
    fontSize: '0.875rem',
    overflow: 'hidden',
    textOverflow: 'ellipsis',
    whiteSpace: 'nowrap',
  },

  '& .group-icons': {
    display: 'flex',
    gap: '0.25rem',
  },
})

const StyledCardContent = styled('main', {})

const StyledCardFooter = styled('footer', {
  display: 'flex',
  gap: '0.625rem',

  '& p': {
    fontSize: '0.6875rem',
    fontWeight: 700,
    color: '$blue400',
    backgroundColor: '$blue200',
    padding: '0.3rem 0.5rem',
    borderRadius: 8,
  },
})
