import { styled } from '@/styles'
import { FormHTMLAttributes, ReactNode } from 'react'

interface PrimaryFormContainerProps
  extends FormHTMLAttributes<HTMLFormElement> {
  title: string
  children: ReactNode
}

function PrimaryFormContainer({
  title,
  children,
  ...rest
}: PrimaryFormContainerProps) {
  return (
    <StyledContainer {...rest}>
      <div className="header-wrapper">
        <h2>{title}</h2>
      </div>
      <div className="content-wrapper">{children}</div>
    </StyledContainer>
  )
}

export default PrimaryFormContainer

const StyledContainer = styled('form', {
  padding: '$4',
  border: '1px solid $gray100',
  borderRadius: 8,

  '& div.header-wrapper': {
    borderBottom: '1px solid $gray100',
    marginBottom: '1.5rem',
  },
  '& div.header-wrapper h2': {
    fontSize: '$5',
    fontWeight: 500,
    color: '$gray800',
    margin: '1rem 0',
  },
  '& .content-wrapper': {
    display: 'flex',
    flexDirection: 'column',
    gap: '$6',
  },
})
