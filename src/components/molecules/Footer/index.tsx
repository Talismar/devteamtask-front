import { styled } from '@/styles'
import {
  Copyright,
  FacebookLogo,
  GithubLogo,
  InstagramLogo,
  LinkedinLogo,
} from '@phosphor-icons/react'
import Image from 'next/image'
import Link from 'next/link'
import logoSvg from '../../../../public/icons/logo-blue-md.svg'

function Footer() {
  return (
    <StyledContainer>
      <StyledContainerSections>
        <Image src={logoSvg} alt="logo" />

        <div className="main-content">
          <StyledSectionWrapped>
            <h3>CONTACT</h3>

            <StyledSectionContact>
              <p>talismar788.una@gmail.com</p>
              <p>+55 83 9.9970-2836</p>
            </StyledSectionContact>
          </StyledSectionWrapped>

          <StyledSectionWrapped>
            <h3>ABOUT</h3>

            <StyledSectionAbout>
              <li>
                <Link href="#">Our story</Link>
              </li>
              <li>
                <Link href="#">Team</Link>
              </li>
              <li>
                <Link href="#">Collaboration</Link>
              </li>
            </StyledSectionAbout>
          </StyledSectionWrapped>

          <StyledSectionWrapped>
            <h3>SOCIAL ACCOUNT</h3>
            <StyledSectionSocialAccount>
              <li>
                <FacebookLogo size={24} weight="fill" />
                <a
                  href="https://www.facebook.com/tali.fercosta"
                  target="_blank"
                >
                  Facebook
                </a>
              </li>
              <li>
                <InstagramLogo size={24} weight="fill" />
                <Link href="#">Instagram</Link>
              </li>
              <li>
                <LinkedinLogo size={24} weight="fill" />
                <a
                  href="https://www.linkedin.com/in/talismar-fernandes-costa-941238232/"
                  target="_blank"
                >
                  Likedind
                </a>
              </li>
              <li>
                <GithubLogo size={24} weight="fill" />
                <a href="https://github.com/Talismar" target="_blank">
                  GitHub
                </a>
              </li>
            </StyledSectionSocialAccount>
          </StyledSectionWrapped>
        </div>

        <StyledInfoAuthorWrapped>
          <div className="line-horizontal" />

          <section>
            <Copyright size={20} weight="regular" />
            <p>2023 - Design and Code by Talismar Fernandes</p>
          </section>
        </StyledInfoAuthorWrapped>
      </StyledContainerSections>
    </StyledContainer>
  )
}

export default Footer

const StyledContainer = styled('footer', {
  display: 'flex',
  gap: '1.5rem',
  backgroundColor: '$gray800',
  flexDirection: 'column',
  padding: '1.5rem 1rem 1.5rem 1rem',
  color: '$white100',
})

const StyledContainerSections = styled('div', {
  display: 'flex',
  alignItems: 'flex-start',
  gap: '1.5rem',
  flexDirection: 'column',
  width: '1280px',
  maxWidth: '1280px',
  margin: 'auto',

  '& .main-content': {
    width: '100%',
    display: 'flex',
    justifyContent: 'space-between',
  },
})

const StyledSectionContact = styled('section', {})

const StyledSectionAbout = styled('ul', {
  listStyle: 'none',
  display: 'flex',
  flexDirection: 'column',
  gap: '0.5em',

  '& li a': {
    color: '$white100',
    fontWeight: 300,
  },
})

const StyledSectionSocialAccount = styled(StyledSectionAbout, {
  '& li': {
    display: 'flex',
    alignItems: 'center',
    gap: '0.5rem',
  },

  '& li svg': {
    color: '$blue400',
  },
})

const StyledSectionWrapped = styled('div', {
  display: 'flex',
  flexDirection: 'column',
  gap: '1rem',

  '& h3': {
    fontSize: '1.5rem',
    fontWeight: 400,
  },
})

const StyledInfoAuthorWrapped = styled('section', {
  width: '100%',

  '& div.line-horizontal': {
    width: '100%',
    height: '1px',
    backgroundColor: '$gray100',
  },

  '& section': {
    marginTop: '1rem',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    gap: '0.5rem',
  },

  '& p': {
    fontWeight: 300,
  },
})
