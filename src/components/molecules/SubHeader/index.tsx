import { styled } from '@/styles'
import { ReactNode } from 'react'

interface SubHeaderProps {
  complement?: ReactNode
  title: string
  paragraph: string
}

function SubHeader({ title, paragraph, complement }: SubHeaderProps) {
  return (
    <StyledSubHeaderContainer>
      <section>
        <h1>{title}</h1>
        <p>{paragraph}</p>
      </section>

      {complement}
    </StyledSubHeaderContainer>
  )
}

export default SubHeader

const StyledSubHeaderContainer = styled('header', {
  display: 'flex',
  justifyContent: 'space-between',
  marginBottom: '2rem',
})
