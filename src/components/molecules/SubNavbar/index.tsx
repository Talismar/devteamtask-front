import { config, styled } from '@/styles'
import { Icon, IconContext } from '@phosphor-icons/react'

type NavItemsTypes = {
  name: string
  icon: Icon
  isActive: boolean
  onClick(): void
}

interface SubNavbarProps {
  title: string
  subTitle: string
  navItems: NavItemsTypes[]
}

function SubNavbar({ title, subTitle, navItems }: SubNavbarProps) {
  return (
    <StyledContainer>
      <StyledHeaderWrapper>
        <h3>{title}</h3>
        <span>{subTitle}</span>
      </StyledHeaderWrapper>

      <StyledNavWrapper>
        <IconContext.Provider
          value={{
            size: 20,
            weight: 'fill',
            color: config.theme.colors.gray600,
          }}
        >
          <ul>
            {navItems?.map((item) => (
              <li
                className={`${item.isActive && 'isActive'}`}
                onClick={item.onClick}
                key={item.name}
              >
                {<item.icon />}
                {item.name}
              </li>
            ))}
          </ul>
        </IconContext.Provider>
      </StyledNavWrapper>
    </StyledContainer>
  )
}

export default SubNavbar

const StyledContainer = styled('aside', {
  display: 'flex',
  flexDirection: 'column',
  gap: '$8',
})

const StyledHeaderWrapper = styled('header', {
  '& h3': {
    fontWeight: 500,
    color: '$gray800',
    marginBottom: '$2',
    fontSize: '$5',
  },

  '& span': {
    color: '$gray500',
    marginBottom: '$2',
  },
})

const StyledNavWrapper = styled('nav', {
  '& ul': {
    display: 'flex',
    flexDirection: 'column',
    gap: '$4',
  },

  '& ul li': {
    padding: '$2',
    display: 'flex',
    gap: '$2',
    alignItems: 'center',
    borderRadius: '4px',
    color: '$gray600',
    textDecoration: 'none',
    cursor: 'pointer',
  },

  '& ul li.isActive': {
    backgroundColor: '$gray100',
  },
})
