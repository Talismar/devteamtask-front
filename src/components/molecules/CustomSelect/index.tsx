import { styled } from '@/styles'
import React from 'react'
import Select, {
  ActionMeta,
  MultiValue,
  PropsValue,
  SingleValue,
} from 'react-select'

type OptionTypes = {
  value: string | number
  label: string
}

interface CustomSelectProps {
  labelText: string
  options: OptionTypes[]
  name: string
  value?: PropsValue<OptionTypes> | undefined
  isMulti?: boolean
  defaultValue?: OptionTypes
  onBlur?: React.FocusEventHandler<HTMLInputElement> | undefined
  onChange?:
    | ((
        newValue: MultiValue<OptionTypes> | SingleValue<OptionTypes>,
        actionMeta: ActionMeta<OptionTypes>
      ) => void)
    | undefined
}

function CustomSelect({
  labelText,
  isMulti,
  options,
  defaultValue,
  ...rest
}: CustomSelectProps) {
  return (
    <StyledSelectWrapped>
      <span>{labelText}</span>
      <Select
        options={options}
        isMulti={isMulti}
        isSearchable
        isClearable
        defaultValue={defaultValue}
        styles={{
          control: (baseStyles, state) => ({
            ...baseStyles,
            padding: '0.4rem',
            fontSize: '0.875rem',

            borderColor: state.isFocused ? '#47b2e4' : '#E6E6E6',
            boxShadow: state.isFocused ? '0 0 0 1px #47b2e4' : '',
          }),
          container: (baseStyles, state) => ({
            ...baseStyles,
          }),
          multiValue: (styles, { data }) => {
            return {
              ...styles,
              backgroundColor: '#E6E6E6',
            }
          },
          // valueContainer: (base, props) => ({
          //   ...base,
          // }),
          // singleValue: (base, props) => ({
          //   ...base,
          //   color: '#666666',
          //   width: 'min-content',
          //   backgroundColor: '#E6E6E6',
          // }),
        }}
        {...rest}
      />
    </StyledSelectWrapped>
  )
}

export default CustomSelect

const StyledSelectWrapped = styled('label', {
  display: 'flex',
  flexDirection: 'column',
  gap: '0.5rem',

  '& > span': {
    fontWeight: '500',
    color: '$gray700',
  },
})
