import Button from '@/components/atoms/Button'
import { styled } from '@/styles'
import { Plus, X } from '@phosphor-icons/react'
import { InputHTMLAttributes } from 'react'

interface TagsInputProps extends InputHTMLAttributes<HTMLInputElement> {
  items: { id: number; name: string }[]
  handleDelete(itemId: number): void
  handleNewTag(): Promise<void>
}

function TagsInput({
  items,
  handleDelete,
  handleNewTag,
  ...rest
}: TagsInputProps) {
  return (
    <DivContainer>
      <Label htmlFor="tag">
        <span>Tags</span>
        <DivItemsWrapper>
          {items.map((item) => (
            <p key={item.id} onClick={() => handleDelete(item.id)}>
              <X size={18} />
              {item.name}
            </p>
          ))}
          <input type="text" name="tag" id="tag" {...rest} />
        </DivItemsWrapper>
      </Label>

      <Button
        size="sm"
        withIcon
        type="button"
        css={{ bg: '$blue200', color: '$blue400' }}
        onClick={handleNewTag}
      >
        <Plus size={24} />
        New Tag
      </Button>
    </DivContainer>
  )
}

export default TagsInput

const DivContainer = styled('div', {
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'flex-start',
  gap: '1rem',
})

const Label = styled('label', {
  width: '100%',
  display: 'flex',
  flexDirection: 'column',
  gap: '0.5rem',

  '& span': {
    color: '$gray700',
    fontWeight: 500,
  },
})

const DivItemsWrapper = styled('div', {
  display: 'flex',
  gap: '$2',
  padding: '$4',
  borderRadius: 4,
  flexWrap: 'wrap',
  border: '1px solid $gray100',

  '& p': {
    color: '$gray500',
    backgroundColor: '$gray100',
    padding: '0.25rem',
    display: 'flex',
    alignItems: 'center',
    gap: '0.5rem',
    fontSize: '0.875rem',
    textTransform: 'uppercase',
    borderRadius: 4,
  },

  '& input': {
    maxWidth: 100,
  },
})
