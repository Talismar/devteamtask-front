import { ProjectDetailsTypes, ProjectTypes } from '@/@types/projects'
import Button from '@/components/atoms/Button'
import Input from '@/components/atoms/Input'
import Textarea from '@/components/atoms/Textarea'
import ModalDefault from '@/components/molecules/ModalDefault'
import { ProjectContext } from '@/contexts/ProjectContext'
import { SprintService } from '@/services/SprintService'
import { styled } from '@/styles'
import { Check } from '@phosphor-icons/react'
import { useFormik } from 'formik'
import { useContext } from 'react'
import { toast } from 'react-toastify'
import * as Yup from 'yup'

interface ModalNewSprintProps {
  handleClose(): void
  isOpen: boolean
  projectData: ProjectDetailsTypes
}

const formSchema = Yup.object().shape({
  name: Yup.string().required('Required.'),
  description: Yup.string().required(),
})

type FormikValuesTypes = Yup.InferType<typeof formSchema>

function ModalNewSprint({
  handleClose,
  isOpen,
  projectData,
}: ModalNewSprintProps) {
  const { setProjectData } = useContext(ProjectContext)
  const formik = useFormik<FormikValuesTypes>({
    initialValues: {
      name: '',
      description: '',
    },
    validationSchema: formSchema,
    onSubmit: async (values, { resetForm }) => {
      const sprintService = new SprintService()
      try {
        const response = await sprintService.create({
          ...values,
          project_id: projectData.project_data.id,
        })

        if (response.status === 201) {
          toast.success('Sprint created successfully.')
          resetForm()
          setProjectData(
            (prev) =>
              prev && {
                ...prev,
                project_data: {
                  ...prev.project_data,
                  sprints: [...prev.project_data.sprints, response.data],
                },
              }
          )
          handleClose()
        }
      } catch (error) {
        toast.error('Error creating sprint')
      }
    },
  })

  return (
    <ModalDefault
      title="Create sprint"
      subTitle="Describe the sprint in an easy way"
      state={isOpen}
      handleClose={handleClose}
      content={
        <StyledForm onSubmit={formik.handleSubmit}>
          <Input
            id="name"
            labelText="Sprint name"
            type="text"
            name="name"
            placeholder="Enter the sprint name"
            value={formik.values.name}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            errorMessage={
              formik.touched.name && formik.errors.name
                ? formik.errors.name
                : ''
            }
          />

          <Textarea
            id="description"
            labelText="Sprint description"
            name="description"
            rows={8}
            placeholder="Enter the sprint description"
            value={formik.values.description}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            errorMessage={
              formik.touched.description && formik.errors.description
                ? formik.errors.description
                : ''
            }
          />

          <Button
            type="submit"
            withIcon
            css={{ color: '$white100', backgroundColor: '$blue400' }}
          >
            <Check size={16} /> Create sprint
          </Button>
        </StyledForm>
      }
    />
  )
}

export default ModalNewSprint

const StyledForm = styled('form', {
  display: 'flex',
  gap: '1.5rem',
  flexDirection: 'column',
  minWidth: '511px',
})
