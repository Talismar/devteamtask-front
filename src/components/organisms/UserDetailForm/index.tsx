import Button from '@/components/atoms/Button'
import Input from '@/components/atoms/Input'
import FileInputWithPreview from '@/components/molecules/FileInputWithPreview'
import PrimaryFormContainer from '@/components/molecules/PrimaryForm'
import { AuthContext } from '@/contexts/AuthContext'
import { UserService } from '@/services/UserService'
import { styled } from '@/styles'
import userDetailFormValidation, {
  UserDetailFormFormikTypes,
} from '@/validation/userDetailFormValidation'
import { Check, Key } from '@phosphor-icons/react'
import { useContext, useEffect, useState } from 'react'
import { toast } from 'react-toastify'

function UserDetailForm() {
  const { user, setUser, getProfileImage } = useContext(AuthContext)
  const { formik, handleUpdateInitialValues } = userDetailFormValidation({
    handleSubmit,
  })

  async function handleSubmit(values: UserDetailFormFormikTypes) {
    const userService = new UserService()

    if (!(values.avatar_url instanceof File)) {
      delete values.avatar_url
    }

    try {
      const response = await userService.update(user.id, values)

      if (response.status === 200) {
        toast.success('Data updated successfully')

        setUser((prev) => ({
          ...prev,
          avatar_url: response.data.avatar_url,
          phone_number: response.data.phone_number,
        }))
      }
    } catch (error) {
      toast.error('Error updating user.')
    }
  }

  useEffect(() => {
    handleUpdateInitialValues({
      avatar_url: getProfileImage() as string,
      phone_number: user.phone_number,
    })
  }, [user])

  return (
    <PrimaryFormContainer
      title="Project Details"
      onSubmit={formik.handleSubmit}
    >
      <FileInputWithPreview
        labelText="Upload Profile Image"
        formikSetFieldValue={formik.setFieldValue}
        name="avatar_url"
        src={formik.values.avatar_url}
      />

      <Input
        labelText="Name"
        id="name"
        name="name"
        defaultValue={user.name}
        readOnly
      />

      <StyledGroupWrapper>
        <Input
          labelText="E-mail"
          id="phoneNumber"
          value={user.email}
          readOnly
        />
        <Input
          labelText="Phone Number"
          id="phone_number"
          name="phone_number"
          value={formik.values.phone_number}
          onChange={formik.handleChange}
          placeholder="Enter the phone number"
        />
      </StyledGroupWrapper>

      <Button
        css={{
          backgroundColor: '$blue400',
          color: '$white100',
          marginLeft: 'auto',
        }}
        withIcon
        type="submit"
      >
        <Check size={24} />
        Save Changes
      </Button>
    </PrimaryFormContainer>
  )
}

export default UserDetailForm

const StyledGroupWrapper = styled('div', {
  gap: '$6',
  display: 'grid',
  gridTemplateColumns: '1fr 1fr',
})
