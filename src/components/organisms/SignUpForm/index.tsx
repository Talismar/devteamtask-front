import { styled } from '@/styles'
import Image from 'next/image'
import { FormHTMLAttributes, ReactNode } from 'react'
import logoSvg from '../../../../public/icons/logo-blue-md.svg'

interface SignUpFormProps extends FormHTMLAttributes<HTMLFormElement> {
  children: ReactNode
  title: string
  paragraph: string
}

function SignUpForm({ children, title, paragraph, ...rest }: SignUpFormProps) {
  return (
    <FormWrapper {...rest}>
      <FormHeader>
        <Image src={logoSvg} alt="logo" />
        <h1>{title}</h1>
        <p>{paragraph}</p>
      </FormHeader>
      {children}
    </FormWrapper>
  )
}

export default SignUpForm

const FormWrapper = styled('form', {
  minWidth: '22.6875rem', // 363px
  display: 'flex',
  flexDirection: 'column',
  gap: '$4',
})

const FormHeader = styled('div', {
  '& img': {
    display: 'block',
    margin: 'auto',
    marginBottom: '$4',
  },
  '& h1': {
    fontSize: '2rem',
    color: '$gray800',
    marginBottom: '1rem',
    textAlign: 'center',
  },
  '& p': {
    color: '$gray700',
    textAlign: 'center',
    marginBottom: '3rem',
  },
})
