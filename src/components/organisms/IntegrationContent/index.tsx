import Button from '@/components/atoms/Button'
import Circle from '@/components/atoms/Circle'
import Input from '@/components/atoms/Input'
import ModalDefault from '@/components/molecules/ModalDefault'
import PrimaryFormContainer from '@/components/molecules/PrimaryForm'
import { ProjectContext } from '@/contexts/ProjectContext'
import { styled } from '@/styles'
import { Copy } from '@phosphor-icons/react'
import { useContext, useState } from 'react'

const WEBHOOK_SECRET = process.env.NEXT_PUBLIC_DTT_WEBHOOK_SECRET
const WEBHOOK_BASE_URL = process.env.NEXT_PUBLIC_DTT_BACKEND_WEBHOOK_URL

function IntegrationContent() {
  const [isOpen, setIsOpen] = useState(false)
  const { projectData } = useContext(ProjectContext)
  const WEBHOOK_URL = `${WEBHOOK_BASE_URL}?project_id=${projectData?.project_data.id}`

  function redirectToGitHubInstallation(project_id: string) {
    const clientId = process.env.NEXT_PUBLIC_GITHUB_CLIENT_ID
    const redirectUri = encodeURIComponent(
      'http://127.0.0.1:8001/api/project/integration/github/installation'
    )
    const scope = 'read:user,repo,write:repo_hook'

    const githubInstallURL = `https://github.com/apps/devteamtask/installations/new?state=SUA_INFORMACAO_DE_ESTADO&client_id=${clientId}&redirect_uri=${redirectUri}&scope=${scope}`

    window.location.href = githubInstallURL
  }

  return (
    <>
      <ModalDefault
        title="Get configuration to activate webhooks"
        subTitle="Sub title"
        state={isOpen}
        handleClose={() => setIsOpen(false)}
        content={
          <StyledDiv>
            <StyledWrapperInput>
              <Input
                name="payload-url"
                type="text"
                readOnly
                value={WEBHOOK_URL}
                labelText="Payload URL"
                id="webhook-payload-url"
              />
              <Button
                size="sm"
                css={{ color: '$blue400', bg: '$blue200' }}
                onClick={() => navigator.clipboard.writeText(WEBHOOK_URL)}
              >
                <Copy size={32} />
              </Button>
            </StyledWrapperInput>
            <StyledWrapperInput>
              <Input
                name="webhook-secret"
                type="password"
                readOnly
                value={WEBHOOK_SECRET}
                labelText={'Secret'}
                id="webhook-secret"
              />
              <Button
                size="sm"
                css={{ color: '$blue400', bg: '$blue200' }}
                onClick={() => {
                  navigator.clipboard.writeText(WEBHOOK_SECRET)
                }}
              >
                <Copy size={32} />
              </Button>
            </StyledWrapperInput>
          </StyledDiv>
        }
      />
      <PrimaryFormContainer title="Integrations">
        <StyledGroupWrapper>
          <Circle css={{ backgroundColor: '$blue400' }} />

          <div className="text-wrapper">
            <p>
              DevTeamTask <strong>CLI</strong>
            </p>

            <span>
              A tool to log your tasks in your development environment
            </span>
          </div>
        </StyledGroupWrapper>

        <StyledGroupWrapper>
          <a
            href="https://github.com/Talismar/devteamtask-cli#how-to-use"
            target="_blank"
          >
            <Button
              size="sm"
              css={{ color: '$blue400', bg: '$blue200' }}
              type="button"
            >
              Download
            </Button>
          </a>
          <a
            href="https://github.com/Talismar/devteamtask-cli/tree/main#--------devteamtask--"
            target="_blank"
          >
            <Button
              size="sm"
              css={{ color: '$orange400', bg: '$orange200' }}
              type="button"
            >
              Manual
            </Button>
          </a>
        </StyledGroupWrapper>

        <StyledGroupWrapper>
          <Circle css={{ backgroundColor: '$blue400' }} />

          <div className="text-wrapper">
            <p>
              GitHub <strong>HOOK</strong>
            </p>
            <span>Mark tasks as done based on their commit</span>
          </div>
        </StyledGroupWrapper>

        <StyledGroupWrapper>
          <Button
            size="sm"
            css={{ color: '$blue400', bg: '$blue200' }}
            type="button"
            onClick={() => setIsOpen(true)}
          >
            Get Configuration
          </Button>

          <a
            href="#"
            // target="_blank"
          >
            <Button
              size="sm"
              css={{ color: '$orange400', bg: '$orange200' }}
              type="button"
            >
              Manual
            </Button>
          </a>
        </StyledGroupWrapper>
      </PrimaryFormContainer>
    </>
  )
}

export default IntegrationContent

const StyledGroupWrapper = styled('div', {
  gap: '$4',
  display: 'flex',
  alignItems: 'center',

  '& .text-wrapper span': {
    color: '$gray600',
    fontWeight: 300,
  },

  '& .text-wrapper p': {
    color: '$gray700',
  },
})

const StyledDiv = styled('div', {
  display: 'flex',
  flexDirection: 'column',
  gap: '$4',
  minWidth: '480px',
})

const StyledWrapperInput = styled('div', {
  display: 'grid',
  gridTemplateColumns: '1fr auto',
  gap: '$4',

  button: {
    marginTop: 'auto',
  },
})
