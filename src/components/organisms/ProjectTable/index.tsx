import { ProjectListTypes } from '@/@types/projects'
import Avatar from '@/components/atoms/Avatar'
import { AuthContext } from '@/contexts/AuthContext'
import { ProjectService } from '@/services/ProjectService'
import { styled, theme } from '@/styles'
import {
  CheckSquareOffset,
  GearSix,
  SpinnerGap,
  Trash,
} from '@phosphor-icons/react'
import Image from 'next/image'
import Link from 'next/link'
import { useRouter } from 'next/router'
import { useContext, useState } from 'react'
import { toast } from 'react-toastify'
import emtpyTableImage from '../../../../public/imgs/empty-table-image.jpg'
import ModalConfirmation from '../ModalConfirmation'
import { UserTypes } from '@/@types/users'

interface ProjectTableProps {
  table: ProjectListTypes
  deleteProjectInState(projectId: string): void
}

function ProjectTable({ table, deleteProjectInState }: ProjectTableProps) {
  const { user } = useContext(AuthContext)
  const router = useRouter()
  const [dialogDeleteProject, setDialogDeleteProject] = useState({
    state: false,
    projectId: '',
  })

  async function handleDeleteProject() {
    const projectService = new ProjectService()
    try {
      const response = await projectService.deleteProject(
        dialogDeleteProject.projectId
      )
      if (response.status === 204) {
        toast.success(`Project deleted successfully`)
        deleteProjectInState(dialogDeleteProject.projectId)
        setDialogDeleteProject((prev) => ({ ...prev, state: false }))
      }
    } catch (error) {
      toast.error('Error deleting project')
    }
  }

  function handleGoToProjectSettings(projectId: string) {
    router.push(`/projects/${projectId}/settings`)
  }

  function handleOpenModalDeleteProject(projectId: string) {
    setDialogDeleteProject({ state: true, projectId })
  }

  function getCollaboratorProfileImage(user_information: Object) {
    if (
      'avatar_url' in user_information &&
      user_information.avatar_url !== null
    ) {
      return `${process.env.NEXT_PUBLIC_DTT_SERVICE_USER_URL}/media/images/users/${user_information.avatar_url}`
    } else if (
      'auth_provider' in user_information &&
      user_information.auth_provider === 'GITHUB'
    ) {
      return `https://avatars.githubusercontent.com/${user.name}`
    }
  }

  return (
    <>
      <ModalConfirmation
        isOpen={dialogDeleteProject.state}
        title="Delete Project"
        subTitle="Do you really want to delete this project now ?"
        handleClose={() =>
          setDialogDeleteProject((prev) => ({ ...prev, state: false }))
        }
        handleSubmit={handleDeleteProject}
      />

      {table.projects?.length === 0 ? (
        <DivImageWrapper>
          <Image src={emtpyTableImage} alt="Empty Table Image" width={320} />
        </DivImageWrapper>
      ) : (
        <StyledWrapped>
          <thead>
            <tr>
              <th style={{ width: '64px' }} />
              <th>Project name</th>
              <th>Current sprint</th>
              <th>Team leader</th>
              <th>Time</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            {table.projects?.map((project) => {
              const leader = table.users.find(
                (item) => item.id === project.leader_id
              )
              const collaborators = table.users.filter((item) =>
                project.collaborators_ids.includes(item.id)
              )

              return (
                <tr key={project.id}>
                  <td>
                    {project.state === 'IN PROGRESS' ? (
                      <SpinnerGap
                        size={24}
                        weight="bold"
                        color={theme.colors.blue400.toString()}
                      />
                    ) : (
                      <CheckSquareOffset
                        size={24}
                        weight="bold"
                        color={theme.colors.blue400.toString()}
                      />
                    )}
                  </td>
                  <td className="limit-size project-name">
                    <Link href={`/projects/${project.id}/boards`}>
                      {project.name}
                    </Link>
                  </td>
                  <td className="limit-size">
                    {project.current_sprint?.name || (
                      <Span
                        css={{
                          backgroundColor: '$orange200',
                          color: '$orange400',
                          fontWeight: 500,
                          padding: 4,
                          display: 'inline-block',
                          borderRadius: 4,
                        }}
                      >
                        No Sprint Started
                      </Span>
                    )}
                  </td>
                  <td className="limit-size">{leader?.name}</td>
                  <td>
                    <ul className="list-collaborators">
                      {collaborators.map((collaborator_data) => {
                        return (
                          <li key={collaborator_data.id}>
                            <Avatar
                              title={collaborator_data.name}
                              size="xs"
                              withBorderBlue400
                              src={getCollaboratorProfileImage(
                                collaborator_data
                              )}
                              firstLetter={collaborator_data.name
                                .charAt(0)
                                .toUpperCase()}
                            />
                          </li>
                        )
                      })}
                      {project.collaborators_ids?.length === 0 && (
                        <Span
                          css={{
                            backgroundColor: '$blue200',
                            color: '$blue400',
                            fontWeight: 500,
                            padding: 4,
                            display: 'inline-block',
                            borderRadius: 4,
                          }}
                        >
                          No Team
                        </Span>
                      )}
                    </ul>
                  </td>
                  <td className="icons">
                    {project.leader_id === user.id && (
                      <>
                        <Trash
                          weight="fill"
                          onClick={() =>
                            handleOpenModalDeleteProject(project.id)
                          }
                        />
                        <GearSix
                          weight="fill"
                          onClick={() => handleGoToProjectSettings(project.id)}
                        />
                      </>
                    )}
                  </td>
                </tr>
              )
            })}
          </tbody>
        </StyledWrapped>
      )}
    </>
  )
}

export default ProjectTable

const StyledWrapped = styled('table', {
  minWidth: '100%',
  borderCollapse: 'collapse',
  overflow: 'scroll',

  '& tr th': {
    backgroundColor: '$blue400',
    color: '$white100',
    paddingTop: '0.75rem',
    paddingBottom: '0.75rem',
    whiteSpace: 'nowrap',
    lineHeight: 1.5,
  },

  '& th, td': {
    textAlign: 'left',
    paddingRight: '2rem',
    paddingLeft: '0.5rem',
  },

  '& tr td': {
    borderBottom: '1px solid $gray100',
    paddingTop: '0.5rem',
    paddingBottom: '0.5rem',
    color: '$gray700',
  },

  '& tr td.limit-size': {
    overflow: 'hidden',
    whiteSpace: 'nowrap',
    maxWidth: '180px',
    textOverflow: 'ellipsis',
  },

  '& tr td.project-name a': {
    color: '$gray800',
  },

  '& tr td.icons svg:first-child': {
    color: '$red100',
    marginRight: '0.4rem',
    cursor: 'pointer',
  },
  '& tr td.icons svg:last-child': {
    color: '$gray300',
    cursor: 'pointer',
  },
  '& tr td ul.list-collaborators': {
    listStyle: 'none',
    display: 'flex',
    gap: '$2',
  },
})

const DivImageWrapper = styled('div', {
  height: '560px',
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
})

const Span = styled('span', {})
