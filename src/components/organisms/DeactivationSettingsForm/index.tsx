import Button from '@/components/atoms/Button'
import PrimaryFormContainer from '@/components/molecules/PrimaryForm'
import { AuthContext } from '@/contexts/AuthContext'
import { useSignOut } from '@/hooks/useSignOut'
import { UserService } from '@/services/UserService'
import { styled } from '@/styles'
import { SignOut, Trash } from '@phosphor-icons/react'
import { useContext } from 'react'
import { toast } from 'react-toastify'

const userService = new UserService()

function DeactivationSettingsForm() {
  const { user } = useContext(AuthContext)

  async function handleDeleteAccount() {
    try {
      // const response = await userService.deleteAccount(user.id)
      // if (response.status === 204) {
      //   useSignOut('/')
      //   toast.success('Account deleted successfully.')
      // }
    } catch (error) {
      toast.error('Error deleting account.')
    }
  }

  async function handleDeactivateAccount() {
    try {
      const response = await userService.update(user.id, { is_active: false })

      if (response.status === 200) {
        useSignOut('/')
        toast.success('Deactivated successfully.')
      }
    } catch (error) {
      toast.error('Error updating account.')
    }
  }

  return (
    <PrimaryFormContainer title="Deactivate Account">
      <StyledItemWrapper>
        <SignOut size={24} />
        <div className="text-wrapper">
          <p>Deactivate Account</p>
          <span>You can activate your account again whenever you want.</span>
        </div>
      </StyledItemWrapper>

      <StyledItemWrapper>
        <Trash size={24} />
        <div className="text-wrapper">
          <p>Delete Account permanantly</p>
          <span>
            You can’t re-activate your account again, It will delete your
            account permanantly.
          </span>
        </div>
      </StyledItemWrapper>

      <StyledGroupWrapper>
        <Button
          size="sm"
          css={{ color: '$white100', backgroundColor: '$blue400' }}
          onClick={handleDeactivateAccount}
          type="button"
        >
          Deactivate Account
        </Button>
        <Button
          size="sm"
          css={{ color: '$white100', backgroundColor: '$red400' }}
          onClick={handleDeleteAccount}
          type="button"
        >
          Delete Account
        </Button>
      </StyledGroupWrapper>
    </PrimaryFormContainer>
  )
}

export default DeactivationSettingsForm

const StyledItemWrapper = styled('div', {
  display: 'flex',
  gap: '$4',
  alignItems: 'center',

  svg: {
    backgroundColor: '$gray300',
    padding: '$4',
    boxSizing: 'content-box',
    borderRadius: '50%',
    color: '$gray800',
  },
})

const StyledGroupWrapper = styled('div', {
  gap: '1.5rem',
  display: 'flex',
})
