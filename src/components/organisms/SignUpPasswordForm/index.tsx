import Button from '@/components/atoms/Button'
import Input from '@/components/atoms/Input'
import { SignUpFormikTypes } from '@/validation/signupValidation'
import { FormikProps } from 'formik'
import SignUpForm from '../SignUpForm'

interface SignUpPasswordFormProps {
  formik: FormikProps<SignUpFormikTypes>
}

function SignUpPasswordForm({ formik }: SignUpPasswordFormProps) {
  return (
    <SignUpForm
      title="Choose a password"
      paragraph="Must be at least 8 characters."
      onSubmit={formik.handleSubmit}
    >
      <Input
        id="password"
        labelText="Password"
        name="password"
        autoComplete="new-password"
        value={formik.values.password}
        onChange={formik.handleChange}
        onBlur={formik.handleBlur}
        type="password"
        placeholder="Enter the password"
        errorMessage={
          (formik.errors.password &&
            formik.touched.password &&
            formik.errors.password) ||
          ''
        }
      />

      <Input
        id="confirmPassword"
        labelText="Confirm Password"
        type="password"
        autoComplete="new-password"
        placeholder="Enter the password again"
        value={formik.values.confirmPassword}
        onChange={formik.handleChange}
        onBlur={formik.handleBlur}
        errorMessage={
          (formik.errors.confirmPassword &&
            formik.touched.confirmPassword &&
            formik.errors.confirmPassword) ||
          ''
        }
      />

      <Button
        css={{
          marginTop: '$4',
          backgroundColor: '$blue400',
          color: '$white100',
        }}
        type="submit"
        // onClick={formik.submitForm}
      >
        Register Now
      </Button>
    </SignUpForm>
  )
}

export default SignUpPasswordForm
