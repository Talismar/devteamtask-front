import Button from '@/components/atoms/Button'
import ToogleButton from '@/components/atoms/ToogleButton'
import PrimaryFormContainer from '@/components/molecules/PrimaryForm'
import { AuthContext } from '@/contexts/AuthContext'
import { UserService } from '@/services/UserService'
import { styled } from '@/styles'
import { Check } from '@phosphor-icons/react'
import { useContext, useState } from 'react'
import { toast } from 'react-toastify'

function NotificationSettingsForm() {
  const { user, setUser, getAllNotifications } = useContext(AuthContext)
  const [state, setState] = useState(user.notification_state)

  async function handleSaveChange() {
    if (user.notification_state === state) return

    const userService = new UserService()

    try {
      const response = await userService.update(user.id, {
        notification_state: state,
      })
      if (response.status === 200) {
        toast.success('Successfully saved changes')
        setUser((prev) => ({ ...prev, notification_state: state }))

        if (state) {
          getAllNotifications()
        }
      }
    } catch (error) {
      toast.error('Error saving changes')
    }
  }

  return (
    <PrimaryFormContainer title="Notification Settings">
      <DivWrapper>
        <p>General email notifications</p>
        <ToogleButton
          state={state === true ? 'enabled' : 'disabled'}
          onClick={() => setState((prev) => !prev)}
        />
      </DivWrapper>
      <Button
        css={{
          backgroundColor: '$blue400',
          color: '$white100',
          alignSelf: 'flex-end',
        }}
        withIcon
        type="button"
        onClick={handleSaveChange}
      >
        <Check size={24} />
        Save Changes
      </Button>
    </PrimaryFormContainer>
  )
}

export default NotificationSettingsForm

const DivWrapper = styled('div', {
  display: 'flex',
  justifyContent: 'space-between',
})
