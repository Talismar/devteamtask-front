import Header from '@/components/molecules/Header'
import { styled } from '@/styles'
import { ReactNode } from 'react'

interface DefaultLayoutProps {
  navbar: ReactNode
  content: ReactNode
  headerTitle: string
}

function DefaultLayout({ navbar, content, headerTitle }: DefaultLayoutProps) {
  return (
    <WrappedPage>
      {navbar}

      <div>
        <Header title={headerTitle} />
        <div className="content-container">{content}</div>
      </div>
    </WrappedPage>
  )
}

export default DefaultLayout

const WrappedPage = styled('div', {
  minHeight: '100vh',
  display: 'grid',
  gridTemplateColumns: 'auto 80vw',

  '& div > div.content-container': {
    mx: '1.5rem',
    marginTop: '0.75rem',
  },
})
