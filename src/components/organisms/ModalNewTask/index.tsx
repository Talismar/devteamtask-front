import {
  ProjectDetailsTypes,
  ProjectTypes,
  StatusTypes,
} from '@/@types/projects'
import Button from '@/components/atoms/Button'
import Input from '@/components/atoms/Input'
import Textarea from '@/components/atoms/Textarea'
import CustomSelect from '@/components/molecules/CustomSelect'
import ModalDefault from '@/components/molecules/ModalDefault'
import { styled } from '@/styles'
import { priorityFormatted } from '@/utils/priorityFormatted'
import { BoardTaskFormikTypes } from '@/validation/boardTaskValidation'
import { Check } from '@phosphor-icons/react'
import { FormikProps } from 'formik'
import { useEffect } from 'react'

interface ModalNewTaskProps {
  handleClose(): void
  formik: FormikProps<BoardTaskFormikTypes>
  isOpen: boolean
  projectData: ProjectDetailsTypes
  statusDefaultValue: StatusTypes
}

function ModalNewTask({
  handleClose,
  isOpen,
  projectData,
  statusDefaultValue,
  formik,
}: ModalNewTaskProps) {
  useEffect(() => {
    formik.setValues((prev) => ({
      ...prev,
      taskStatus: {
        label: statusDefaultValue.name,
        value: statusDefaultValue.id,
      },
    }))
  }, [])

  return (
    <ModalDefault
      title="Create tasks"
      subTitle="Describe the task in an easy way"
      state={isOpen}
      handleClose={handleClose}
      content={
        <StyledForm onSubmit={formik.handleSubmit}>
          <Input
            id="taskName"
            labelText="Task name"
            type="text"
            name="taskName"
            placeholder="Enter the task name"
            value={formik.values.taskName}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            errorMessage={
              formik.touched.taskName && formik.errors.taskName
                ? formik.errors.taskName
                : ''
            }
          />

          <Textarea
            id="taskDescription"
            labelText="Task description"
            name="taskDescription"
            rows={8}
            placeholder="Enter the task description"
            value={formik.values.taskDescription}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            errorMessage={
              formik.touched.taskDescription && formik.errors.taskDescription
                ? formik.errors.taskDescription
                : ''
            }
          />

          <StyledInputGroup>
            <CustomSelect
              labelText="Task status"
              name="taskStatus"
              options={projectData.project_data.status.map((status) => ({
                value: status.id,
                label: status.name,
              }))}
              value={formik.values.taskStatus}
              onChange={(selectedOption) => {
                const event = {
                  target: { name: 'taskStatus', value: selectedOption },
                }
                formik.handleChange(event)
              }}
              onBlur={() => {
                formik.handleBlur({ target: { name: 'taskStatus' } })
              }}
            />

            <CustomSelect
              labelText="Task Priority"
              name="taskPriority"
              options={priorityFormatted.map((priority) => ({
                value: priority.value,
                label: priority.label,
              }))}
              value={formik.values.taskPriority}
              onChange={(selectedOption) => {
                const event = {
                  target: { name: 'taskPriority', value: selectedOption },
                }
                formik.handleChange(event)
              }}
              onBlur={() => {
                formik.handleBlur({ target: { name: 'taskPriority' } })
              }}
            />
          </StyledInputGroup>

          <CustomSelect
            labelText="Task tag"
            name="taskTag"
            isMulti
            value={formik.values.taskTag ?? []}
            onChange={(selectedOption) => {
              const event = {
                target: { name: 'taskTag', value: selectedOption },
              }
              formik.handleChange(event)
            }}
            onBlur={() => {
              formik.handleBlur({ target: { name: 'taskTag' } })
            }}
            options={projectData.project_data.tags.map((tag) => ({
              value: tag.id,
              label: tag.name,
            }))}
          />

          <CustomSelect
            labelText="Assing this task to"
            name="taskCollaborator"
            options={projectData.project_data.collaborators_ids.map(
              (collaborator_id) => {
                const collaborator = projectData.users.find(
                  (item) => item.id === collaborator_id
                )

                return {
                  value: collaborator!.id,
                  label: collaborator!.name,
                }
              }
            )}
            value={{
              value: formik.values.taskCollaborator.label || '',
              label: formik.values.taskCollaborator.label || '',
            }}
            onChange={(selectedOption) => {
              const event = {
                target: { name: 'taskCollaborator', value: selectedOption },
              }
              formik.handleChange(event)
            }}
          />

          <Button
            type="submit"
            withIcon
            css={{ color: '$white100', backgroundColor: '$blue400' }}
          >
            <Check size={16} /> Create task
          </Button>
        </StyledForm>
      }
    />
  )
}

export default ModalNewTask

const StyledInputGroup = styled('div', {
  display: 'grid',
  gridTemplateColumns: '1fr 1fr',
  columnGap: '1rem',
  alignItems: 'end',
})

const StyledForm = styled('form', {
  display: 'flex',
  gap: '1.5rem',
  flexDirection: 'column',
  minWidth: '511px',
})
