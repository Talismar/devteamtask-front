import Button from '@/components/atoms/Button'
import Input from '@/components/atoms/Input'
import FileInputWithPreview from '@/components/molecules/FileInputWithPreview'
import PrimaryFormContainer from '@/components/molecules/PrimaryForm'
import TagsInput from '@/components/molecules/TagsInput'
import { ProjectContext } from '@/contexts/ProjectContext'
import { ProjectService } from '@/services/ProjectService'
import { TagService } from '@/services/TagService'
import { styled } from '@/styles'
import { useFormik } from 'formik'
import { useContext, useEffect, useState } from 'react'
import { toast } from 'react-toastify'
import * as Yup from 'yup'
import ModalConfirmation from '../ModalConfirmation'
import { useRouter } from 'next/router'

const formSchema = Yup.object().shape({
  name: Yup.string(),
  end_date: Yup.mixed(),
  product_owner: Yup.string().nullable().email(),
  tag: Yup.string(),
  logo_url: Yup.mixed().nullable(),
})

type FormTypes = Omit<
  Yup.InferType<typeof formSchema>,
  'end_date' | 'tag' | 'logo_url'
> & {
  end_date?: string
  tag: string
  logo_url: null | string | File
}

const projectService = new ProjectService()

function ProjectDetailForm() {
  const { projectData, setProjectData, removeTagInProjectState } =
    useContext(ProjectContext)
  const [dialogIsOpen, setDialogIsOpen] = useState(false)
  const router = useRouter()
  const formik = useFormik<FormTypes>({
    initialValues: {
      name: projectData?.project_data.name,
      end_date: projectData?.project_data.end_date,
      product_owner: projectData?.users.find(
        (item) => item.id === projectData.project_data.product_owner_id
      )?.email,
      tag: '',
      logo_url: projectData?.project_data.logo_url ?? null,
    },
    validationSchema: formSchema,
    onSubmit: async (values) => {
      if (!projectData) return
      try {
        const response = await projectService.update(
          projectData.project_data.id,
          {
            name: values.name,
            logo_url:
              values.logo_url instanceof File ? values.logo_url : undefined,
            product_owner: values.product_owner,
          }
        )

        if (response.status === 200) {
          toast.success('Project updated successfully')

          if (values.product_owner !== formik.initialValues.product_owner) {
            toast.success(
              'The invitation to the product owner was sent successfully'
            )
          }

          setProjectData(
            (prev) =>
              prev && {
                ...prev,
                project_data: {
                  ...prev.project_data,
                  logo_url: response.data.project_data.logo_url,
                  name: response.data.project_data.name,
                },
              }
          )
        }
      } catch (error) {
        toast.error('Error while updating project')
      }
    },
  })

  useEffect(() => {
    if (projectData?.project_data) {
      formik.setValues({
        name: projectData.project_data.name,
        end_date: projectData.project_data.end_date,
        product_owner: projectData.users.find(
          (item) => item.id === projectData.project_data.product_owner_id
        )?.email,
        tag: '',
        logo_url: projectData?.project_data.logo_url,
      })
    }
  }, [projectData?.project_data])

  if (projectData) {
    async function handleNewTag() {
      if (!projectData) return
      if (formik.values.tag.trim().length === 0) {
        toast.warn('Please enter a tag name')
        return
      }

      const tagService = new TagService()
      try {
        const response = await tagService.create({
          name: formik.values.tag,
          project_id: projectData.project_data.id,
        })

        if (response.status === 201) {
          formik.setValues((prev) => ({ ...prev, tag: '' }))
          const tags = projectData.project_data.tags
          const tagAlreadyExist = tags.find(
            (item) => item.id === response.data.id
          )
          if (!tagAlreadyExist) tags?.push(response.data)
          toast.success('Tag created successfully.')
          setProjectData(
            (prev) =>
              prev && {
                ...prev,
                project_data: {
                  ...prev.project_data,
                  tags: tags,
                },
              }
          )
        }
      } catch (error) {
        toast.error('Error creating tag')
      }
    }

    async function handleDeleteTag(tagId: number) {
      try {
        const response = await projectService.removeProjectTagStatus(
          projectData?.project_data.id ?? '',
          'Tag',
          tagId
        )

        if (response.status === 200) {
          toast.success('Tag deleted successfully')
          removeTagInProjectState(tagId)
        }
      } catch (error) {
        toast.error('Error when trying to delete tag')
      }
    }

    async function handleFinishProject() {
      if (!projectData?.project_data.id) return
      try {
        const response = await projectService.update(
          projectData.project_data.id,
          {
            state: 'COMPLETED',
          }
        )

        if (response.status === 200) {
          toast.success('Project completed successfully')
          setProjectData(
            (prev) =>
              prev && {
                ...prev,
                project_data: { ...prev.project_data, state: 'COMPLETED' },
              }
          )
          router.push('/projects')
        }
      } catch (error) {
        toast.error('Error when trying to completed project')
      }
    }

    return (
      <>
        <ModalConfirmation
          isOpen={dialogIsOpen}
          title="Finish Project"
          subTitle="Do you really want to finish this project now?"
          handleClose={() => setDialogIsOpen(false)}
          handleSubmit={() => {
            handleFinishProject()
            setDialogIsOpen(false)
          }}
        />

        <PrimaryFormContainer
          title="User Details"
          onSubmit={formik.handleSubmit}
        >
          <FileInputWithPreview
            labelText="Upload Project Logo"
            src={formik.values.logo_url || projectData.project_data.logo_url}
            name="logo_url"
            formikSetFieldValue={formik.setFieldValue}
          />
          <Input
            labelText="Name"
            id="name"
            name="name"
            value={formik.values.name}
            onChange={formik.handleChange}
          />

          <StyledGroupWrapper display="grid">
            <Input
              type="date"
              labelText="Estimated end date"
              id="estimatedEndDate"
              name="estimatedEndDate"
              value={projectData.project_data.end_date.split('T')[0]}
              disabled
            />
            <Input
              labelText="State"
              id="status"
              value={projectData?.project_data.state}
              disabled
            />
          </StyledGroupWrapper>

          <Input
            labelText="Product Owner"
            id="product_owner"
            name="product_owner"
            type="email"
            placeholder="enter the product owner email"
            value={
              formik.values.product_owner ||
              projectData.project_data.product_owner_id ||
              ''
            }
            onChange={formik.handleChange}
          />

          <TagsInput
            items={projectData.project_data.tags}
            handleDelete={handleDeleteTag}
            handleNewTag={handleNewTag}
            value={formik.values.tag}
            onChange={formik.handleChange}
          />

          <StyledGroupWrapper display="flex" justifyContent="end">
            <Button css={{ bg: '$blue400' }} type="submit">
              Save Changes
            </Button>
            <Button
              variant="outline"
              type="button"
              disabled={projectData.project_data.state === 'COMPLETED'}
              css={{ color: '$orange400', borderColor: '$orange400' }}
              onClick={() => setDialogIsOpen(true)}
            >
              Finished Project
            </Button>
          </StyledGroupWrapper>
        </PrimaryFormContainer>
      </>
    )
  }

  return null
}

export default ProjectDetailForm

const StyledGroupWrapper = styled('div', {
  gap: '$6',

  variants: {
    display: {
      flex: {
        display: 'flex',
      },
      grid: {
        display: 'grid',
        gridTemplateColumns: '1fr 1fr',
      },
    },
    justifyContent: {
      end: {
        justifyContent: 'flex-end',
      },
    },
  },
})
