import { styled } from '@/styles'
import { CSS } from '@stitches/react'
import { ReactNode, TableHTMLAttributes } from 'react'

interface TableRootProps extends TableHTMLAttributes<HTMLTableElement> {
  children: ReactNode
  css?: CSS
}

function TableRoot({ children, css, ...rest }: TableRootProps) {
  return (
    <TableWrapper css={css}>
      <Table {...rest}>{children}</Table>
    </TableWrapper>
  )
}

const TableWrapper = styled('div', {
  overflowX: 'auto',
})

const Table = styled('table', {
  width: '100%',
  borderCollapse: 'collapse',
})

export default TableRoot
