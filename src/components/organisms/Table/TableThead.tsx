import { styled } from '@/styles'
import { ReactElement } from 'react'

interface TableTheadProps {
  children: ReactElement
}

function TableThead({ children }: TableTheadProps) {
  return <StyledTableThead>{children}</StyledTableThead>
}

export default TableThead

const StyledTableThead = styled('thead', {})
