import TableRoot from './TableRoot'
import TableTbody from './TableTbody'
import TableTd from './TableTd'
import TableTh from './TableTh'
import TableThead from './TableThead'
import TableTr from './TableTr'

const Table = {
  Root: TableRoot,
  Tr: TableTr,
  Th: TableTh,
  Td: TableTd,
  Thead: TableThead,
  Tbody: TableTbody,
}

export default Table
