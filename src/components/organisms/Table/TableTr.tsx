import { styled } from '@/styles'
import React, { DetailedHTMLProps, ReactNode } from 'react'

interface TableTrProps
  extends DetailedHTMLProps<
    React.HTMLAttributes<HTMLTableRowElement>,
    HTMLTableRowElement
  > {
  children: ReactNode
}

function TableTrComponent({ children, ...rest }: TableTrProps) {
  return <tr {...rest}>{children}</tr>
}

const TableTr = styled(TableTrComponent, {
  variants: {
    backgroundColor: {
      primary: {
        backgroundColor: '$blue400',
        color: '$white100',
      },
    },
    withBorderBottom: {
      true: {
        borderBottom: '1px solid $gray100',
      },
    },
  },
})

export default TableTr
