import { styled } from '@/styles'
import { DetailedHTMLProps, ReactNode, ThHTMLAttributes } from 'react'

interface TableThProps
  extends DetailedHTMLProps<
    ThHTMLAttributes<HTMLTableHeaderCellElement>,
    HTMLTableHeaderCellElement
  > {
  children?: ReactNode
}

function TableThComponent({ children, ...rest }: TableThProps) {
  return <th {...rest}>{children}</th>
}

const TableTh = styled(TableThComponent, {
  textAlign: 'start',
  whiteSpace: 'nowrap',
  overflow: 'hidden',
  textOverflow: 'ellipsis',
  padding: '0.75rem 0.5rem',
})
export default TableTh
