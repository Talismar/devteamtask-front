import { styled } from '@/styles'

const TableTd = styled('td', {
  textAlign: 'start',
  whiteSpace: 'nowrap',
  overflow: 'hidden',
  textOverflow: 'ellipsis',
  padding: '0.5rem',

  variants: {
    flexColumn: {
      true: {
        display: 'flex',
        flexDirection: 'column',
      },
    },
    cursorPointer: {
      true: {
        cursor: 'pointer',
      },
    },
  },
})

export default TableTd
