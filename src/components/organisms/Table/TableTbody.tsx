import { styled } from '@/styles'
import { ReactNode } from 'react'

interface TableTbodyProps {
  children: ReactNode
}

function TableTbody({ children }: TableTbodyProps) {
  return <StyledTableTbody>{children}</StyledTableTbody>
}

export default TableTbody

const StyledTableTbody = styled('tbody', {})
