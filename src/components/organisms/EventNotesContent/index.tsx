/* eslint-disable @typescript-eslint/no-unused-vars */
import { EventNotesTypes } from '@/@types/eventNotes'
import Button from '@/components/atoms/Button'
import Header from '@/components/molecules/Header'
import ProjectNavBar from '@/components/molecules/NavBar/ProjectNavBar'
import { EventNoteService } from '@/services/EventNoteService'
import { styled } from '@/styles'
import { AxiosError } from 'axios'
import MarkdownIt from 'markdown-it'
import { useState } from 'react'
import MdEditor, { Plugins } from 'react-markdown-editor-lite'
import { toast } from 'react-toastify'

MdEditor.use(Plugins.BlockCodeBlock, {})

// Initialize a markdown parser
const mdParser = new MarkdownIt(/* Markdown-it options */)

interface EventNotesContentProps {
  projectId: string
  eventNoteData: EventNotesTypes
  title: 'planning' | 'retrospective' | 'review'
}

function EventNotesContent({
  projectId,
  eventNoteData,
  title,
}: EventNotesContentProps) {
  const [text, setText] = useState(eventNoteData[title] || '')

  function handleEditorChange({ html, text }: { html: string; text: string }) {
    setText(text)
  }

  async function handleSave() {
    const eventNoteService = new EventNoteService()
    try {
      const response = await eventNoteService.update(eventNoteData.id, {
        [title]: text,
      })
      if (response.status === 200) {
        toast.success('Your plan has been successfully updated.')
      }
    } catch (error) {
      if (error instanceof AxiosError) {
        toast.error('Error updating.')
      }
    }
  }

  return (
    <StyledPageContainer>
      <ProjectNavBar pathActive="event-notes" projectId={projectId} />

      <StyledContent>
        <Header
          title={`Projects / Event notes / ${
            title.charAt(0).toUpperCase() + title.slice(1)
          }`}
        />

        <StyledEditorContainer>
          <SubHeaderWrapped style={{ flexGrow: 3 }}>
            <h2>{title.charAt(0).toUpperCase() + title.slice(1)}</h2>
            <p>
              Create your sprint {title} with <strong>markdown</strong> here
            </p>
          </SubHeaderWrapped>

          <MdEditor
            value={text}
            style={{ height: '100%', marginBottom: '1.5rem' }}
            renderHTML={(text) => mdParser.render(text)}
            onChange={handleEditorChange}
          />

          <StyledCustomButton
            onClick={handleSave}
            disabled={eventNoteData[title] === text}
            variant="solid"
            css={{ bg: '$blue400' }}
          >
            Save note
          </StyledCustomButton>
        </StyledEditorContainer>
      </StyledContent>
    </StyledPageContainer>
  )
}

export default EventNotesContent

const StyledPageContainer = styled('div', {
  minHeight: '100vh',
  display: 'grid',
  gridTemplateColumns: 'auto 1fr',
})

const StyledEditorContainer = styled('div', {
  display: 'flex',
  flexDirection: 'column',
  padding: '0 1.5rem 1.5rem',
})

const StyledContent = styled('div', {
  display: 'grid',
  gridTemplateRows: 'auto 1fr',
})

const StyledCustomButton = styled(Button, {
  alignSelf: 'end',
})

const SubHeaderWrapped = styled('header', {
  marginBottom: '1.5rem',
  marginTop: '1rem',
})
