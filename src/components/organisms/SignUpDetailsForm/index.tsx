import Button from '@/components/atoms/Button'
import Input from '@/components/atoms/Input'
import { SignUpFormikTypes } from '@/validation/signupValidation'
import { FormikProps } from 'formik'
import SignUpForm from '../SignUpForm'

interface SignUpDetailsFormProps {
  formik: FormikProps<SignUpFormikTypes>
  next(): void
}

function SignUpDetailsForm({ formik, next }: SignUpDetailsFormProps) {
  return (
    <SignUpForm
      title="Your details"
      paragraph="Enter your name and active email to proceed"
    >
      <Input
        id="fullName"
        labelText="Full Name"
        name="fullName"
        value={formik.values.fullName}
        onChange={formik.handleChange}
        onBlur={formik.handleBlur}
        type="text"
        placeholder="Enter the your full name"
        errorMessage={
          (formik.errors.fullName &&
            formik.touched.fullName &&
            formik.errors.fullName) ||
          ''
        }
      />

      <Input
        id="email"
        labelText="E-mail"
        type="email"
        autoComplete="username"
        placeholder="Enter the e-mail"
        value={formik.values.email}
        onChange={formik.handleChange}
        onBlur={formik.handleBlur}
        errorMessage={
          (formik.errors.email &&
            formik.touched.email &&
            formik.errors.email) ||
          ''
        }
      />

      <Input
        id="phoneNumber"
        labelText="Phone Number"
        type="text"
        autoComplete="phoneNumber"
        placeholder="Enter the phone number"
        value={formik.values.phoneNumber}
        onChange={formik.handleChange}
        onBlur={formik.handleBlur}
        errorMessage={
          (formik.errors.phoneNumber &&
            formik.touched.phoneNumber &&
            formik.errors.phoneNumber) ||
          ''
        }
      />

      <Button
        onClick={next}
        type="button"
        css={{
          marginTop: '$4',
          backgroundColor: '$blue400',
          color: '$white100',
        }}
      >
        Continue
      </Button>
    </SignUpForm>
  )
}

export default SignUpDetailsForm
