import Avatar from '@/components/atoms/Avatar'
import Button from '@/components/atoms/Button'
import Input from '@/components/atoms/Input'
import ModalDefault from '@/components/molecules/ModalDefault'
import { config, styled } from '@/styles'
import { ProjectFormikTypes } from '@/validation/projectValidation'
import { Plus } from '@phosphor-icons/react'
import { FormikProps } from 'formik'
import dynamic from 'next/dynamic'

const Select = dynamic(() => import('react-select'), {
  ssr: false,
})

interface ModalNewProjectProps {
  dialogState: boolean
  collaboratorsEmail: string[]
  collaboratorInputEmail: string
  formik: FormikProps<ProjectFormikTypes>
  dialogHandleClose(): void
  handleAddCollaboratorEmail(): void
  handleChangeCollaboratorInputEmail(email: string): void
  handleRemoveCollaboratorEmail(email: string): void
}

function ModalNewProject({
  dialogState,
  dialogHandleClose,
  handleAddCollaboratorEmail,
  handleRemoveCollaboratorEmail,
  handleChangeCollaboratorInputEmail,
  collaboratorInputEmail,
  formik,
  collaboratorsEmail,
}: ModalNewProjectProps) {
  return (
    <ModalDefault
      state={dialogState}
      handleClose={dialogHandleClose}
      title="Create project"
      subTitle="You can invite multiple users at once"
      content={
        <StyledFormNewProject onSubmit={formik.handleSubmit}>
          <Input
            id="projectName"
            labelText="Project name"
            type="text"
            name="projectName"
            placeholder="Enter the project name"
            value={formik.values.projectName}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            errorMessage={
              formik.touched.projectName && formik.errors.projectName
                ? formik.errors.projectName
                : ''
            }
          />

          <Input
            id="estimatedEndDate"
            labelText="Estimated end date"
            type="date"
            name="estimatedEndDate"
            placeholder="Enter the project name"
            value={formik.values.estimatedEndDate}
            min={new Date().toISOString().split('T')[0]}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            errorMessage={
              formik.touched.estimatedEndDate && formik.errors.estimatedEndDate
                ? formik.errors.estimatedEndDate
                : ''
            }
          />

          <Input
            id="productOwner"
            labelText="Product Onwer - Send invitation"
            type="email"
            name="productOwner"
            placeholder="Enter the e-mail of the product owner"
            value={formik.values.productOwner}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            errorMessage={
              formik.touched.productOwner && formik.errors.productOwner
                ? formik.errors.productOwner
                : ''
            }
          />

          <StyledGridGroup>
            <Input
              id="collaborators"
              labelText="Inivite members"
              type="email"
              name="collaborators"
              placeholder="Enter the e-mail of collaborators"
              value={collaboratorInputEmail}
              onChange={(e) =>
                handleChangeCollaboratorInputEmail(e.target.value)
              }
            />

            <div className="wrapped-btn" onClick={handleAddCollaboratorEmail}>
              <Plus
                size={20}
                color={config.theme.colors.blue400}
                weight="bold"
              />
            </div>
          </StyledGridGroup>
          {collaboratorsEmail.length > 0 && (
            <>
              {collaboratorsEmail.map((item) => (
                <DivItemsContainer key={item}>
                  <div>
                    <Avatar
                      size="sm"
                      firstLetter={item.charAt(0).toUpperCase()}
                    />
                    <span>{item}</span>
                  </div>
                  <Button
                    size="sm"
                    css={{ backgroundColor: '$red400', color: '$white100' }}
                    onClick={() => handleRemoveCollaboratorEmail(item)}
                  >
                    Remove
                  </Button>
                </DivItemsContainer>
              ))}
              <P>
                <Span>{collaboratorsEmail.length} email</Span> address added to
                invite, Click invite now to send invite
              </P>
            </>
          )}
          <Button css={{ bg: '$blue400' }}>Create project</Button>
        </StyledFormNewProject>
      }
    />
  )
}

export default ModalNewProject

const StyledFormNewProject = styled('form', {
  minWidth: '511px',
  display: 'flex',
  flexDirection: 'column',
  gap: '1.5rem',
})

const StyledGridGroup = styled('div', {
  display: 'grid',
  gridTemplateColumns: '1fr auto',
  columnGap: '1rem',
  alignItems: 'end',

  'div.wrapped-btn': {
    backgroundColor: '$blue200',
    padding: '0.82rem',
    borderRadius: '.5rem',
    cursor: 'pointer',
  },
})

const DivItemsContainer = styled('div', {
  display: 'flex',
  alignItems: 'center',
  gap: '$4',
  justifyContent: 'space-between',
  borderBottom: '1px solid $gray100',
  paddingBottom: '1rem',

  '& div': {
    display: 'flex',
    gap: '0.5rem',
    alignItems: 'center',
  },
})

const P = styled('p', {
  color: '$gray600',
})

const Span = styled('span', {
  color: '$blue400',
})
