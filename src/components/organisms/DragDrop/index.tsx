/* eslint-disable @typescript-eslint/no-explicit-any */
import { StatusTypes, TagTypes } from '@/@types/projects'
import { UserTypes } from '@/@types/users'
import CardBoard from '@/components/molecules/CardBoard'
import { ProjectContext } from '@/contexts/ProjectContext'
import { TaksService } from '@/services/TaskService'
import { styled } from '@/styles'
import { compareArray } from '@/utils/compareArray'
import { Plus } from '@phosphor-icons/react'
import { useContext, useEffect, useState } from 'react'
import { ReactSortable } from 'react-sortablejs'
import { toast } from 'react-toastify'

interface DragDropProps {
  handleOpenDialogNewTask(status: StatusTypes): void
  openDialogDeleteTask(taskId: number): void
}

type DataTypes = {
  [key: string]: {
    statusId: number
    id: number
    name: string
    description: string
    tags: TagTypes[]
    assingedTo?: UserTypes
  }[]
}

function DragDrop({
  handleOpenDialogNewTask,
  openDialogDeleteTask,
}: DragDropProps) {
  const [taskState, setTaskState] = useState<DataTypes>()
  const { setProjectData, projectData, activeSprint } =
    useContext(ProjectContext)
  const [initialStatusIds, setInitialStatusIds] = useState<number[]>([])

  useEffect(() => {
    generateInitialState()
  }, [projectData])

  useEffect(() => {
    if (projectData) {
      const currentStatusIds = projectData.project_data.status.map((item) => {
        return item.id
      })

      if (initialStatusIds.length === 0) {
        setInitialStatusIds(
          projectData?.project_data.status.map((item) => {
            return item.id
          })
        )
      }

      if (compareArray(currentStatusIds, initialStatusIds) === false) {
        handleUpdateStatus()
      }
    }
  }, [projectData])

  const generateInitialState = () => {
    const data: { [key: string]: any } = {}

    if (!projectData) return

    for (const iterator of projectData.project_data.status) {
      data[`status-${iterator.id}`] = []
    }

    for (
      let index = 0;
      index < projectData.project_data.tasks.length;
      index++
    ) {
      const element = projectData.project_data.tasks[index]

      if (
        element.sprint_id !== null &&
        activeSprint &&
        element.sprint_id === activeSprint.id
      ) {
        if (data[`status-${element.status.id}`]) {
          const assigned_to = projectData.users.find(
            (user) => user.id === element.assigned_to_user_id
          )

          data[`status-${element.status.id}`].push({
            statusId: element.status.id,
            id: element.id,
            name: element.name,
            description: element.description,
            tags: element.tags,
            assingedTo: assigned_to,
          })
        }
      }
    }

    setTaskState(data)
  }

  async function handleUpdateTask(currentId: string, toId: string) {
    const taskId = Number(currentId.split('task-')[1])
    const statusId = Number(toId.split('status-')[1])

    const taskService = new TaksService()

    try {
      const response = await taskService.update(taskId, { status_id: statusId })

      if (response.status === 200) {
        toast.success('Task updated successfully')
        setProjectData(
          (prev) =>
            prev && {
              ...prev,
              project_data: {
                ...prev.project_data,
                tasks: prev.project_data.tasks.map((item) =>
                  item.id === taskId
                    ? { ...item, status: response.data.status }
                    : item
                ),
              },
            }
        )
      }
    } catch (error) {
      toast.error('Task updated failed')
    }
  }

  async function handleUpdateStatus() {
    const requestData = projectData?.project_data.status.map((item, index) => {
      return { id: item.id, order: index + 1 }
    })

    try {
      // const response = await
    } catch (error) {}
  }

  return (
    <StyledPageContainer>
      {taskState && projectData && (
        <ReactSortable
          list={projectData.project_data.status}
          animation={150}
          setList={(newState) => {
            setProjectData(
              (prev) =>
                prev && {
                  ...prev,
                  project_data: { ...prev.project_data, status: newState },
                }
            )
          }}
          group="wrapper-cards"
          className="wrapper-items"
        >
          {projectData.project_data.status.map((status) => {
            return (
              <StyledItemWrapped
                key={status.id}
                id={`wrapper-status-${status.id}`}
              >
                <header>
                  <h4>{status.name}</h4>
                  <Plus
                    weight="bold"
                    onClick={() => handleOpenDialogNewTask(status)}
                    cursor="pointer"
                  />
                </header>

                <ReactSortable
                  list={taskState[`status-${status.id}`] || []}
                  setList={(newState) =>
                    setTaskState((prev) => ({
                      ...prev,
                      [`status-${status.id}`]: newState,
                    }))
                  }
                  animation={150}
                  id={`status-${status.id}`}
                  group="cards"
                  onEnd={(evt) => {
                    handleUpdateTask(evt.item.id, evt.to.id)
                  }}
                  className="items"
                >
                  {taskState[`status-${status.id}`]?.map((item) => (
                    <CardBoard
                      key={item.id}
                      taskId={item.id as number}
                      taskTags={item.tags}
                      taskName={item.name as string}
                      taskDescription={item.description}
                      assingedTo={item.assingedTo}
                      openDialogDeleteTask={openDialogDeleteTask}
                    />
                  ))}
                </ReactSortable>
              </StyledItemWrapped>
            )
          })}
        </ReactSortable>
      )}
    </StyledPageContainer>
  )
}

export default DragDrop

const StyledPageContainer = styled('div', {
  overflow: 'auto',
  paddingLeft: '$4',

  '.wrapper-items': {
    display: 'flex',
    gap: '3rem',
    paddingRight: '$4',
  },
})

const StyledItemWrapped = styled('section', {
  '& > header': {
    display: 'flex',
    justifyContent: 'space-between',
    py: '1.5rem',
  },

  '& .items': {
    minWidth: '21.1018125rem',
    minHeight: 'calc(100vh - 220px)',
    display: 'flex',
    flexDirection: 'column',
    gap: '1.5rem',
    backgroundColor: '#e6e6e633',
  },
})
