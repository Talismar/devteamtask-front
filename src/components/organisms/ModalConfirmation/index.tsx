import Button from '@/components/atoms/Button'
import ModalDefault from '@/components/molecules/ModalDefault'
import { styled } from '@/styles'
import { Check, X } from '@phosphor-icons/react'

interface ModalConfirmationProps {
  handleClose(): void
  handleSubmit(): void
  isOpen: boolean
  title: string
  subTitle: string
}

function ModalConfirmation({
  handleClose,
  handleSubmit,
  isOpen,
  title,
  subTitle,
}: ModalConfirmationProps) {
  return (
    <ModalDefault
      title={title}
      subTitle={subTitle}
      state={isOpen}
      handleClose={handleClose}
      content={
        <DivButtonsWrapper>
          <Button
            type="button"
            withIcon
            css={{ color: '$white100', backgroundColor: '$red400' }}
            onClick={handleClose}
          >
            <X size={16} /> No
          </Button>

          <Button
            type="button"
            withIcon
            css={{ color: '$white100', backgroundColor: '$blue400' }}
            onClick={handleSubmit}
          >
            <Check size={16} /> Yes
          </Button>
        </DivButtonsWrapper>
      }
    />
  )
}

export default ModalConfirmation

const DivButtonsWrapper = styled('div', {
  display: 'grid',
  gridTemplateColumns: '1fr 1fr',
  justifyContent: 'center',
  gap: '$4',
  minWidth: '400px',
})
