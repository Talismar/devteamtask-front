import Button from '@/components/atoms/Button'
import Input from '@/components/atoms/Input'
import PrimaryFormContainer from '@/components/molecules/PrimaryForm'
import { AuthContext } from '@/contexts/AuthContext'
import { UserService } from '@/services/UserService'
import userPasswordSettingsFormValidation from '@/validation/userPasswordSettingsFormValidation'
import { Check } from '@phosphor-icons/react'
import { AxiosError } from 'axios'
import { useContext } from 'react'
import { toast } from 'react-toastify'

function PasswordSettingsForm() {
  const { user } = useContext(AuthContext)
  const { formik } = userPasswordSettingsFormValidation({ handleSubmit })

  async function handleSubmit(values: typeof formik.values) {
    const userService = new UserService()
    try {
      const response = await userService.changePassword({
        old_password: values.old_password,
        new_password: values.new_password,
      })

      if (response.status === 200) {
        toast.success('Password updated successfully')
      }
    } catch (error) {
      if (error instanceof AxiosError) {
        if (error.response) {
          toast.warn(error.response.data.detail)
        }
      }
    }
  }

  return !(typeof user.auth_provider === 'string') ? (
    <PrimaryFormContainer
      title="Password Settings"
      onSubmit={formik.handleSubmit}
    >
      <Input
        labelText="Current Password"
        id="old_password"
        name="old_password"
        type="password"
        placeholder="Enter the password"
        autoComplete="current-password"
        onChange={formik.handleChange}
        onBlur={formik.handleBlur}
        value={formik.values.old_password}
        errorMessage={
          formik.errors && formik.touched && formik.errors.old_password
        }
      />

      <Input
        labelText="New Password"
        type="password"
        id="new_password"
        name="new_password"
        placeholder="Enter the new password"
        autoComplete="new-password"
        onChange={formik.handleChange}
        onBlur={formik.handleBlur}
        value={formik.values.new_password}
        errorMessage={
          formik.errors && formik.touched && formik.errors.new_password
        }
      />

      <Input
        labelText="Re-type Password"
        id="confirm_password"
        type="password"
        name="confirm_password"
        placeholder="Enter the new password again"
        autoComplete="new-password"
        onChange={formik.handleChange}
        onBlur={formik.handleBlur}
        value={formik.values.confirm_password}
        errorMessage={
          formik.errors && formik.touched && formik.errors.confirm_password
        }
      />

      <Button
        css={{
          backgroundColor: '$blue400',
          color: '$white100',
          alignSelf: 'flex-end',
        }}
        withIcon
        type="submit"
      >
        <Check size={24} />
        Save Changes
      </Button>
    </PrimaryFormContainer>
  ) : (
    <p>
      You do not have access to this feature. Your registration in the system is
      done by {user.auth_provider?.toUpperCase()}.
    </p>
  )
}

export default PasswordSettingsForm
