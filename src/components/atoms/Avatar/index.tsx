import { styled } from '@/styles'
import Image from 'next/image'

interface AvatarProps {
  size: 'sm' | 'md' | 'lg' | 'xs'
  src?: string | null | File
  firstLetter?: string
  withBorderBlue400?: boolean
  title?: string
}

function Avatar({
  size,
  src,
  title,
  firstLetter,
  withBorderBlue400,
}: AvatarProps) {
  return (
    <StyledAvatarWrapped
      size={size}
      withBorderBlue400={withBorderBlue400}
      title={title}
    >
      {src ? (
        <Image
          src={src instanceof File ? URL.createObjectURL(src) : src}
          alt="My photo"
          width={96}
          height={96}
          style={{ objectFit: 'cover' }}
        />
      ) : (
        <span>{firstLetter?.charAt(0).toUpperCase()}</span>
      )}
    </StyledAvatarWrapped>
  )
}

export default Avatar

const StyledAvatarWrapped = styled('div', {
  display: 'flex',
  alignItems: 'center',
  backgroundColor: '$gray300',
  borderRadius: '50%',
  overflow: 'hidden',

  '& span': {
    color: '$gray800',
    margin: 'auto',
  },

  variants: {
    size: {
      xs: {
        size: '1.5rem',
        span: {
          fontSize: '0.8rem',
        },
      },
      sm: {
        size: '2rem',
        span: {
          fontSize: '1rem',
        },
      },
      md: {
        size: '3rem',
        span: {
          fontSize: '1.5rem',
        },
      },
      lg: {
        size: '6rem',
      },
    },
    withBorderBlue400: {
      true: {
        border: '1px solid $blue400',
      },
    },
  },
})
