import { styled } from '@/styles'

const Button = styled('button', {
  borderWidth: 2,
  borderStyle: 'solid',
  cursor: 'pointer',
  transition: 'background-color 0.2s linear',

  variants: {
    size: {
      sm: {
        borderRadius: '4px',
        fontSize: '0.875rem',
        padding: '$2',
        fontWeight: 600,
      },
      md: {
        borderRadius: '8px',
        padding: '1rem 2.28125rem',
        fontWeight: 600,
      },
      lg: {
        padding: '0.25rem',
      },
    },
    padding: {
      sm: {
        padding: '0.5rem 2rem',
      },
    },
    variant: {
      solid: {
        color: '$white100',
        border: 'none',
        outlineColor: '$gray600',
      },
      ghost: {
        backgroundColor: 'transparent',
        border: 'none',
        color: '$blue400',
        '&:hover': {
          backgroundColor: '$blue200',
        },
      },
      outline: {
        backgroundColor: 'transparent !important',
      },
    },
    withIcon: {
      true: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        gap: '0.5rem',
      },
    },
  },

  defaultVariants: {
    variant: 'solid',
    size: 'md',
  },
})

export default Button
