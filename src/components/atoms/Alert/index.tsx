import { styled } from '@/styles'
import React from 'react'

interface AlertProps {
  children: string
}

function Alert({ children }: AlertProps) {
  return (
    <StyledContainer>
      <p>Note:</p>
      <span> {children}</span>
    </StyledContainer>
  )
}

export default Alert

const StyledContainer = styled('section', {
  backgroundColor: '$yellow200',
  padding: '0.75rem 1rem',
  fontSize: '0.75rem',
  textAlign: 'center',

  '& p': {
    display: 'inline-block',
    color: '$gray800',
  },

  '& span': {
    color: '$gray600',
  },
})
