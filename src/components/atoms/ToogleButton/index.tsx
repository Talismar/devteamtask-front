import { styled } from '@/styles'

interface ToogleButtonProps {
  state?: 'enabled' | 'disabled'
  onClick: () => void
}

function ToogleButton({ state, onClick }: ToogleButtonProps) {
  return (
    <SWrapper state={state === 'enabled'}>
      <SButton onClick={onClick} type="button" />
    </SWrapper>
  )
}

export default ToogleButton

const SWrapper = styled('div', {
  width: 48,
  height: 24,
  display: 'flex',
  borderRadius: 8,
  overflow: 'hidden',
  cursor: 'pointer',

  variants: {
    state: {
      true: {
        backgroundColor: '$green400',
        justifyContent: 'flex-end',
      },
      false: {
        backgroundColor: '$red400',
      },
    },
  },
})

const SButton = styled('button', {
  width: 24,
  height: 24,
  backgroundColor: '$white100',
  outline: 'none',
  borderRadius: 8,
  cursor: 'pointer',
})
