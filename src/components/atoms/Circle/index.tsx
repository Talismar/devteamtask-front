import { styled } from '@/styles'

const Circle = styled('div', {
  overflow: 'hidden',
  borderRadius: '50%',
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',

  variants: {
    size: {
      sm: {
        width: '3rem',
        height: '3rem',
      },
    },
  },

  defaultVariants: {
    size: 'sm',
  },
})

export default Circle
