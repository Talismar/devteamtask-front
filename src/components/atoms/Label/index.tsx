import { styled } from '@/styles'

const Label = styled('span', {
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
  gap: '0.25rem',
})

export default Label
