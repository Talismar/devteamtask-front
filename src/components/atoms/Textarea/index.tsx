import { TextareaTypes } from '@/@types/html'
import { styled } from '@/styles'
import React, { RefCallback } from 'react'

interface TextareaProps extends TextareaTypes {
  id: string
  labelText: string
  ref?: RefCallback<HTMLTextAreaElement>
  errorMessage?: string
}

function Textarea({
  errorMessage,
  id,
  ref,
  labelText,
  ...rest
}: TextareaProps) {
  return (
    <StyledLabel htmlFor={id}>
      <span>{labelText}</span>
      <div className={errorMessage ? 'input-wrapped' : ''}>
        <StyledTextarea
          withError={
            typeof errorMessage === 'string' && errorMessage.length > 0
          }
          ref={ref}
          id={id}
          {...rest}
        />
        {errorMessage && <p>{errorMessage}</p>}
      </div>
    </StyledLabel>
  )
}

const StyledTextarea = styled('textarea', {
  padding: '1rem',
  border: '1px solid $gray100',
  borderRadius: 4,
  color: '$gray700',
  minWidth: '100%',

  variants: {
    withError: {
      true: {
        borderColor: '$red400',
        '&:focus': {
          outline: 'none',
        },
      },
    },
  },
})

const StyledLabel = styled('label', {
  span: {
    display: 'block',
    color: '$gray700',
    fontWeight: 500,
    marginBottom: '0.5rem',
  },

  '& div.input-wrapped': {
    backgroundColor: '$red400',
    padding: 2,
    borderTopLeftRadius: 4,
    borderTopRightRadius: 4,
  },

  '& div.input-wrapped p': {
    color: '$white100',
    paddingTop: '0.250rem',
    paddingBottom: '0.250rem',
    fontSize: '0.75rem',
    marginLeft: '0.250rem',
    marginRight: '0.250rem',
    lineHeight: 1.5,
  },
})

export default Textarea
