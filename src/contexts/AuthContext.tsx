import { NotificationTypes } from '@/@types/notification'
import { UserTypes } from '@/@types/users'
import { useSignOut } from '@/hooks/useSignOut'
import { apiClient } from '@/libs/apiClient'
import { NotificationService } from '@/services/NotificationService'
import { setAuthCookiesByContext } from '@/utils/setAuthCookies'
import { AxiosResponse } from 'axios'
import Router, { useRouter } from 'next/router'
import { parseCookies, setCookie } from 'nookies'
import React, {
  Dispatch,
  SetStateAction,
  createContext,
  useEffect,
  useState,
} from 'react'
import { toast } from 'react-toastify'

type SignInCredentials = {
  email: string
  password: string
}

type AuthContextData = {
  signIn(
    credentials: SignInCredentials,
    redirectUrl: '/' | '/dashboard'
  ): Promise<AxiosResponse>
  isAuthenticated: boolean
  user: UserTypes
  setUser: Dispatch<SetStateAction<UserTypes>>
  notifications: NotificationTypes[]
  markNotificationAsRead(notificationId: number): void
  getAllNotifications(): Promise<void>
  getProfileImage(): string | undefined
}

const AuthContext = createContext({} as AuthContextData)

type AuthProviderProps = {
  children: React.ReactNode
}

function AuthProvider({ children }: AuthProviderProps) {
  const [user, setUser] = useState({} as UserTypes)
  const [notifications, setNotifications] = useState<NotificationTypes[]>([])
  const isAuthenticated = !!user.name
  const router = useRouter()

  useEffect(() => {
    const openRoutes = [
      '/reset-password',
      '/signup',
      '/forgot-password',
      '/signup',
      '/expired-link',
      '/',
      '/login',
      '/login-cli',
    ]
    const { '@dtt.accessToken': token, '@dtt.refreshToken': refresh } =
      parseCookies()

    if (openRoutes.includes(Router.router?.pathname || '')) {
      if (token && !isAuthenticated) {
        getUsersMe('/dashboard')
      }
    } else {
      if (token && !isAuthenticated) {
        getUsersMe(router.asPath)
      } else if (!refresh) {
        useSignOut('/login')
      }
    }
  }, [])

  useEffect(() => {
    if (typeof user.id === 'number') {
      getAllNotifications()
    }
  }, [user])

  async function getUsersMe(redirectUrl: string) {
    try {
      const response = await apiClient.get('/user/user/me')
      setUser(response.data)
      Router.push(redirectUrl)
    } catch (error) {
      toast.error('Error getting user informations')
      useSignOut('/login')
    }
  }

  async function signIn(
    data: SignInCredentials,
    redirectUrl: '/' | '/dashboard'
  ) {
    try {
      const responseToken = await (
        await apiClient.post('/user/authentication/token', data)
      ).data

      setCookie(null, '@dtt.accessToken', responseToken.access_token, {
        maxAge: 30 * 24 * 60 * 60,
        path: '/',
      })
      setCookie(null, '@dtt.refreshToken', responseToken.refresh_token, {
        maxAge: 30 * 24 * 60 * 60,
        path: '/',
      })

      setAuthCookiesByContext({
        access: responseToken.access_token,
        refresh: responseToken.refresh_token,
      })

      apiClient.defaults.headers.Authorization = `Bearer ${responseToken.access_token}`

      getUsersMe(redirectUrl)

      return responseToken
    } catch (error) {
      return Promise.reject(error)
    }
  }

  async function getAllNotifications() {
    const client_id = user.id
    const ws = new WebSocket(`ws://localhost:8002/ws/${client_id}`)

    ws.onerror = async (event) => {
      const notificationService = new NotificationService()

      try {
        const response = await notificationService.getAll()

        if (response.status === 200) {
          setNotifications(response.data)
        }
      } catch (error) {
        toast.error('Error while getting notifications.')
      }
    }

    ws.onmessage = function (event) {
      const text_content = event.data
      const object_content = JSON.parse(text_content)

      if (Array.isArray(object_content)) {
        setNotifications(object_content)
      } else {
        setNotifications((prev) => [...prev, object_content])
      }
    }
  }

  async function markNotificationAsRead(notificationId: number) {
    const notificationService = new NotificationService()
    try {
      const response = await notificationService.markAsRead(notificationId, {
        state: false,
      })
      if (!response.data.id) return
      setNotifications((prev) =>
        prev.filter((item) => item.id !== response.data.id)
      )
    } catch (error) {
      toast.error('Error.')
    }
  }

  function getProfileImage() {
    if (user.avatar_url !== null) {
      return user.avatar_url
    } else if (user.auth_provider === 'GITHUB') {
      return `https://avatars.githubusercontent.com/${user.name}`
    }
  }

  return (
    <AuthContext.Provider
      value={{
        signIn,
        isAuthenticated,
        user,
        setUser,
        notifications,
        markNotificationAsRead,
        getAllNotifications,
        getProfileImage,
      }}
    >
      {children}
    </AuthContext.Provider>
  )
}

export { AuthContext, AuthProvider }
