import {
  ProjectDetailsTypes,
  ProjectTypes,
  SprintTypes,
  TagTypes,
} from '@/@types/projects'
import { TaskTypes } from '@/@types/tasks'
import { ProjectService } from '@/services/ProjectService'
import { AxiosError } from 'axios'
import { useRouter } from 'next/router'
import {
  Dispatch,
  FC,
  ReactNode,
  SetStateAction,
  createContext,
  useEffect,
  useState,
} from 'react'
import { toast } from 'react-toastify'

type ProjectContextDataTypes = {
  projectData?: ProjectDetailsTypes
  activeSprint?: SprintTypes
  fetchProjectData(projectId: string): Promise<void>
  setProjectData: Dispatch<SetStateAction<ProjectDetailsTypes | undefined>>
  addNewTaskInProjectState(newTask: TaskTypes): void
  updateTaskInProjectState(
    updatedTask: Omit<TaskTypes, 'tag'> & { tags: number[] }
  ): void
  removeTaskInProjectState(taskId: number): void
  removeTagInProjectState(tagId: number): void
  finishSprintInProjectState(): void
}

interface ProjectProviderProps {
  children: ReactNode
}

const ProjectContext = createContext({} as ProjectContextDataTypes)

const ProjectProvider: FC<ProjectProviderProps> = ({ children }) => {
  const [projectData, setProjectData] = useState<ProjectDetailsTypes>()
  const [projectId, setProjectId] = useState('')
  const router = useRouter()
  const activeSprint = projectData?.project_data.sprints.find(
    (item) => item.state === 'IN PROGRESS'
  )

  useEffect(() => {
    const { projectId: id } = router.query as { projectId: string }

    if (id && id !== projectId) {
      setProjectId(id)
      fetchProjectData(id)
    } else if (typeof id === 'undefined') {
      setProjectId('')
      setProjectData(undefined)
    }
  }, [router.query])

  async function fetchProjectData(projectId: string) {
    const projectService = new ProjectService()
    try {
      const response = await projectService.getOne(projectId)
      setProjectData(response.data)
    } catch (error) {
      const isAxiosError = error instanceof AxiosError

      if (isAxiosError && error.response?.status === 404) {
        router.replace('/404')
      } else {
        toast.error("Couldn't get project data")
      }
    }
  }

  function addNewTaskInProjectState(newTask: TaskTypes) {
    setProjectData(
      (prev) =>
        prev && {
          ...prev,
          project_data: {
            ...prev.project_data,
            tasks: [...prev?.project_data.tasks, newTask],
          },
        }
    )
  }

  function removeTaskInProjectState(taskId: number) {
    setProjectData(
      (prev) =>
        prev && {
          ...prev,
          project_data: {
            ...prev.project_data,
            tasks:
              prev?.project_data.tasks.filter(
                (item) => item.id !== taskId && item
              ) ?? [],
          },
        }
    )
  }

  function finishSprintInProjectState() {
    if (!activeSprint) return
    const sprintSet = projectData?.project_data.sprints.map((item) => ({
      ...item,
      state: 'COMPLETED',
    }))
    setProjectData(
      (prev) =>
        prev && {
          ...prev,
          project_data: { ...prev.project_data, sprints: sprintSet || [] },
        }
    )
  }

  function updateTaskInProjectState(
    updatedTask: Omit<TaskTypes, 'tag'> & { tags: number[] }
  ) {
    if (!projectData) return

    const tags: TagTypes[] = []

    for (let index = 0; index < updatedTask.tags.length; index++) {
      const tagId = updatedTask.tags[index].id

      const tag = projectData.project_data.tags.find(
        (item) => item.id === tagId
      )

      if (tag) {
        tags.push(tag)
      }
    }

    setProjectData(
      (prev) =>
        prev && {
          ...prev,
          project_data: {
            ...prev.project_data,
            tasks: prev?.project_data.tasks.map((item) =>
              item.id === updatedTask.id ? { ...updatedTask, tags } : item
            ),
          },
        }
    )
  }

  function removeTagInProjectState(tagId: number) {
    setProjectData(
      (prev) =>
        prev && {
          ...prev,
          project_data: {
            ...prev.project_data,
            tags: prev.project_data.tags.filter((item) => item.id !== tagId),
          },
        }
    )
  }

  return (
    <ProjectContext.Provider
      value={{
        fetchProjectData,
        projectData,
        setProjectData,
        activeSprint,
        addNewTaskInProjectState,
        removeTaskInProjectState,
        updateTaskInProjectState,
        finishSprintInProjectState,
        removeTagInProjectState,
      }}
    >
      {children}
    </ProjectContext.Provider>
  )
}

export { ProjectContext, ProjectProvider }
