import { useState, useEffect, Dispatch, SetStateAction } from 'react'

function getStorageValue(key: string, defaultValue: any) {
  if (typeof window !== 'undefined') {
    const saved = localStorage.getItem(key)

    if (typeof saved === 'string') {
      const initial = JSON.parse(saved)

      return initial
    }

    return defaultValue
  }
}

type SetValue<T> = Dispatch<SetStateAction<T>>

export const useLocalStorage = <T>(
  key: string,
  defaultValue: T
): [T, SetValue<T>] => {
  const [value, setValue] = useState<T>(() => {
    return getStorageValue(key, defaultValue)
  })

  useEffect(() => {
    localStorage.setItem(key, JSON.stringify(value))
  }, [key, value])

  return [value, setValue]
}
