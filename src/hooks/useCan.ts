import { AuthContext } from '@/contexts/AuthContext'
import { useContext } from 'react'

type UseCanParams = {
  groups?: string[]
}

type GroupUserType = {
  id: number
  name: string
}

export function useCan({ groups }: UseCanParams) {
  const { user, isAuthenticated } = useContext(AuthContext)

  // const userGroups = user.groups as GroupUserType[]

  // if (!isAuthenticated) {
  //   return false
  // }

  // const hasPermission = validateUserGroups({
  //   userGroups,
  //   groups
  // })
  return true
}
