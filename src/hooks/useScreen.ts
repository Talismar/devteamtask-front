import { useEffect, useState } from 'react'

type UseScreenTypes = {
  screenWidth: number
  screenHeight: number
}

export function useScreen() {
  const [screen, setScreen] = useState<UseScreenTypes>({} as UseScreenTypes)

  const handleResize = () => {
    setScreen({
      screenWidth: window.innerWidth,
      screenHeight: window.innerHeight
    })
  }

  useEffect(() => {
    setScreen({
      screenWidth: window.innerWidth,
      screenHeight: window.innerHeight
    })
    window.addEventListener('resize', handleResize)

    return () => {
      window.removeEventListener('resize', handleResize)
    }
  }, [])

  return screen
}
