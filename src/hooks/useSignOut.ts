import clearAllCookies from '@/utils/clearAllCookies'
import Router from 'next/router'

type urlRedirectTypes = '/login' | '/'

function useSignOut(url: urlRedirectTypes) {
  clearAllCookies(undefined)

  Router.push(url)
}

export { useSignOut }
