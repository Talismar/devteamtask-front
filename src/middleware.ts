import { NextRequest, NextResponse } from 'next/server'

function deleteAuthCookies(response: NextResponse) {
  response.cookies.delete('@dtt.accessToken')
  response.cookies.delete('@dtt.refreshToken')
}

export function middleware(request: NextRequest) {
  const response = NextResponse.next({ request })
  // const response = NextResponse.redirect(
  //   new URL('/investigator/analysis/intra-rater', request.url)
  // )
  // get the cookies from the request
  // const cookieFromRequest = request.cookies['my-cookie']
  // set the `cookie`
  // response.cookies.set('hello', 'world')
  // // set the `cookie` with options
  // const cookieWithOptions = response.cookies.set('hello', 'world', {
  //   path: '/',
  //   maxAge: 1000 * 60 * 60 * 24 * 7,
  //   httpOnly: true,
  //   sameSite: 'strict',
  //   domain: 'example.com'
  // })
  // clear the `cookie`
  // console.log(request.cookies)
  // deleteAuthCookies(response)

  // deleteCookie(response, '@dtt.accessToken')

  return response
}

export const config = {
  matcher: [
    /*
     * Match all request paths except for the ones starting with:
     * - api (API routes)
     * - _next/static (static files)
     * - favicon.ico (favicon file)
     */
    '/((?!api|_next/static|favicon.ico).*)'
  ]
}
