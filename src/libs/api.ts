import { AuthTokenTypes } from '@/@types/authToken'
import { ContextTypes } from '@/@types/context'
import { useSignOut } from '@/hooks/useSignOut'
import { setAuthCookiesByContext } from '@/utils/setAuthCookies'
import axios, {
  AxiosError,
  AxiosInstance,
  AxiosRequestConfig,
  AxiosResponse,
} from 'axios'
import { parseCookies } from 'nookies'

type FailedRequestsQueueTypes = {
  onSuccess: (token: string) => void
  onFailure: (error: AxiosError) => void
}

let isRefreshing = false

let failedRequestsQueue: FailedRequestsQueueTypes[] = []

function getCookie(ctx: ContextTypes, keyName: string) {
  const cookies = parseCookies(ctx)

  return cookies[`@dtt.${keyName}`]
}

function responseWithSucess(response: AxiosResponse) {
  return response
}

function responseWithError(
  ctx: ContextTypes,
  error: AxiosError,
  axiosInstance: AxiosInstance
) {
  const { response } = error

  if (response?.status === 401) {
    if (response.statusText === 'Unauthorized') {
      const refreshToken = getCookie(ctx, 'refreshToken')
      const originalConfig = error.config as AxiosRequestConfig

      if (!isRefreshing) {
        isRefreshing = true

        axiosInstance
          .post('/user/authentication/refresh_token', {
            refresh_token: refreshToken,
          })
          .then((response) => {
            const { data } = response as { data: AuthTokenTypes }

            setAuthCookiesByContext(
              {
                access: data.access_token,
                refresh: data.refresh_token,
              },
              ctx
            )

            axiosInstance.defaults.headers.Authorization = `Bearer ${data.access_token}`

            failedRequestsQueue.forEach((request) => {
              return request.onSuccess(data.access_token)
            })
            failedRequestsQueue = []
          })
          .catch((err) => {
            failedRequestsQueue.forEach((request) => {
              return request.onFailure(err)
            })
            failedRequestsQueue = []
          })
          .finally(() => {
            isRefreshing = false
          })
      }

      return new Promise((resolve, reject) => {
        failedRequestsQueue.push({
          onSuccess: (token: string) => {
            if (originalConfig.headers) {
              originalConfig.headers.Authorization = `Bearer ${token}`
            }

            // Repetindo as requisições que deram errado apos o novo token ter sido renovado
            resolve(axiosInstance(originalConfig))
          },
          onFailure: (err: AxiosError) => {
            reject(err)
          },
        })
      })
    } else {
      // Deslogar o usuário
      if (!(typeof window === 'undefined')) {
        // eslint-disable-next-line react-hooks/rules-of-hooks
        useSignOut('/login')
      }
    }
  }

  return Promise.reject(error)
}

export function apiClient(
  ctx?: ContextTypes
  // redirectUrl?: '/login' | '/patient-access'
) {
  const accessToken = getCookie(ctx, 'accessToken')

  const axiosInstance = axios.create({
    baseURL: process.env.NEXT_PUBLIC_DTT_BACKEND_URL,
    headers: {
      Authorization: `Bearer ${accessToken}`,
    },
  })

  axiosInstance.interceptors.response.use(
    responseWithSucess,
    (response: AxiosError) => {
      return responseWithError(ctx, response, axiosInstance)
    }
  )

  return axiosInstance
}

export default apiClient
