import { globalCss } from '.'

const globalStyles = globalCss({
  '*': {
    margin: 0,
    padding: 0,
    boxSizing: 'border-box',
    fontFamily: "'Fira Sans', sans-serif",
  },

  'body, html': {
    fontSize: '16px',
    fontFamily: "'Fira Sans', sans-serif",
  },

  /* Setting Iphone */
  'input::-webkit-date-and-time-value': {
    textAlign: 'left',
  },

  'input:focus, textarea:focus': {
    outlineColor: '$blue400',
    backgroundColor: '#f6fbfe',
  },

  '::placeholder': {
    /* Chrome, Firefox, Opera, Safari 10.1+ */
    color: '$gray300',
    opacity: 1 /* Firefox */,
  },

  /* Internet Explorer 10-11 */
  ':-ms-input-placeholder': {
    color: '$gray300',
  },

  /* Microsoft Edge */
  '::-ms-input-placeholder': {
    color: '$gray300',
  },
})

export default globalStyles
