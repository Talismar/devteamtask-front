import { createStitches } from '@stitches/react'

type ColorsTypes =
  | '$blue400'
  | '$blue200'
  | '$orange400'
  | '$orange200'
  | '$green400'
  | '$red100'

export const {
  getCssText,
  theme,
  styled,
  globalCss,
  css,
  config,
  keyframes,
  createTheme,
} = createStitches({
  theme: {
    colors: {
      blue100: '#edf7fc',
      blue200: '#e3f3fb',
      blue300: '#c6e7f7',
      blue400: '#47b2e4',
      blue500: '#40a0cd',
      blue600: '#398eb6',

      blueDark300: '#4667A0',
      blueDark400: '#37517E',

      white100: '#FFFFFF',

      gray100: '#E6E6E6',
      gray300: '#B3B3B3',
      gray400: '#808080',
      gray500: '#666666',
      gray600: '#4C4C4C',
      gray700: '#333333',
      gray800: '#1A1A1A',

      orange200: '#FBECE4',
      orange400: '#E47947',
      yellow200: '#F0F2A6',
      red100: '#EB7A7A',
      red400: '#E44747',
      green400: '#47E457',
    },
    shadows: {
      blueDark300: '#4667A0',
      blueDark400: '#37517E',
    },
    fontSizes: {
      2: '0.5rem',
      4: '1rem',
      5: '1.5rem',
    },
    space: {
      2: '0.5rem',
      4: '1rem',
      6: '1.5rem',
      8: '2rem',
    },
  },
  utils: {
    mx: (value: number | string) => ({
      marginLeft: value,
      marginRight: value,
    }),
    my: (value: number) => ({
      marginTop: value,
      marginBottom: value,
    }),
    px: (value: number | string) => ({
      paddingLeft: value,
      paddingRight: value,
    }),
    py: (value: number | string) => ({
      paddingTop: value,
      paddingBottom: value,
    }),
    size: (value: number | string) => ({
      width: value,
      height: value,
    }),
    bg: (value: ColorsTypes) => ({
      backgroundColor: value,
    }),
  },
})
