export type NotificationTypes = {
  id: number
  title: string
  description: string
  state: boolean
  user: number
}
