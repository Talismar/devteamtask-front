export type UserTypes = {
  id: number
  name: string
  email: string
  avatar_url: string
  phone_number: string
  notification_state: boolean
  auth_provider: string | null
}
