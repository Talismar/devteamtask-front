export type AuthTokenTypes = {
  refresh_token: string
  access_token: string
}
