import { StatusTypes, TagTypes } from './projects'
import { UserTypes } from './users'

export type TaskTypes = {
  id: number
  name: string
  description: string
  priority: number
  status: StatusTypes
  created_at: string
  updated_at: string
  tags: TagTypes[] | null
  project_id: string
  sprint_id: null | number
  assigned_to_user_id?: number
  created_by_user_id: number
}
