import { GetServerSidePropsContext } from 'next'

export type ContextTypes = undefined | GetServerSidePropsContext
