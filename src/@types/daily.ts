export type DailyTypes = {
  id: number
  note: string
  created_at: string
  updated_at: string
  event_notes_id: number
}
