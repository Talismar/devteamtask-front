import { TaskTypes } from './tasks'

export type CollaboratorsTypes = {
  id: number
  name: string
  email: string
  avatar_url: string
}

export type StatusTypes = {
  id: number
  name: string
  order: number
}

export type TagTypes = {
  id: number
  name: string
}

export type SprintTypes = {
  id: number
  name: string
  description: string
  state: string
}

export type ProjectTypes = {
  id: string
  name: string
  state: string
  start_date: Date
  end_date: string
  sprint_set: SprintTypes[]
  collaborators: CollaboratorsTypes[]
  leader_id: number
  product_owner_id: string | null
  tags: TagTypes[]
  status: StatusTypes[]
  event_notes: number
  logo_url: string | null
  tasks_set: TaskTypes[]
}

export type ProjectDetailsTypes = {
  project_data: {
    id: string
    name: string
    state: string
    start_date: Date
    end_date: string
    logo_url: string | null
    leader_id: number
    product_owner_id: number | null
    collaborators_ids: number[]
    current_sprint_name: string
    sprints: SprintTypes[]
    tasks: TaskTypes[]
    tags: TagTypes[]
    status: StatusTypes[]
  }
  users: {
    id: number
    name: string
    avatar_url: string | null
    email: string
    auth_provider: string | null
  }[]
}

export type ProjectListTypes = {
  projects: {
    id: string
    name: string
    state: string
    start_date: Date
    end_date: string
    logo_url: string | null
    leader_id: number
    product_owner_id: string | null
    collaborators_ids: number[]
    current_sprint: SprintTypes | null
  }[]
  users: {
    id: number
    name: string
    avatar_url: string | null
    email: string
  }[]
}
