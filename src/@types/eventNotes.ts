export type EventNotesTypes = {
  id: number
  planning: string | null
  review: string | null
  retrospective: string | null
  sprint_id: number
}
