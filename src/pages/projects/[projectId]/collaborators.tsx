import Avatar from '@/components/atoms/Avatar'
import Button from '@/components/atoms/Button'
import Input from '@/components/atoms/Input'
import ModalDefault from '@/components/molecules/ModalDefault'
import ProjectNavBar from '@/components/molecules/NavBar/ProjectNavBar'
import SubHeader from '@/components/molecules/SubHeader'
import DefaultLayout from '@/components/organisms/DefaultLayout'
import { ProjectContext } from '@/contexts/ProjectContext'
import { ProjectService } from '@/services/ProjectService'
import { config, styled } from '@/styles'
import emailValidation from '@/validation/emailValidation'
import { Check, DotsThreeOutline, Plus } from '@phosphor-icons/react'
import { AxiosError } from 'axios'
import { GetServerSideProps } from 'next'
import Head from 'next/head'
import { useContext, useState } from 'react'
import { toast } from 'react-toastify'

interface ProjectCollaboratorsProps {
  projectId: string
  projectCollaborators: []
}

function ProjectCollaborators({
  projectCollaborators = [],
  projectId,
}: ProjectCollaboratorsProps) {
  const [dialog, setDialog] = useState(false)
  const { projectData } = useContext(ProjectContext)
  const [inputEmail, setInputEmail] = useState('')
  const [collaboratorsEmail, setCollaboratorsEmail] = useState<string[]>([])

  function handleOpenDialog() {
    setDialog(true)
  }

  function handleCloseDialog() {
    setDialog(false)
  }

  async function handleAddCollaboratorEmail() {
    const emailAlreadyExists = collaboratorsEmail.find(
      (item) => item === inputEmail
    )
    const isValidEmail = await emailValidation(inputEmail)

    if (inputEmail.trim().length > 0 && !emailAlreadyExists && isValidEmail) {
      setCollaboratorsEmail((prev) => [...prev, inputEmail])
      setInputEmail('')
    } else {
      toast.warning('Please enter a valid email')
    }
  }

  function handleRemoveCollaboratorEmail(email: string) {
    setCollaboratorsEmail((prev) => prev.filter((item) => item !== email))
  }

  async function handleSubmit() {
    const projectService = new ProjectService()
    try {
      await projectService.inviteCollaborators({
        project_id: projectId,
        emails: collaboratorsEmail,
      })

      toast.success('Successfully.')
      setDialog(false)
    } catch (error) {
      if (error instanceof AxiosError) {
        toast.error('Error')
      }
    }
  }

  return (
    <>
      <Head>
        <title>{`DDT | ${projectData?.project_data.name || 'Project'}`}</title>
      </Head>

      <ModalDefault
        title="Invite Users"
        subTitle="You can invite multiple users at once"
        state={dialog}
        handleClose={handleCloseDialog}
        content={
          <StyledForm>
            <StyledGridGroup>
              <Input
                id="collaborators"
                labelText="E-mail Address"
                type="email"
                name="collaborators"
                placeholder="Enter the e-mail of collaborators"
                value={inputEmail}
                onChange={(e) => setInputEmail(e.target.value)}
                tabIndex={1}
              />

              <div
                className="wrapped-btn"
                tabIndex={2}
                onClick={handleAddCollaboratorEmail}
                onKeyDown={handleAddCollaboratorEmail}
              >
                <Plus
                  size={20}
                  color={config.theme.colors.blue400}
                  weight="bold"
                />
              </div>
            </StyledGridGroup>

            {collaboratorsEmail.length > 0 && (
              <>
                {collaboratorsEmail.map((item) => (
                  <DivItemsContainer key={item}>
                    <div>
                      <Avatar
                        size="sm"
                        firstLetter={item.charAt(0).toUpperCase()}
                      />
                      <span>{item}</span>
                    </div>
                    <Button
                      size="sm"
                      css={{ backgroundColor: '$red400', color: '$white100' }}
                      onClick={() => handleRemoveCollaboratorEmail(item)}
                    >
                      Remove
                    </Button>
                  </DivItemsContainer>
                ))}
                <P>
                  <Span>{collaboratorsEmail.length} email</Span> address added
                  to invite, Click invite now to send invite
                </P>
              </>
            )}

            <Button
              variant="solid"
              size="md"
              onClick={handleSubmit}
              css={{ backgroundColor: '$blue400', color: '$white100' }}
              type="button"
              withIcon
            >
              <Check size={16} weight="bold" /> Invite Now
            </Button>
          </StyledForm>
        }
      />

      <DefaultLayout
        headerTitle="Projects / Boards"
        navbar={
          <ProjectNavBar pathActive="collaborators" projectId={projectId} />
        }
        content={
          <div>
            <SubHeader
              title="My Collaborators"
              paragraph={`${
                projectCollaborators.length ?? 0
              } Total collaborators are added`}
              complement={
                <Button
                  size="sm"
                  withIcon
                  css={{
                    backgroundColor: '$blue200',
                    alignSelf: 'flex-start',
                    color: '$blue400',
                  }}
                  onClick={handleOpenDialog}
                >
                  <Plus size={20} />
                  Collaborators
                </Button>
              }
            />

            <StyledCollaboratorsCardsContainer>
              {projectData?.project_data.collaborators_ids.map(
                (collaborator_id) => {
                  const collaborator = projectData.users.find(
                    (item) => item.id === collaborator_id
                  )
                  return (
                    <StyledCollaboratorCardWrapper key={collaborator!.id}>
                      <Avatar
                        size="md"
                        src={
                          collaborator!.avatar_url &&
                          `${
                            process.env.NEXT_PUBLIC_DTT_SERVICE_USER_URL
                          }/media/images/users/${collaborator!.avatar_url}`
                        }
                        firstLetter={collaborator!.name[0]}
                      />

                      <div className="text-wrapper">
                        <strong>{collaborator!.name}</strong>
                        <span>Web Development</span>
                      </div>

                      <DotsThreeOutline weight="fill" />
                    </StyledCollaboratorCardWrapper>
                  )
                }
              )}
            </StyledCollaboratorsCardsContainer>
          </div>
        }
      />
    </>
  )
}

export default ProjectCollaborators

const P = styled('p', {
  color: '$gray600',
})
const Span = styled('span', {
  color: '$blue400',
})

const StyledForm = styled('form', {
  display: 'flex',
  gap: '1.5rem',
  flexDirection: 'column',
  minWidth: '511px',
})

const StyledGridGroup = styled('div', {
  display: 'grid',
  gridTemplateColumns: '1fr auto',
  columnGap: '1rem',
  alignItems: 'end',

  'div.wrapped-btn': {
    backgroundColor: '$blue200',
    padding: '0.82rem',
    borderRadius: '.5rem',
    cursor: 'pointer',
  },
})

const DivItemsContainer = styled('div', {
  display: 'flex',
  alignItems: 'center',
  gap: '$4',
  justifyContent: 'space-between',
  borderBottom: '1px solid $gray100',
  paddingBottom: '1rem',

  '& div': {
    display: 'flex',
    gap: '0.5rem',
    alignItems: 'center',
  },
})

const StyledCollaboratorsCardsContainer = styled('section', {
  display: 'flex',
  gap: '1rem',
  flexWrap: 'wrap',
  marginBottom: '1.5rem',
  marginTop: '1rem',
})

const StyledCollaboratorCardWrapper = styled('div', {
  display: 'flex',
  gap: '1rem',
  alignItems: 'center',
  border: '1px solid $gray100',
  padding: '1rem',
  borderRadius: 12,

  'div.text-wrapper': {
    display: 'flex',
    flexDirection: 'column',
  },

  '& svg': {
    color: '$gray300',
    alignSelf: 'self-start',
    marginLeft: '1.5rem',
  },
})

export const getServerSideProps: GetServerSideProps = async (ctx) => {
  const { projectId } = ctx.params as { projectId: string }
  return {
    props: {
      projectId,
    },
  }
}
