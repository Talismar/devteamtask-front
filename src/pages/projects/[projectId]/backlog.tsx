import { TaskTypes } from '@/@types/tasks'
import Button from '@/components/atoms/Button'
import BackLogHeader from '@/components/molecules/BackLogHeader'
import ProjectNavBar from '@/components/molecules/NavBar/ProjectNavBar'
import ProductBacklogTable from '@/components/molecules/ProductBacklogTable'
import SprintBacklogTable from '@/components/molecules/SprintBacklogTable'
import SubHeader from '@/components/molecules/SubHeader'
import BacklogModalNewTask from '@/components/organisms/BacklogModalNewTask'
import DefaultLayout from '@/components/organisms/DefaultLayout'
import ModalConfirmation from '@/components/organisms/ModalConfirmation'
import ModalNewSprint from '@/components/organisms/ModalNewSprint'
import { AuthContext } from '@/contexts/AuthContext'
import { ProjectContext } from '@/contexts/ProjectContext'
import { SprintService } from '@/services/SprintService'
import { TaksService } from '@/services/TaskService'
import { config, styled } from '@/styles'
import { priorityFormatted } from '@/utils/priorityFormatted'
import backlogTaskValidation from '@/validation/backlogTaskValidation'
import { Plus } from '@phosphor-icons/react'
import { GetServerSideProps } from 'next'
import Head from 'next/head'
import { useContext, useState } from 'react'
import { toast } from 'react-toastify'

interface ProjectBacklogProps {
  projectId: string
}

const taskService = new TaksService()

function ProjectBacklog({ projectId }: ProjectBacklogProps) {
  const {
    projectData,
    setProjectData,
    activeSprint,
    removeTaskInProjectState,
    addNewTaskInProjectState,
    updateTaskInProjectState,
    finishSprintInProjectState,
  } = useContext(ProjectContext)
  const { user } = useContext(AuthContext)
  const [dialog, setDialog] = useState({
    newTask: false,
    newSprint: false,
    finishSprint: false,
  })
  const [isEdit, setIsEdit] = useState({ state: false, taskId: 0 })
  const { formik, isUpdating, handleUpdateInitialValues } =
    backlogTaskValidation({
      handleSubmit,
    })

  function handleSubmit(values: any) {
    if (isEdit.state) {
      handleUpdateTask(values)
    } else {
      handleCreateTask(values)
    }
  }

  function handleOpenDialog(
    dialog: 'newTask' | 'newSprint' | 'finishSprint',
    action?: 'update',
    data?: TaskTypes
  ) {
    if (dialog === 'newSprint' && activeSprint) {
      toast.warn('You cannot create sprint until you finish the current one')
      return
    }

    if (action === 'update' && data && projectData) {
      const assigned_to = projectData.users.find(
        (user) => user.id === data.assigned_to_user_id
      )

      handleUpdateInitialValues({
        taskName: data.name,
        taskDescription: data.description,
        addTaskIn:
          data.sprint_id !== null
            ? { label: 'Sprint backlog', value: 1 }
            : { label: 'Product backlog', value: 2 },
        taskCollaborator: assigned_to
          ? { label: assigned_to.email, value: assigned_to.id }
          : { label: '', value: 0 },
        taskStatus: data.status
          ? { label: data.status.name, value: data.status.id }
          : { label: '', value: 0 },
        taskPriority: {
          label:
            priorityFormatted.find((item) => item.value === data.priority)
              ?.label ?? '',
          value: data.priority,
        },
        taskTag:
          data.tags !== null
            ? data.tags.map((item) => ({ value: item.id, label: item.name }))
            : [],
      })
      setIsEdit({ state: true, taskId: data.id })
    } else {
      formik.resetForm()
    }
    setDialog((prev) => ({ ...prev, [dialog]: true }))
  }

  function handleCloseDialog(dialog: 'newTask' | 'newSprint' | 'finishSprint') {
    setDialog((prev) => ({ ...prev, [dialog]: false }))
    setIsEdit({ state: false, taskId: 0 })
  }

  async function handleChangeTaskBacklog(
    taskId: number,
    to: 'product_backlog' | 'sprint_backlog'
  ) {
    if (!activeSprint) {
      toast.warn(
        'It is not possible to perform this action, because there is no sprint started'
      )
      return
    }
    const taskService = new TaksService()
    const sprint_id = to === 'sprint_backlog' ? activeSprint.id : null

    try {
      const response = await taskService.update(taskId, {
        sprint_id: sprint_id,
      })
      if (response.status === 200) {
        toast.success('Task updated successfully')
        setProjectData(
          (prev) =>
            prev && {
              ...prev,
              project_data: {
                ...prev.project_data,
                tasks: prev.project_data.tasks.map((task) =>
                  task.id === taskId ? { ...task, sprint_id: sprint_id } : task
                ),
              },
            }
        )
      }
    } catch (error) {
      toast.error('Error')
    }
  }

  async function handleDeleteTask(taskId: number) {
    const taskService = new TaksService()
    try {
      const response = await taskService.deleteTask(taskId)

      if (response.status === 204) {
        toast.success('Task deleted successfully')
        removeTaskInProjectState(taskId)
      }
    } catch (error) {
      toast.error('Task deleted failed')
    }
  }

  async function handleCreateTask(values: typeof formik.values) {
    try {
      const response = await taskService.create({
        name: values.taskName,
        description: values.taskDescription,
        project_id: projectData?.project_data.id ?? '',
        priority: values.taskPriority.value,
        status_id: values.taskStatus.value,
        tags_ids: values.taskTag?.map((item: any) => item.value) ?? [],
        assigned_to_user_id:
          values.taskCollaborator !== null
            ? values.taskCollaborator.value
            : undefined,
        sprint_id:
          values.addTaskIn.label === 'Sprint backlog' && activeSprint?.id
            ? activeSprint.id
            : null,
      })

      if (response.status === 201) {
        toast.success('Project created successfully.')
        addNewTaskInProjectState(response.data)
        handleCloseDialog('newTask')
      }
    } catch (error) {
      toast.error('Error while created task')
    }
  }

  async function handleUpdateTask(values: typeof formik.values) {
    try {
      const response = await taskService.update(isEdit.taskId, {
        name: values.taskName,
        description: values.taskDescription,
        project_id: projectData?.project_data.id ?? '',
        priority: values.taskPriority.value,
        status_id: values.taskStatus.value,
        tags_ids: values.taskTag?.map((item: any) => item.value) || undefined,
        assigned_to_user_id:
          values.taskCollaborator === null
            ? values.taskCollaborator
            : values.taskCollaborator.value
              ? values.taskCollaborator.value
              : undefined,
      })

      if (response.status === 200) {
        toast.success('Project updated successfully.')
        updateTaskInProjectState(response.data)
        handleCloseDialog('newTask')
      }
    } catch (error) {
      console.log(error)

      toast.error('Error while updated task')
    }
  }

  async function handleFinishSprint() {
    if (!activeSprint) return
    const sprintService = new SprintService()
    try {
      const response = await sprintService.update(activeSprint.id, {
        state: 'COMPLETED',
      })
      if (response.status === 200) {
        toast.success(`Sprint ${activeSprint.name} completed successfully.`)
        handleCloseDialog('finishSprint')
        finishSprintInProjectState()
      }
    } catch (error) {
      toast.error('Error update sprint')
    }
  }

  return projectData ? (
    <>
      <Head>
        <title>{`DDT | ${projectData.project_data.name || 'Project'}`}</title>
      </Head>

      {projectData && (
        <>
          <BacklogModalNewTask
            projectData={projectData}
            isOpen={dialog.newTask}
            handleClose={() => handleCloseDialog('newTask')}
            formik={formik}
            isUpdating={isUpdating}
          />

          <ModalNewSprint
            isOpen={dialog.newSprint}
            handleClose={() => handleCloseDialog('newSprint')}
            projectData={projectData}
          />

          <ModalConfirmation
            isOpen={dialog.finishSprint}
            title="Finish Sprint"
            subTitle="Do you really want to finish this sprint now?"
            handleClose={() => handleCloseDialog('finishSprint')}
            handleSubmit={handleFinishSprint}
          />
        </>
      )}

      <DefaultLayout
        headerTitle="Projects / Backlog"
        navbar={<ProjectNavBar pathActive="backlog" projectId={projectId} />}
        content={
          <DivPageContent>
            <SubHeader
              title="Backlog"
              paragraph={`Create and complete tasks using list`}
            />

            {projectData && (
              <>
                {activeSprint && (
                  <>
                    <BackLogHeader
                      title="Sprint Backlog"
                      subTitle={activeSprint.name}
                      sprintDescription={activeSprint.description}
                      button={
                        user.id === projectData.project_data.leader_id ? (
                          <Button
                            variant="outline"
                            size="md"
                            css={{ borderColor: '$red400', color: '$red400' }}
                            onClick={() => handleOpenDialog('finishSprint')}
                          >
                            Finish Sprint
                          </Button>
                        ) : (
                          <></>
                        )
                      }
                    />

                    <SprintBacklogTable
                      activeSprint={activeSprint}
                      projectData={projectData}
                      openDialog={handleOpenDialog}
                      deleteTask={handleDeleteTask}
                      changeTaskBacklog={handleChangeTaskBacklog}
                    />
                  </>
                )}

                <BackLogHeader
                  title="Product Backlog"
                  subTitle="Add upcoming tasks here"
                  button={
                    user.id === projectData.project_data.leader_id &&
                    typeof activeSprint === 'undefined' ? (
                      <Button
                        variant="solid"
                        onClick={() => handleOpenDialog('newSprint')}
                        css={{ color: '$white100', bg: '$green400' }}
                        size="md"
                      >
                        Start Sprint
                      </Button>
                    ) : (
                      <></>
                    )
                  }
                />

                <ProductBacklogTable
                  projectData={projectData}
                  openDialog={handleOpenDialog}
                  deleteTask={handleDeleteTask}
                  changeTaskBacklog={handleChangeTaskBacklog}
                />
              </>
            )}

            <Button
              css={{
                position: 'fixed',
                bottom: '$4',
                right: '$6',
                backgroundColor: '$orange400',
                padding: '$4',
                borderRadius: '50%',
              }}
              withIcon
              onClick={() => handleOpenDialog('newTask')}
            >
              <Plus color={config.theme.colors.white100} size={24} />
            </Button>
          </DivPageContent>
        }
      />
    </>
  ) : null
}

export default ProjectBacklog

export const getServerSideProps: GetServerSideProps = async (ctx) => {
  const { projectId } = ctx.params as { projectId: string }
  return {
    props: {
      projectId,
    },
  }
}

const DivPageContent = styled('div', {
  display: 'grid',
  gap: '$4',
})
