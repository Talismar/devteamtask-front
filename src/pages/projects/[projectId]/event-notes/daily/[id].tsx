import { DailyTypes } from '@/@types/daily'
import Button from '@/components/atoms/Button'
import Header from '@/components/molecules/Header'
import ProjectNavBar from '@/components/molecules/NavBar/ProjectNavBar'
import { ProjectContext } from '@/contexts/ProjectContext'
import { DailyService } from '@/services/DailyService'
import { EventNoteService } from '@/services/EventNoteService'
import { styled } from '@/styles'
import { AxiosError } from 'axios'
import MarkdownIt from 'markdown-it'
import { GetServerSideProps } from 'next'
import Head from 'next/head'
import { useContext, useState } from 'react'
import MdEditor, { Plugins } from 'react-markdown-editor-lite'
import { toast } from 'react-toastify'

MdEditor.use(Plugins.BlockCodeBlock, {})

// Initialize a markdown parser
const mdParser = new MarkdownIt(/* Markdown-it options */)
interface EventNotesDailyItemProps {
  projectId: string
  dailyData: DailyTypes
}

function EventNotesDailyItem({
  projectId,
  dailyData,
}: EventNotesDailyItemProps) {
  const { projectData } = useContext(ProjectContext)
  const [text, setText] = useState(dailyData.note)

  function handleEditorChange({ html, text }: { html: string; text: string }) {
    setText(text)
  }

  async function handleSave() {
    const dailyService = new DailyService()
    try {
      const response = await dailyService.update(dailyData.id, { note: text })
      if (response.status === 200) {
        toast.success('Your plan has been successfully updated.')
      }
    } catch (error) {
      if (error instanceof AxiosError) {
        toast.error('Error updating.')
      }
    }
  }

  return (
    <>
      <Head>
        <title>{`DDT | ${projectData?.project_data.name || 'Project'}`}</title>
      </Head>
      <StyledPageContainer>
        <ProjectNavBar pathActive="event-notes" projectId={projectId} />

        <StyledContent>
          <Header title={`Projects / Event notes / Daily / New note`} />

          <StyledEditorContainer>
            <SubHeaderWrapped style={{ flexGrow: 3 }}>
              <h2>Daily</h2>
              <p>
                Create your sprint daily with <strong>markdown</strong> here
              </p>
            </SubHeaderWrapped>

            <MdEditor
              value={text}
              style={{ height: '100%', marginBottom: '1.5rem' }}
              renderHTML={(text) => mdParser.render(text)}
              onChange={handleEditorChange}
            />

            <StyledCustomButton
              onClick={handleSave}
              disabled={dailyData.note === text}
              variant="solid"
              css={{ bg: '$blue400' }}
            >
              Save note
            </StyledCustomButton>
          </StyledEditorContainer>
        </StyledContent>
      </StyledPageContainer>
    </>
  )
}

export default EventNotesDailyItem

export const getServerSideProps: GetServerSideProps = async (ctx) => {
  const { projectId, id: dailyid } = ctx.params as {
    projectId: string
    id: string
  }

  try {
    const dailyService = new DailyService(ctx)
    const response = await dailyService.getOne(dailyid)

    if (response.status !== 200) {
      return {
        notFound: true,
      }
    }

    return {
      props: {
        projectId,
        dailyData: response.data,
      },
    }
  } catch (error) {
    return {
      notFound: true,
    }
  }
}

// const WrappedPage = styled('div', {
//   minHeight: '100vh',
//   display: 'grid',
//   gridTemplateColumns: 'auto 1fr',
// })

// type ContextParamsTypes = {
//   projectId: string
//   id: string
// }

const StyledPageContainer = styled('div', {
  minHeight: '100vh',
  display: 'grid',
  gridTemplateColumns: 'auto 1fr',
})

const StyledEditorContainer = styled('div', {
  display: 'flex',
  flexDirection: 'column',
  padding: '0 1.5rem 1.5rem',
})

const StyledContent = styled('div', {
  display: 'grid',
  gridTemplateRows: 'auto 1fr',
})

const StyledCustomButton = styled(Button, {
  alignSelf: 'end',
})

const SubHeaderWrapped = styled('header', {
  marginBottom: '1.5rem',
  marginTop: '1rem',
})
