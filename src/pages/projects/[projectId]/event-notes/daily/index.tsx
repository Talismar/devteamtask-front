import { DailyTypes } from '@/@types/daily'
import { ProjectTypes } from '@/@types/projects'
import Button from '@/components/atoms/Button'
import ProjectNavBar from '@/components/molecules/NavBar/ProjectNavBar'
import SubHeader from '@/components/molecules/SubHeader'
import DefaultLayout from '@/components/organisms/DefaultLayout'
import { ProjectContext } from '@/contexts/ProjectContext'
import { DailyService } from '@/services/DailyService'
import { ProjectService } from '@/services/ProjectService'
import { styled } from '@/styles'
import { monthNames } from '@/utils/monthNames'
import { AxiosError } from 'axios'
import { GetServerSideProps } from 'next'
import Head from 'next/head'
import Link from 'next/link'
import { useRouter } from 'next/router'
import { useContext } from 'react'
import { toast } from 'react-toastify'

interface EventNotesDailyProps {
  projectId: string
  sprintId: number
  dailyData: DailyTypes[]
}

function EventNotesDaily({
  projectId,
  dailyData,
  sprintId,
}: EventNotesDailyProps) {
  const { projectData } = useContext(ProjectContext)
  const router = useRouter()

  async function handleNewNote() {
    const dailyService = new DailyService()
    try {
      const response = await dailyService.create({
        sprint_id: sprintId,
        note: '',
      })

      if (response.status === 201) {
        router.push(
          `/projects/${projectId}/event-notes/daily/${response.data.id}`
        )
        toast.success('New note successfully created.')
      }
    } catch (error) {
      toast.error('Error daily')
      // if (error instanceof AxiosError) {
      //   const key = Object.keys(error.response?.data)
      //   if (typeof error.response?.data[key[0]] === 'object') {
      //     toast.error(error.response?.data[key[0]][0])
      //   } else {
      //     toast.error(error.response?.data[key[0]])
      //   }
      // }
    }
  }

  return (
    <>
      <Head>
        <title>{`DDT | ${projectData?.project_data.name || 'Project'}`}</title>
      </Head>
      <DefaultLayout
        headerTitle="Projects / Event notes / Daily"
        navbar={
          <ProjectNavBar pathActive="event-notes" projectId={projectId} />
        }
        content={
          <StyledPageContainer>
            <SubHeader
              title="Daily"
              paragraph="Write down the main topics of today's meeting"
              complement={
                <Button onClick={handleNewNote} css={{ bg: '$blue400' }}>
                  New note
                </Button>
              }
            />

            <StyledDailyItemsContainer>
              {dailyData?.map((daily) => {
                const updatedDate = new Date(daily.updated_at)

                return (
                  <StyledDailyItemWrapper key={daily.id}>
                    <Link
                      href={`/projects/${projectId}/event-notes/daily/${daily.id}`}
                    >
                      <span>{`${
                        monthNames[updatedDate.getMonth()]
                      } ${updatedDate.getDate()}, ${updatedDate.getFullYear()}`}</span>
                      <p>{daily.note || 'Empty text.'}</p>
                    </Link>
                  </StyledDailyItemWrapper>
                )
              })}
            </StyledDailyItemsContainer>
          </StyledPageContainer>
        }
      />
    </>
  )
}

export default EventNotesDaily

export const getServerSideProps: GetServerSideProps = async (ctx) => {
  const { projectId } = ctx.params as { projectId: string }
  const { sprint_id } = ctx.query as { sprint_id: string }

  try {
    const dailyService = new DailyService(ctx)
    const response = await dailyService.getBySprintId(sprint_id)

    if (response.status !== 200) {
      return {
        notFound: true,
      }
    }

    return {
      props: {
        dailyData: response.data,
        sprintId: sprint_id,
        projectId,
      },
    }
  } catch (error) {
    return {
      notFound: true,
    }
  }
}

const StyledPageContainer = styled('div', {
  display: 'grid',
})

const StyledDailyItemsContainer = styled('section', {
  display: 'grid',
  gap: '$6',
})

const StyledDailyItemWrapper = styled('div', {
  '& a': {
    display: 'flex',
    flexDirection: 'column',
    gap: '$4',
    borderBottom: '1px solid black',
    textDecoration: 'none',

    '& span, p': {
      fontSize: '$4',
      lineHeight: '1.625rem',
    },

    '& span': {
      color: '$gray500',
    },

    '& p': {
      color: '$gray600',
      marginBottom: '$6',
    },
  },

  variants: {
    withoutBorderBottom: {
      true: {
        '& a': {
          border: 'none',
        },
      },
    },
  },
})
