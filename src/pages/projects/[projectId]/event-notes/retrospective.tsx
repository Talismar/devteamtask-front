/* eslint-disable @typescript-eslint/no-explicit-any */
import { EventNotesTypes } from '@/@types/eventNotes'
import EventNotesContent from '@/components/organisms/EventNotesContent'
import { ProjectContext } from '@/contexts/ProjectContext'
import { EventNoteService } from '@/services/EventNoteService'
import { GetServerSideProps } from 'next'
import Head from 'next/head'
import { useContext } from 'react'

interface EventNotesRetrospectiveProps {
  projectId: string
  eventNoteData: EventNotesTypes
}

function EventNotesRetrospective({
  projectId,
  eventNoteData,
}: EventNotesRetrospectiveProps) {
  const { projectData } = useContext(ProjectContext)

  return (
    <>
      <Head>
        <title>{`DDT | ${projectData?.project_data.name || 'Project'}`}</title>
      </Head>
      <EventNotesContent
        title="retrospective"
        projectId={projectId}
        eventNoteData={eventNoteData}
      />
    </>
  )
}

export default EventNotesRetrospective

export const getServerSideProps: GetServerSideProps = async (ctx) => {
  const { projectId } = ctx.params as { projectId: string }
  const { sprint_id } = ctx.query as { sprint_id: string }
  const eventNoteService = new EventNoteService(ctx)

  try {
    const response = await eventNoteService.getBySprintId(sprint_id)

    if (response.status !== 200) {
      return {
        notFound: true,
      }
    }

    return {
      props: {
        projectId,
        eventNoteData: response.data,
      },
    }
  } catch (error) {
    return {
      notFound: true,
    }
  }
}
