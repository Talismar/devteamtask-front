import Header from '@/components/molecules/Header'
import ProjectNavBar from '@/components/molecules/NavBar/ProjectNavBar'
import SubNavbar from '@/components/molecules/SubNavbar'
import IntegrationContent from '@/components/organisms/IntegrationContent'
import ProjectDetailForm from '@/components/organisms/ProjectDetailForm'
import { ProjectContext } from '@/contexts/ProjectContext'
import { useMultiStepForm } from '@/hooks/useMultiStepForm'
import { styled } from '@/styles'
import { GearSix, Plugs } from '@phosphor-icons/react'
import { GetServerSideProps } from 'next'
import Head from 'next/head'
import { useContext } from 'react'

interface ProjectSettingsProps {
  projectId: string
}

function ProjectSettings({ projectId }: ProjectSettingsProps) {
  const { projectData } = useContext(ProjectContext)
  const { goTo, step, currentStepIndex } = useMultiStepForm([
    <ProjectDetailForm key={1} />,
    <IntegrationContent key={2} />,
  ])

  return (
    <>
      <Head>
        <title>{`DDT | ${projectData?.project_data.name}`}</title>
      </Head>
      <WrappedPage>
        <ProjectNavBar pathActive="settings" projectId={projectId} />

        <div>
          <Header title="Projects / Settings" />

          <StyledFormContainer>
            <SubNavbar
              title="Project Settings"
              subTitle="You can find all settings here"
              navItems={[
                {
                  icon: GearSix,
                  name: 'General Settings',
                  isActive: currentStepIndex === 0,
                  onClick: () => goTo(0),
                },
                {
                  icon: Plugs,
                  name: 'Integrations',
                  isActive: currentStepIndex === 1,
                  onClick: () => goTo(1),
                },
              ]}
            />

            {step}
          </StyledFormContainer>
        </div>
      </WrappedPage>
    </>
  )
}

export default ProjectSettings

const WrappedPage = styled('div', {
  minHeight: '100vh',
  display: 'grid',
  gridTemplateColumns: 'auto 1fr',
})

export const getServerSideProps: GetServerSideProps = async (ctx) => {
  const { projectId } = ctx.params as { projectId: string }
  return {
    props: {
      projectId,
    },
  }
}

const StyledFormContainer = styled('div', {
  display: 'grid',
  gridTemplateColumns: '244px auto',
  columnGap: '$8',
  margin: '$6',
})
