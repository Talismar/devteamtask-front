import { StatusTypes } from '@/@types/projects'
import Button from '@/components/atoms/Button'
import Input from '@/components/atoms/Input'
import LoadingSpinner from '@/components/molecules/Loading'
import ModalDefault from '@/components/molecules/ModalDefault'
import ProjectNavBar from '@/components/molecules/NavBar/ProjectNavBar'
import DefaultLayout from '@/components/organisms/DefaultLayout'
import DragDrop from '@/components/organisms/DragDrop'
import ModalConfirmation from '@/components/organisms/ModalConfirmation'
import ModalNewTask from '@/components/organisms/ModalNewTask'
import { AuthContext } from '@/contexts/AuthContext'
import { ProjectContext } from '@/contexts/ProjectContext'
import { SprintService } from '@/services/SprintService'
import { StatusService } from '@/services/StatusService'
import { TaksService } from '@/services/TaskService'
import { styled } from '@/styles'
import boardTaskValidation, {
  BoardTaskFormikTypes,
} from '@/validation/boardTaskValidation'
import { Check, MagnifyingGlass } from '@phosphor-icons/react'
import { AxiosError } from 'axios'
import { GetServerSideProps } from 'next'
import Head from 'next/head'
import { useContext, useEffect, useState } from 'react'
import { toast } from 'react-toastify'

type DialogNewTaskTypes = {
  action: 'open' | 'close'
  status?: StatusTypes
}

const taskService = new TaksService()

function ProjectBoards() {
  const [dialogNewStatus, setDialogNewStatus] = useState(false)
  const [dialogNewTask, setDialogNewTask] = useState<DialogNewTaskTypes>()
  const [dialogFinishSprint, setDialogFinishSprint] = useState(false)
  const [searchTask, setSearchTask] = useState('')
  const [wasResearched, setWasResearched] = useState(false)
  const [dialogDeleteTask, setDialogDeleteTask] = useState({
    state: false,
    taskId: 0,
  })
  const [statusNameInput, setStatusNameInput] = useState('')
  const {
    projectData,
    setProjectData,
    activeSprint,
    addNewTaskInProjectState,
    finishSprintInProjectState,
    removeTaskInProjectState,
  } = useContext(ProjectContext)
  const { user } = useContext(AuthContext)
  const { formik } = boardTaskValidation({ handleSubmit: handleSubmitNewTask })

  useEffect(() => {
    let isFetching = false
    if (wasResearched && searchTask.trim().length === 0) {
      handleSearchTask('')
      isFetching = true
      setWasResearched(false)
    }
    return () => {
      if (wasResearched && !isFetching) {
        handleSearchTask('all')
      }
    }
  }, [wasResearched, searchTask])

  async function handleSubmitNewTask(values: BoardTaskFormikTypes) {
    try {
      const response = await taskService.create({
        name: values.taskName,
        description: values.taskDescription,
        project_id: projectData?.project_data.id ?? '',
        priority: values.taskPriority.value,
        status_id: values.taskStatus.value,
        tags_ids: values.taskTag?.map((item: any) => item.value) ?? [],
        sprint_id: activeSprint?.id,
        assigned_to_user_id:
          values.taskCollaborator.value !== 0
            ? values.taskCollaborator.value
            : undefined,
      })
      if (response.status === 201) {
        toast.success('Project created successfully.')
        addNewTaskInProjectState(response.data)
        handleCloseDialogNewTask()
        formik.resetForm()
      }
    } catch (error) {
      if (error instanceof AxiosError) {
        const key = Object.keys(error.response?.data)
        toast.error(error.response?.data[key[0]])
      }
    }
  }

  async function handleDeleteTask(taskId: number) {
    const taskService = new TaksService()
    try {
      const response = await taskService.deleteTask(taskId)

      if (response.status === 204) {
        toast.success('Task deleted successfully')
        removeTaskInProjectState(taskId)
      }
    } catch (error) {
      toast.error('Task deleted failed')
    }
  }

  async function handleSearchTask(taskName: string) {
    if (!projectData) return
    const taskService = new TaksService()

    try {
      const response = await taskService.getAllByProjectIdWithFilter(
        projectData.project_data.id,
        taskName === 'all' ? undefined : taskName
      )
      if (response.status === 200) {
        setProjectData(
          (prev) =>
            prev && {
              ...prev,
              project_data: {
                ...prev.project_data,
                tasks: response.data,
              },
            }
        )
      }
    } catch (error) {
      toast.error('Error search task')
    }
  }

  async function handleSubmitNewStatus() {
    if (!projectData) return
    if (statusNameInput.trim().length === 0) {
      toast.warn('Please enter a status name')
      return
    }

    const statusService = new StatusService()
    try {
      const response = await statusService.create({
        name: statusNameInput,
        order: projectData.project_data.status.length + 1,
        project_id: projectData.project_data.id ?? '',
      })

      if (response.status === 201) {
        setProjectData(
          (prev) =>
            prev && {
              ...prev,
              project_data: {
                ...prev.project_data,
                status: [...prev.project_data.status, response.data],
              },
            }
        )
        toast.success('New Board successfully added.')
        handleCloseDialogNewStatus()
      }
    } catch (error) {
      toast.error('Error while submitting new status')
    }
  }

  async function handleFinishSprint() {
    if (!activeSprint) return
    const sprintService = new SprintService()
    try {
      const response = await sprintService.update(activeSprint.id, {
        state: '2',
      })
      if (response.status === 200) {
        toast.success(`Sprint ${activeSprint.name} completed successfully.`)
        setDialogFinishSprint(false)
        finishSprintInProjectState()
      }
    } catch (error) {
      toast.error('Error update sprint')
    }
  }

  function handleCloseDialogNewStatus() {
    setDialogNewStatus(false)
  }

  function handleOpenDialogNewStatus() {
    setDialogNewStatus(true)
  }

  function handleCloseDialogNewTask() {
    setDialogNewTask({ action: 'close' })
  }

  function handleOpenDialogNewTask(status: StatusTypes) {
    setDialogNewTask({ action: 'open', status })
  }

  return projectData ? (
    <>
      <Head>
        <title>{`DDT | ${projectData.project_data.name || 'Project'}`}</title>
      </Head>

      <ModalDefault
        title="Create board"
        subTitle="Categorize your board to track each process"
        state={dialogNewStatus}
        handleClose={handleCloseDialogNewStatus}
        content={
          <StyledForm>
            <Input
              id="statusName"
              labelText="Board name"
              type="text"
              name="statusName"
              value={statusNameInput}
              onChange={(e) => setStatusNameInput(e.target.value)}
              placeholder="Enter the board name"
            />

            <Button
              variant="solid"
              size="md"
              onClick={handleSubmitNewStatus}
              css={{ bg: '$blue400' }}
              type="button"
              withIcon
            >
              <Check size={16} weight="bold" /> Create board
            </Button>
          </StyledForm>
        }
      />

      {dialogNewTask?.status && (
        <ModalNewTask
          isOpen={dialogNewTask.action === 'open'}
          statusDefaultValue={dialogNewTask.status}
          handleClose={handleCloseDialogNewTask}
          projectData={projectData}
          formik={formik}
        />
      )}

      <ModalConfirmation
        isOpen={dialogFinishSprint}
        title="Finish Sprint"
        subTitle="Do you really want to finish this sprint now?"
        handleClose={() => setDialogFinishSprint(false)}
        handleSubmit={() => {
          handleFinishSprint()
          setDialogFinishSprint(false)
        }}
      />

      <ModalConfirmation
        isOpen={dialogDeleteTask.state}
        title="Delete Task"
        subTitle="Do you really want to delete this task now?"
        handleClose={() => setDialogDeleteTask({ state: false, taskId: 0 })}
        handleSubmit={() => {
          handleDeleteTask(dialogDeleteTask.taskId)
          setDialogDeleteTask({ state: false, taskId: 0 })
        }}
      />

      <DefaultLayout
        headerTitle="Project / Boards"
        navbar={<ProjectNavBar projectId={projectData.project_data.id} />}
        content={
          <>
            <StyledHeader>
              <StyledSubHeader>
                <h1>Tasks Board</h1>
                <p>Create and complete tasks using boards</p>
              </StyledSubHeader>

              <StyledBoxInputAndButton>
                <StyledSearchInput
                  type="text"
                  placeholder="Search by task name..."
                  value={searchTask}
                  onChange={(e) => setSearchTask(e.target.value)}
                  tabIndex={1}
                />
                <StyledSwitchFilter
                  onClick={() => {
                    handleSearchTask(searchTask)
                    setWasResearched(true)
                  }}
                  onKeyDown={() => {
                    handleSearchTask(searchTask)
                    setWasResearched(true)
                  }}
                  tabIndex={2}
                >
                  <MagnifyingGlass size={24} />
                  {/* <p>Search</p> */}
                </StyledSwitchFilter>
                <Button
                  variant="outline"
                  size="md"
                  css={{ borderColor: '$blue400', color: '$blue400' }}
                  onClick={handleOpenDialogNewStatus}
                >
                  New Board
                </Button>

                {activeSprint &&
                  user.id === projectData.project_data.leader_id && (
                    <Button
                      variant="outline"
                      size="md"
                      css={{ borderColor: '$red400', color: '$red400' }}
                      onClick={() => setDialogFinishSprint(true)}
                    >
                      Finish Sprint
                    </Button>
                  )}
              </StyledBoxInputAndButton>
            </StyledHeader>

            <DragDrop
              handleOpenDialogNewTask={(status) => {
                activeSprint
                  ? handleOpenDialogNewTask(status)
                  : toast.warn(
                      'Create a sprint first so you can create tasks here.'
                    )
              }}
              openDialogDeleteTask={(taskId) =>
                setDialogDeleteTask({ state: true, taskId: taskId })
              }
            />
          </>
        }
      />
    </>
  ) : (
    <LoadingSpinner />
  )
}

export default ProjectBoards

export const getServerSideProps: GetServerSideProps = async (ctx) => {
  const { projectId } = ctx.params as { projectId: string }
  return {
    props: {
      projectId,
    },
  }
}

const StyledForm = styled('form', {
  display: 'flex',
  gap: '1.5rem',
  flexDirection: 'column',
  minWidth: '511px',
})

const StyledHeader = styled('header', {})

const StyledSubHeader = styled('div', {
  marginBottom: '2.875rem',
})

const StyledBoxInputAndButton = styled('section', {
  display: 'grid',
  gridTemplateColumns: '1fr auto auto auto',
  columnGap: '1rem',
})

const StyledSearchInput = styled('input', {
  borderRadius: 8,

  padding: '1rem',
  border: '2px solid $gray100',
})

const StyledSwitchFilter = styled('section', {
  display: 'flex',
  gap: '0.5rem',
  alignItems: 'center',
  backgroundColor: '$orange400',
  padding: '0.75rem 1rem',
  color: '$white100',
  borderRadius: 4,
  cursor: 'pointer',

  p: {
    fontWeight: 500,
  },
})
