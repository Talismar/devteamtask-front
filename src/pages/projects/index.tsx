import { ProjectListTypes } from '@/@types/projects'
import Button from '@/components/atoms/Button'
import SystemNavBar from '@/components/molecules/NavBar/SystemNavBar'
import SubHeader from '@/components/molecules/SubHeader'
import DefaultLayout from '@/components/organisms/DefaultLayout'
import ProjectTable from '@/components/organisms/ProjectTable'
import { ProjectService } from '@/services/ProjectService'
import Head from 'next/head'
import { useContext, useEffect, useState } from 'react'
import { toast } from 'react-toastify'

import LoadingSpinner from '@/components/molecules/Loading'
import ModalNewProject from '@/components/organisms/ModalNewProject'
import emailValidation from '@/validation/emailValidation'
import projectValidation, {
  ProjectFormikTypes,
} from '@/validation/projectValidation'
import { AxiosError } from 'axios'
import { AuthContext } from '@/contexts/AuthContext'

const projectService = new ProjectService()

function Projects() {
  const { formik } = projectValidation({ handleSubmit })
  const { user } = useContext(AuthContext)
  const [inputEmail, setInputEmail] = useState('')
  const [collaboratorsEmail, setCollaboratorsEmail] = useState<string[]>([])
  const [projectsTable, setProjectsTable] = useState<ProjectListTypes>({
    projects: [],
    users: [],
  })
  const [dialogNewProject, setDialogNewProject] = useState(false)
  const [isLoading, setIsLoading] = useState(false)

  async function handleSubmit(values: ProjectFormikTypes) {
    try {
      const response = await projectService.create({
        name: values.projectName,
        end_date: new Date(values.estimatedEndDate),
        product_owner_email: values.productOwner || undefined,
        collaborators_email: collaboratorsEmail,
      })

      if (response.status === 201) {
        toast.success('Project created successfully.')

        const currentUserIsOnProject = projectsTable.users.find(
          (item) => item.id === user.id
        )

        if (typeof currentUserIsOnProject === 'undefined') {
          setProjectsTable((prev) => ({
            users: [...prev.users, { ...user }],
            projects: [...prev.projects, response.data],
          }))
        } else {
          setProjectsTable((prev) => ({
            ...prev,
            projects: [...prev.projects, response.data],
          }))
        }

        handleCloseDialog()
        formik.resetForm()
      }
    } catch (error) {
      if (error instanceof AxiosError && error.status !== 422) {
        toast.error('Error creating project')
      }
    }
  }

  useEffect(() => {
    getProjects()
  }, [])

  function handleCloseDialog() {
    setDialogNewProject(false)
  }

  function handleOpenDialog() {
    setDialogNewProject(true)
  }

  async function getProjects() {
    setIsLoading(true)
    try {
      const response = await projectService.getAll()

      if (response.status === 200) {
        setProjectsTable(response.data)
      }
    } catch (error) {
      toast.error('Internal server error - Project')
    } finally {
      setIsLoading(false)
    }
  }

  function handleDeleteProjectInState(projectId: string) {
    setProjectsTable((prev) => ({
      ...prev,
      projects: prev.projects.filter((item) => item.id !== projectId),
    }))
  }

  async function handleAddCollaboratorEmail() {
    const emailAlreadyExists = collaboratorsEmail.find(
      (item) => item === inputEmail
    )
    const isValidEmail = await emailValidation(inputEmail)

    if (inputEmail.trim().length > 0 && !emailAlreadyExists && isValidEmail) {
      setCollaboratorsEmail((prev) => [...prev, inputEmail])
      setInputEmail('')
    } else {
      toast.warning('Please enter a valid email')
    }
  }

  function handleRemoveCollaboratorEmail(email: string) {
    setCollaboratorsEmail((prev) => prev.filter((item) => item !== email))
  }

  function handleChangeCollaboratorInputEmail(email: string) {
    setInputEmail(email)
  }

  return (
    <>
      <Head>
        <title>DTT | Projects</title>
      </Head>

      <ModalNewProject
        formik={formik}
        collaboratorInputEmail={inputEmail}
        collaboratorsEmail={collaboratorsEmail}
        dialogHandleClose={handleCloseDialog}
        dialogState={dialogNewProject}
        handleAddCollaboratorEmail={handleAddCollaboratorEmail}
        handleChangeCollaboratorInputEmail={handleChangeCollaboratorInputEmail}
        handleRemoveCollaboratorEmail={handleRemoveCollaboratorEmail}
      />

      <DefaultLayout
        navbar={<SystemNavBar pathActive="projects" />}
        content={
          <>
            <SubHeader
              title="My Projects"
              paragraph="Create and manager your projects"
              complement={
                <Button onClick={handleOpenDialog} css={{ bg: '$blue400' }}>
                  New Project
                </Button>
              }
            />

            {isLoading ? (
              <LoadingSpinner />
            ) : (
              <ProjectTable
                table={projectsTable}
                deleteProjectInState={handleDeleteProjectInState}
              />
            )}
          </>
        }
        headerTitle="Projects"
      />
    </>
  )
}

export default Projects
