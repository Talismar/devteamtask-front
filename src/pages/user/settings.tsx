import SystemNavBar from '@/components/molecules/NavBar/SystemNavBar'
import SubNavbar from '@/components/molecules/SubNavbar'
import DeactivationSettingsForm from '@/components/organisms/DeactivationSettingsForm'
import DefaultLayout from '@/components/organisms/DefaultLayout'
import NotificationSettingsForm from '@/components/organisms/NotificationSettingsForm'
import PasswordSettingsForm from '@/components/organisms/PasswordSettingsForm'
import UserDetailForm from '@/components/organisms/UserDetailForm'
import { useMultiStepForm } from '@/hooks/useMultiStepForm'
import { styled } from '@/styles'
import { Bell, GearSix, Password, User } from '@phosphor-icons/react'
import Head from 'next/head'

function UserSettings() {
  const { goTo, currentStepIndex } = useMultiStepForm([...Array(2).keys()])
  const formItems = [
    <UserDetailForm key={1} />,
    <PasswordSettingsForm key={2} />,
    <NotificationSettingsForm key={3} />,
    <DeactivationSettingsForm key={4} />,
  ]

  return (
    <>
      <Head>
        <title>User Settings | DTT</title>
      </Head>
      <DefaultLayout
        navbar={<SystemNavBar pathActive="dashboard" />}
        content={
          <div>
            <StyledFormContainer>
              <SubNavbar
                title="Settings"
                subTitle="You can find all settings here"
                navItems={[
                  {
                    name: 'General Settings',
                    isActive: currentStepIndex === 0,
                    icon: GearSix,
                    onClick: () => {
                      goTo(0)
                    },
                  },
                  {
                    name: 'Password Settings',
                    isActive: currentStepIndex === 1,
                    icon: Password,
                    onClick: () => {
                      goTo(1)
                    },
                  },
                  {
                    name: 'Notification Settings',
                    isActive: currentStepIndex === 2,
                    icon: Bell,
                    onClick: () => {
                      goTo(2)
                    },
                  },
                  {
                    name: 'Deactivation',
                    isActive: currentStepIndex === 3,
                    icon: User,
                    onClick: () => {
                      goTo(3)
                    },
                  },
                ]}
              />

              {formItems[currentStepIndex]}
            </StyledFormContainer>
          </div>
        }
        headerTitle="Settings"
      />
    </>
  )
}

export default UserSettings

const StyledFormContainer = styled('div', {
  display: 'grid',
  gridTemplateColumns: '244px auto',
  columnGap: '$8',
  margin: '$6',
})
