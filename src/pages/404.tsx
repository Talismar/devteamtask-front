import Button from '@/components/atoms/Button'
import { styled } from '@/styles'
import Image from 'next/image'
import { useRouter } from 'next/router'
import logoSvg from '../../public/icons/logo-white-sm.svg'

function Page404() {
  const router = useRouter()

  function handleGoToDashboard() {
    router.push('/dashboard')
  }

  return (
    <MainPageContainer>
      <DivContentWrapper>
        <DivImageWrapper>
          <Image src={logoSvg} alt="Logo" />
        </DivImageWrapper>

        <h1>Page not found</h1>

        <Button
          size="md"
          css={{ bg: '$blue400', width: '100%' }}
          onClick={handleGoToDashboard}
        >
          Go to Dashboard
        </Button>
      </DivContentWrapper>
    </MainPageContainer>
  )
}

export default Page404

const MainPageContainer = styled('main', {
  height: '100vh',
  backgroundColor: '$blueDark400',
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
})

const DivImageWrapper = styled('div', {
  padding: '$4',
  width: 'min-content',
  backgroundColor: '$blue400',
  borderRadius: 4,

  '& img': {
    width: '3.5rem',
    height: '3.44019rem',
  },
})

const DivContentWrapper = styled('div', {
  display: 'flex',
  alignItems: 'center',
  gap: '3.5rem',
  padding: '6rem 3.5rem 11rem 3.5rem',
  flexDirection: 'column',
  backgroundColor: '#3A5583',

  '& h1': {
    fontSize: '3rem',
    color: '$white100',
  },
})
