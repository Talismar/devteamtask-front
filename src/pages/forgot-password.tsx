import Alert from '@/components/atoms/Alert'
import Button from '@/components/atoms/Button'
import Input from '@/components/atoms/Input'
import {
  ResetPasswordRequestBodyTypes,
  UserService,
} from '@/services/UserService'
import { styled } from '@/styles'
import forgotPasswordValidation from '@/validation/forgotPasswordValidation'
import { Copyright } from '@phosphor-icons/react'
import { AxiosError } from 'axios'
import Image from 'next/image'
import { useRouter } from 'next/router'
import { toast } from 'react-toastify'
import logoSvg from '../../public/icons/logo-blue-md.svg'

function ForgotPassword() {
  const router = useRouter()
  const { formik, emailErrors } = forgotPasswordValidation({
    handleSubmit,
  })

  async function handleSubmit(values: ResetPasswordRequestBodyTypes) {
    const userService = new UserService()
    try {
      const response = await userService.resetPassword({ email: values.email })
      if (response.status === 200) {
        formik.resetForm()
        toast.success('Link sent successfully')
        router.push('/reset-password')
      }
    } catch (error) {
      if (error instanceof AxiosError) {
        toast.warn(error.response?.data.detail)
      }
    }
  }

  return (
    <StyledPageContainer>
      <StyledContentWrapped>
        <StyledHeader>
          <Image src={logoSvg} alt="Logo" />

          <div className="text-box">
            <h2>Forgot Password?</h2>
            <p>We’ll send new password link to email</p>
          </div>
        </StyledHeader>

        <StyledForm onSubmit={formik.handleSubmit}>
          <Input
            id="email"
            labelText="E-mail"
            placeholder="Enter the e-mail"
            name="email"
            value={formik.values.email}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            errorMessage={emailErrors}
          />

          <div className="flex flex-4 col">
            <Alert>Link expires in 1 hour</Alert>
            <Button
              variant="solid"
              size="md"
              css={{ bg: '$blue400' }}
              type="submit"
            >
              Send password link
            </Button>
          </div>
        </StyledForm>

        <StyledFooterWrapped>
          <Copyright size={20} />
          <p>DevTeamTask</p>
        </StyledFooterWrapped>
      </StyledContentWrapped>
    </StyledPageContainer>
  )
}

export default ForgotPassword

const StyledFooterWrapped = styled('footer', {
  display: 'flex',
  justifyContent: 'center',
  marginTop: '1.5rem',
  gap: '0.5rem',

  '& svg, p': {
    color: '$gray400',
  },
})

const StyledHeader = styled('header', {
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
  gap: '2.5rem',

  '& .text-box h2': {
    fontWeight: 600,
    color: '$gray800',
    fontSize: '2rem',
  },

  '& .text-box p': {
    textAlign: 'center',
    color: '$gray700',
  },
})

const StyledPageContainer = styled('div', {
  minHeight: '100vh',
  backgroundColor: '$blueDark400',
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
  padding: '1rem',
})

const StyledContentWrapped = styled('main', {
  backgroundColor: '$white100',
  padding: '3rem',
  borderTopLeftRadius: 24,
  borderBottomRightRadius: 24,
  display: 'flex',
  flexDirection: 'column',
  gap: '2.5rem',
})

const StyledForm = styled('form', {
  minWidth: '21.25rem',
  display: 'flex',
  flexDirection: 'column',
  gap: '2rem',
})
