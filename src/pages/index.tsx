import Button from '@/components/atoms/Button'
import Footer from '@/components/molecules/Footer'
import { styled } from '@/styles'
import { GetServerSideProps } from 'next'
import Head from 'next/head'
import Image from 'next/image'
import Link from 'next/link'
import { destroyCookie } from 'nookies'
import { useEffect } from 'react'
import { toast } from 'react-toastify'
import logoSvg from '../../public/icons/logo-blue-md.svg'
import illustration02Img from '../../public/imgs/Illustration-2-home.png'
import ddtDashboard from '../../public/imgs/ddt-dashboard.png'
import illustrationBoardImg from '../../public/imgs/img-illustration-board.png'
import illustrationScrumImg from '../../public/imgs/img-illustration-scrum.png'

interface HomeProps {
  error?: string
  success?: string
}

function Home({ error, success }: HomeProps) {
  useEffect(() => {
    if (error) {
      toast.error(error)
    } else if (success) {
      toast.success(success)
    }
  }, [])

  return (
    <>
      <Head>
        <title>Home</title>
      </Head>

      <StyledPageContainer>
        <StyledHeader>
          <nav>
            <Image src={logoSvg} alt="Logo" />

            <section className="group-button">
              <Link href="/login">
                <Button variant="ghost" size="sm">
                  Login
                </Button>
              </Link>

              <Link href="/signup">
                <Button variant="solid" size="sm" css={{ bg: '$blue400' }}>
                  Sign Up
                </Button>
              </Link>
            </section>
          </nav>
        </StyledHeader>

        <StyledMainContent>
          <div>
            <h1>DevTeamTask</h1>

            <p>
              It has never been so simple to deal with the task management of
              your development team.
            </p>
          </div>

          <Image
            src={ddtDashboard}
            alt="Img"
            width={720}
            quality={100}
            priority
          />
        </StyledMainContent>

        <StyledSecondarySection>
          <section className="content">
            <Image src={illustration02Img} alt="Img" />
            <div>
              <h2>Keep an organization with your team</h2>

              <p>
                Gather your project tasks in a shared space. Choose the view of
                project tasks that fits your style.
              </p>
            </div>
          </section>
        </StyledSecondarySection>

        <StyledThirdSection>
          <header>
            <h2>DevTeamTask is an essential tool for your project</h2>
            <h4>FEATURES</h4>
          </header>

          <StyledFeatureGroup>
            <StyledFeatureWrapped>
              <h3>Boards</h3>

              <Image
                src={illustrationBoardImg}
                alt="Illustration boards image"
              />

              <div>
                <p>
                  Make use of cards to visually indicate and track the progress
                  of tasks
                </p>
                <span>
                  A method of visually organizing work as it progresses over
                  time.
                </span>
              </div>
            </StyledFeatureWrapped>

            <div className="line-vertical" />

            <StyledFeatureWrapped headerColor="secondary">
              <h3>Scrum Methodology</h3>

              <Image
                src={illustrationScrumImg}
                alt="Illustration scrum image"
              />

              <div>
                <p>Assemble your team to work optimally and incrementally</p>
                <span>
                  A framework for managing teams. Used to self-organize and work
                  towards a common goal.
                </span>
              </div>
            </StyledFeatureWrapped>
          </StyledFeatureGroup>
        </StyledThirdSection>

        <Footer />
      </StyledPageContainer>
    </>
  )
}

export default Home

export const getServerSideProps: GetServerSideProps = async (ctx) => {
  const { error, detail_success } = ctx.req.cookies

  destroyCookie(ctx, 'error', { path: '/' })
  destroyCookie(ctx, 'detail_success', { path: '/' })

  return {
    props: {
      error: error ?? '',
      success: detail_success ?? '',
    },
  }
}

const StyledFeatureGroup = styled('div', {
  maxWidth: '1280px',
  display: 'grid',
  gridTemplateColumns: '1fr auto 1fr',
  margin: 'auto',
})

const StyledFeatureWrapped = styled('section', {
  display: 'flex',
  gap: '1.5rem',
  flexDirection: 'column',

  '& h3': {
    textAlign: 'center',
    padding: '0.75rem',
    color: '$white100',
  },

  '& p': {
    fontSize: '1.25rem',
    color: '$gray800',
    marginRight: '1.5rem',
  },

  '& span': {
    fontSize: '1.25rem',
    color: '$gray500',
  },

  img: {
    height: '335px',
    objectFit: 'cover',
    margin: 'auto',
  },

  variants: {
    headerColor: {
      primary: {
        '& h3': {
          backgroundColor: '$blue400',
        },
      },
      secondary: {
        '& h3': {
          backgroundColor: '$orange400',
        },
      },
    },
  },

  defaultVariants: {
    headerColor: 'primary',
  },
})

const StyledHeader = styled('header', {
  boxShadow: '0px 1px 1px #b3b3b3',
  backgroundColor: '$white100',

  nav: {
    maxWidth: '1280px',
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: '1rem',
    margin: 'auto',
  },

  'section.group-button': {
    display: 'flex',
    gap: '1rem',
  },
})

const StyledMainContent = styled('section', {
  display: 'flex',
  alignItems: 'center',
  maxWidth: '1280px',
  margin: 'auto',
  gap: '6rem',
  padding: '4rem 1rem',

  'div h1': {
    fontSize: '4rem',
    fontWeight: '800',
    letterSpacing: '4px',
    color: '$gray800',
  },

  'div p': {
    fontSize: '2rem',
    letterSpacing: '0.06rem',
    color: '$gray600',
    fontStyle: 'italic',
  },
})

const StyledSecondarySection = styled('section', {
  paddingTop: '4rem',
  paddingBottom: '4rem',
  display: 'flex',
  backgroundColor: '$blueDark400',
  color: '$white100',

  'section.content': {
    maxWidth: '1280px',
    paddingLeft: '1rem',
    paddingRight: '1rem',
    display: 'flex',
    alignItems: 'center',
    gap: '6rem',
    margin: 'auto',
  },

  'div h2': {
    fontSize: '3rem',
    letterSpacing: '2px',
    fontWeight: '600',
  },

  'div p': {
    fontStyle: 'italic',
    fontWeight: '300',
    fontSize: '1.5rem',
    lineHeight: '2.625rem',
    letterSpacing: '0.04em',
  },
})

const StyledThirdSection = styled('section', {
  display: 'flex',
  flexDirection: 'column',
  gap: '6rem',
  margin: 'auto',
  padding: '1rem 1rem 4.875rem 1rem',

  '& header': {
    textAlign: 'center',
    color: '$gray800',

    h2: {
      fontWeight: '600',
      fontSize: '2.25rem',
      lineHeight: '2.6875rem',
    },

    h4: {
      fontWeight: '300',
      fontSize: '2.25rem',
      lineHeight: '2.6875rem',
      marginTop: '1rem',
    },
  },
})

const StyledPageContainer = styled('div', {
  backgroundColor: '$white100',
})
