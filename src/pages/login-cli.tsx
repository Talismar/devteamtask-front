import Button from '@/components/atoms/Button'
import Input from '@/components/atoms/Input'
import { styled } from '@/styles'
import { Copyright } from '@phosphor-icons/react'
import axios from 'axios'
import Image from 'next/image'
import { useRouter } from 'next/router'
import { FormEvent, useState } from 'react'
import { toast } from 'react-toastify'
import logoSvg from '../../public/icons/logo-blue-md.svg'
import Alert from '@/components/atoms/Alert'
import githubSvg from '../../public/icons/github-md.svg'
import Head from 'next/head'

function LoginCLI() {
  const router = useRouter()
  const [formControl, setFormControl] = useState({ email: '', password: '' })

  function closeWindow() {
    var newWindow = window.open('', '_self', '')

    if (newWindow !== null) {
      newWindow.close()
    }
  }

  async function handleSubmit(event: FormEvent) {
    event.preventDefault()
    const { redirect } = router.query
    const API_URL = process.env.NEXT_PUBLIC_DTT_BACKEND_URL

    if (!redirect) {
      router.push('/404')
    } else {
      try {
        const response = await axios.post(
          `${API_URL}/user/authentication/token?redirect=${redirect}`,
          formControl
        )

        if (response.status === 200) {
          toast.success('Successfully authenticated 👋👋')
          setFormControl({ email: '', password: '' })
          setTimeout(() => {
            closeWindow()
          }, 5000)
        }
      } catch (error) {
        toast.error('An error occurred')
        toast.warning('Please try again')
      }
    }
  }

  function handleSignInGitHub() {
    const { redirect } = router.query

    if (!redirect) {
      router.push('/404')
    } else {
      const clientID = process.env.NEXT_PUBLIC_GITHUB_CLIENT_ID
      const authorizationEndpoint = 'https://github.com/login/oauth/authorize'
      const scope = 'user,repo'
      const redirectURI =
        'http://127.0.0.1:8000/api/user/authentication/github_auth'

      const githubAuthURL = `${authorizationEndpoint}?client_id=${clientID}&redirect_uri=${redirectURI}&scope=${encodeURIComponent(
        scope
      )}&state=${redirect}`

      window.location.href = githubAuthURL
    }
  }

  return (
    <>
      <Head>
        <title>Authentication CLI</title>
      </Head>

      <StyledPageContainer>
        <StyledContentWrapped>
          <StyledHeader>
            <Image src={logoSvg} alt="Logo" />

            <div className="text-box">
              <h2>Authentication CLI</h2>
              <p>Enter the your credentials</p>
            </div>
          </StyledHeader>
          <Alert>
            This window will close automatically when the authentication process
            completes successfully
          </Alert>

          <StyledFormWrapped onSubmit={handleSubmit}>
            <Input
              id="email"
              labelText="E-mail"
              placeholder="Enter the e-mail address"
              type="email"
              name="email"
              value={formControl.email}
              onChange={(event) =>
                setFormControl((prev) => ({
                  ...prev,
                  email: event.target.value,
                }))
              }
            />

            <Input
              labelText="Password"
              placeholder="Enter the password"
              type="password"
              id="password"
              name="password"
              value={formControl.password}
              onChange={(event) =>
                setFormControl((prev) => ({
                  ...prev,
                  password: event.target.value,
                }))
              }
            />

            <Button
              variant="solid"
              size="md"
              type="submit"
              css={{ bg: '$blue400' }}
            >
              Login
            </Button>
          </StyledFormWrapped>

          <Image
            src={githubSvg}
            alt="github icon"
            onClick={handleSignInGitHub}
            style={{ margin: 'auto', marginBottom: '-32px', cursor: 'pointer' }}
          />

          <StyledFooterWrapped>
            <Copyright size={20} />
            <p>DevTeamTask</p>
          </StyledFooterWrapped>
        </StyledContentWrapped>
      </StyledPageContainer>
    </>
  )
}

export default LoginCLI

const StyledFooterWrapped = styled('footer', {
  display: 'flex',
  justifyContent: 'center',
  marginTop: '1.5rem',
  gap: '0.5rem',

  '& svg, p': {
    color: '$gray400',
  },
})

const StyledHeader = styled('header', {
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
  gap: '2.5rem',

  '& .text-box h2': {
    fontWeight: 600,
    color: '$gray800',
    fontSize: '2rem',
  },

  '& .text-box p': {
    textAlign: 'center',
    color: '$gray700',
  },
})

const StyledPageContainer = styled('div', {
  minHeight: '100vh',
  backgroundColor: '$blueDark400',
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
  padding: '1rem',
})

const StyledContentWrapped = styled('main', {
  backgroundColor: '$white100',
  padding: '3rem',
  borderTopLeftRadius: 24,
  borderBottomRightRadius: 24,
  display: 'flex',
  flexDirection: 'column',
  gap: '2.5rem',
  width: 480,
})

const StyledFormWrapped = styled('form', {
  minWidth: '21.25rem',
  display: 'flex',
  flexDirection: 'column',
  gap: '2rem',
})
