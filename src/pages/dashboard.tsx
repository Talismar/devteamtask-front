import DashboardTaskCard from '@/components/molecules/DashboardTaskCard'
import SystemNavBar from '@/components/molecules/NavBar/SystemNavBar'
import DefaultLayout from '@/components/organisms/DefaultLayout'
import { TaksService } from '@/services/TaskService'
import { config, styled } from '@/styles'
import { At, CalendarBlank, File } from '@phosphor-icons/react'
import { ApexOptions } from 'apexcharts'
import { AxiosError } from 'axios'
import { GetServerSideProps } from 'next'
import dynamic from 'next/dynamic'
import Head from 'next/head'

const Chart = dynamic(() => import('react-apexcharts'), {
  ssr: false,
})

type DashboardDataTypes = {
  total_completed: number
  total_assigned: number
  total_scheduled: number
  total_completed_by_day_in_last_7_days: {
    date: string
    amount: number
  }[]
  total_pending_in_last_7_days: number
  total_task_in_last_7_days: number
}

interface DashboardProps {
  dashboardData: DashboardDataTypes
}

function Dashboard({ dashboardData }: DashboardProps) {
  const options: ApexOptions = {
    chart: {
      toolbar: {
        show: false,
      },
      zoom: {
        enabled: false,
      },
      foreColor: config.theme.colors.gray400,
    },
    grid: {
      // show: true,
      xaxis: {
        lines: {
          show: true,
        },
      },
    },
    dataLabels: {
      enabled: false,
    },
    tooltip: {
      enabled: false,
    },
    xaxis: {
      type: 'datetime',
      axisBorder: {
        color: config.theme.colors.gray600,
      },
      axisTicks: {
        color: config.theme.colors.gray600,
      },
      categories:
        dashboardData.total_completed_by_day_in_last_7_days?.map(
          (item) => item.date
        ) ?? [],
    },
    fill: {
      opacity: 0.3,
      type: 'gradient',
      gradient: {
        shade: 'drak',
        opacityFrom: 0.7,
        opacityTo: 0.3,
      },
    },
  }

  const series = [
    {
      name: 'Series1',
      data:
        dashboardData.total_completed_by_day_in_last_7_days?.map(
          (item) => item.amount
        ) ?? [],
    },
  ]

  return (
    <>
      <Head>
        <title>DevTeamTask | Dashboard</title>
      </Head>

      <DefaultLayout
        navbar={<SystemNavBar pathActive="dashboard" />}
        content={
          <DivContentWrapped>
            <DivCardsContainer>
              <DashboardTaskCard
                title="Completed Tasks"
                color="green"
                value={dashboardData.total_completed}
                icon={<File size={32} weight="bold" />}
              />
              <DashboardTaskCard
                title="Assigned Tasks"
                color="gray"
                value={dashboardData.total_assigned}
                icon={
                  <At
                    size={32}
                    weight="bold"
                    color={config.theme.colors.orange400}
                  />
                }
              />
              <DashboardTaskCard
                title="Scheduled Tasks"
                color="gray"
                value={dashboardData.total_scheduled}
                icon={
                  <CalendarBlank
                    size={32}
                    weight="bold"
                    color={config.theme.colors.orange400}
                  />
                }
              />
            </DivCardsContainer>

            <DivChartContainer>
              <DivChartWrapper>
                <DivChartHeader>
                  <strong>Total Tasks Done</strong>
                  <span>Tasks completed in last 7 days</span>
                </DivChartHeader>
                <Chart
                  type="area"
                  options={options}
                  series={series}
                  height={280}
                />
              </DivChartWrapper>

              <DivChartWrapper>
                <DivChartHeader>
                  <strong>Total Tasks</strong>
                  <span>Task statistics for the last 7 days</span>
                </DivChartHeader>
                <Chart
                  type="pie"
                  options={{
                    labels: ['Tasks', 'Completed', 'Pending'],
                    legend: { position: 'bottom' },
                  }}
                  series={[
                    dashboardData.total_task_in_last_7_days,
                    dashboardData.total_completed_by_day_in_last_7_days?.reduce(
                      (previuos, current) => {
                        return previuos + current.amount
                      },
                      0
                    ) || 0,
                    dashboardData.total_pending_in_last_7_days,
                  ]}
                  height={280}
                />
              </DivChartWrapper>
            </DivChartContainer>
          </DivContentWrapped>
        }
        headerTitle="Dashboard"
      />
    </>
  )
}

export default Dashboard

export const getServerSideProps: GetServerSideProps = async (ctx) => {
  const taskService = new TaksService(ctx)

  try {
    const response = await taskService.getDashboardData()

    return {
      props: {
        dashboardData: response.data,
      },
    }
  } catch (error) {
    if (error instanceof AxiosError) {
      console.log(error)
    }
    return {
      redirect: {
        destination: '/',
        permanent: false,
      },
    }
  }
}

const DivContentWrapped = styled('div', {
  display: 'grid',
  gridTemplateRows: 'auto auto',
  rowGap: '3rem',
  marginTop: '$8',
})

const DivCardsContainer = styled('div', {
  display: 'flex',
  gap: '$6',
  // maxWidth: 1150,
})

const DivChartContainer = styled('div', {
  display: 'grid',
  gridTemplateColumns: '3fr 1.5fr',
  columnGap: '$4',
})

const DivChartWrapper = styled('div', {
  display: 'flex',
  flexDirection: 'column',
  gap: '$6',
  padding: '$4',
  borderRadius: 8,
  border: '1px solid $gray100',
})

const DivChartHeader = styled('div', {
  display: 'flex',
  flexDirection: 'column',

  '& strong': {
    color: '$gray700',
  },

  '& span': {
    color: '$gray400',
  },
})
