import SignUpSideBar from '@/components/molecules/SignUpSideBar'
import SignUpDetailsForm from '@/components/organisms/SignUpDetailsForm'
import SignUpPasswordForm from '@/components/organisms/SignUpPasswordForm'
import { AuthContext } from '@/contexts/AuthContext'
import { useMultiStepForm } from '@/hooks/useMultiStepForm'
import { UserService } from '@/services/UserService'
import { styled } from '@/styles'
import signUpValidation, {
  SignUpFormikTypes,
} from '@/validation/signupValidation'
import { GetServerSideProps } from 'next'
import Image from 'next/image'
import Link from 'next/link'
import { destroyCookie, setCookie } from 'nookies'
import { useContext, useEffect } from 'react'
import { toast } from 'react-toastify'
import githubSvg from '../../public/icons/github-md.svg'
import { AxiosError } from 'axios'

interface SignUpProps {
  error: string
  success: string
}

function SignUp({ error, success }: SignUpProps) {
  const { signIn } = useContext(AuthContext)
  const { goTo, next, step } = useMultiStepForm([0, 1])
  const { formik } = signUpValidation({ handleSubmit })

  async function handleSubmit(values: SignUpFormikTypes) {
    const userService = new UserService()

    try {
      const response = await userService.create({
        name: values.fullName,
        email: values.email,
        password: values.password,
        password_confirm: values.password,
      })

      if (response.status === 201) {
        signIn({ email: values.email, password: values.password }, '/dashboard')
      }
    } catch (error) {
      const isAxiosError = error instanceof AxiosError
      if (isAxiosError) {
        toast.error(
          error.response?.data.detail ?? 'Error signing. Try again later.'
        )
      }
    }
  }

  function validateFirstStepForm() {
    return !(formik.errors.fullName || formik.errors.email)
  }

  useEffect(() => {
    if (error) {
      toast.error(error)
    } else if (success) {
      toast.success(success)
    }
  }, [])

  function handleSignInByGithub() {
    const clientID = process.env.NEXT_PUBLIC_GITHUB_CLIENT_ID
    const authorizationEndpoint = 'https://github.com/login/oauth/authorize'
    const redirectURI = 'http://127.0.0.1:8000/api/user/user/github'
    // const scope = 'user%20repo'&scope=${scope}
    const githubAuthURL = `${authorizationEndpoint}?client_id=${clientID}&redirect_uri=${redirectURI}`

    window.location.href = githubAuthURL
  }

  return (
    <MainPageContainer>
      <SignUpSideBar
        activeItem={step === 0 ? 'detailsForm' : 'passwordForm'}
        goToPage={goTo}
        completedDetails={step === 1}
      />

      <DivFormContainer>
        <DivFormWrapper>
          {step === 0 ? (
            <SignUpDetailsForm
              formik={formik}
              next={() => validateFirstStepForm() && next()}
            />
          ) : (
            <SignUpPasswordForm formik={formik} />
          )}

          {step === 0 && (
            <footer>
              <p>
                Already have an account? <Link href="/login">Login</Link>
              </p>
              <div>
                <span>Sign Up with</span>
              </div>

              <section className="group-icons">
                <Image
                  src={githubSvg}
                  alt="github icon"
                  onClick={handleSignInByGithub}
                />
              </section>
            </footer>
          )}
        </DivFormWrapper>

        <MultiStepContainer>
          <div className={`step-item ${step === 0 && 'active'}`} />
          <div className={`step-item ${step === 1 && 'active'}`} />
        </MultiStepContainer>
      </DivFormContainer>
    </MainPageContainer>
  )
}

export default SignUp

export const getServerSideProps: GetServerSideProps = async (ctx) => {
  const error = ctx.req.cookies.error
  const success = ctx.req.cookies.success
  const { access_token, refresh_token } = ctx.query as {
    access_token?: string
    refresh_token?: string
  }

  destroyCookie(ctx, 'error', { path: '/' })
  destroyCookie(ctx, 'success', { path: '/' })

  if (access_token && refresh_token) {
    setCookie(ctx, '@dtt.accessToken', access_token)
    setCookie(ctx, '@dtt.refreshToken', refresh_token)

    return {
      redirect: { destination: '/dashboard', permanent: false },
    }
  }

  return {
    props: { error: error || '', success: success || '' },
  }
}

const MultiStepContainer = styled('section', {
  display: 'flex',
  justifyContent: 'center',
  gap: '1rem',
  marginBottom: '$8',

  '& div.step-item': {
    width: '4.5rem',
    height: '0.375rem',
    borderRadius: '999px',
    backgroundColor: '$blue300',
  },
  '& div.active': {
    backgroundColor: '$blue400',
  },
})

const MainPageContainer = styled('main', {
  display: 'grid',
  gridTemplateColumns: 'auto 1fr',
})

const DivFormContainer = styled('div', {
  width: '100%',
  display: 'flex',
  flexDirection: 'column',
  justifyContent: 'space-between',
})

const DivFormWrapper = styled('div', {
  paddingTop: '7.625rem',
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',

  footer: {
    minWidth: '22.6875rem', // 363px
    display: 'flex',
    flexDirection: 'column',
    gap: '$8',
    marginTop: '$8',
  },
  'footer p': {
    color: '$gray700',
  },
  'footer p > a': {
    color: '$blue400',
    fontWeight: 600,
    textDecoration: 'none',
  },
  'footer > div': {
    display: 'flex',
    flexWrap: 'nowrap',
    justifyContent: 'center',
    alignItems: 'center',
  },
  'footer > div::before, div::after': {
    content: '',
    flex: '1 1 auto',
    borderBottom: 'solid 1px $gray700',
  },
  'footer > div > span': {
    flex: '0 1 auto',
    padding: '0 0.5rem 0 0.5rem',
    color: '$gray700',
  },

  'footer .group-icons': {
    display: 'flex',
    justifyContent: 'center',
    gap: '1rem',

    img: {
      cursor: 'pointer',
    },
  },
})
