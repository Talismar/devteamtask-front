/* eslint-disable @typescript-eslint/no-explicit-any */
import Button from '@/components/atoms/Button'
import Input from '@/components/atoms/Input'
import { AuthContext } from '@/contexts/AuthContext'
import { UserService } from '@/services/UserService'
import { styled } from '@/styles'
import resetPasswordValidation from '@/validation/resetPasswordValidation'
import { Copyright } from '@phosphor-icons/react'
import { AxiosError } from 'axios'
import Image from 'next/image'
import { useRouter } from 'next/router'
import { useContext } from 'react'
import { toast } from 'react-toastify'
import logoSvg from '../../public/icons/logo-blue-md.svg'

function ResetPassword() {
  const router = useRouter()
  const { signIn } = useContext(AuthContext)
  const { formik, passwordErrors, retypePasswordErrors } =
    resetPasswordValidation({ handleSubmit })

  async function handleSubmit(values: typeof formik.values) {
    const { token } = router.query
    const userService = new UserService()

    try {
      const response = await userService.changePasswordByToken({
        invite_token: token as string,
        password: values.password,
        password_confirm: values.retypePassword,
      })

      if (response.status === 200) {
        toast.success(response.data.message)
        signIn(
          { email: response.data.email, password: values.password },
          '/dashboard'
        )
      }
    } catch (error) {
      if (error instanceof AxiosError) {
        const { data } = error.response as { data: any }
        const keys = Object.keys(data)

        if (data[keys[0]] === 'Token is expired') {
          toast.error('Token is expired')
          router.push('/expired-link')
        } else {
          toast.error('Bad request.')
        }
      }
    }
  }

  return (
    <StyledPageContainer>
      <StyledContentWrapped>
        <StyledHeader>
          <Image src={logoSvg} alt="Logo" />

          <div className="text-box">
            <h2>Reset Password</h2>
            <p>Must be at least 8 characters</p>
          </div>
        </StyledHeader>

        <StyledFormWrapped onSubmit={formik.handleSubmit}>
          <Input
            id="new-password"
            labelText="New password"
            placeholder="Enter the password"
            type="password"
            name="password"
            value={formik.values.password}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            errorMessage={passwordErrors}
          />
          <Input
            labelText="Confirm new password"
            placeholder="Enter the password again"
            type="password"
            id="retypePassword"
            name="retypePassword"
            value={formik.values.retypePassword}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            errorMessage={retypePasswordErrors}
          />

          <Button
            variant="solid"
            size="md"
            type="submit"
            css={{ bg: '$blue400' }}
          >
            Reset
          </Button>
        </StyledFormWrapped>

        <StyledFooterWrapped>
          <Copyright size={20} />
          <p>DevTeamTask</p>
        </StyledFooterWrapped>
      </StyledContentWrapped>
    </StyledPageContainer>
  )
}

export default ResetPassword

const StyledFooterWrapped = styled('footer', {
  display: 'flex',
  justifyContent: 'center',
  marginTop: '1.5rem',
  gap: '0.5rem',

  '& svg, p': {
    color: '$gray400',
  },
})

const StyledHeader = styled('header', {
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
  gap: '2.5rem',

  '& .text-box h2': {
    fontWeight: 600,
    color: '$gray800',
    fontSize: '2rem',
  },

  '& .text-box p': {
    textAlign: 'center',
    color: '$gray700',
  },
})

const StyledPageContainer = styled('div', {
  minHeight: '100vh',
  backgroundColor: '$blueDark400',
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
  padding: '1rem',
})

const StyledContentWrapped = styled('main', {
  backgroundColor: '$white100',
  padding: '3rem',
  borderTopLeftRadius: 24,
  borderBottomRightRadius: 24,
  display: 'flex',
  flexDirection: 'column',
  gap: '2.5rem',
})

const StyledFormWrapped = styled('form', {
  minWidth: '21.25rem',
  display: 'flex',
  flexDirection: 'column',
  gap: '2rem',
})
