import Button from '@/components/atoms/Button'
import { styled } from '@/styles'
import { Copyright } from '@phosphor-icons/react'
import Image from 'next/image'
import { useRouter } from 'next/router'
import logoSvg from '../../public/icons/logo-blue-md.svg'

function ExpiredLink() {
  const router = useRouter()

  function handleGoToForgotPassword() {
    router.push('/forgot-password')
  }

  return (
    <StyledPageContainer>
      <StyledContentWrapped>
        <StyledHeader>
          <Image src={logoSvg} alt="Logo" />

          <div className="text-box">
            <h2>Reset Password</h2>
            <p>Link has expired</p>
          </div>
        </StyledHeader>

        <Button
          variant="solid"
          size="md"
          css={{ bg: '$blue400' }}
          onClick={handleGoToForgotPassword}
        >
          Try again
        </Button>

        <StyledFooterWrapped>
          <Copyright size={20} />
          <p>DevTeamTask</p>
        </StyledFooterWrapped>
      </StyledContentWrapped>
    </StyledPageContainer>
  )
}

export default ExpiredLink

const StyledFooterWrapped = styled('footer', {
  display: 'flex',
  justifyContent: 'center',
  marginTop: '1.5rem',
  gap: '0.5rem',

  '& svg, p': {
    color: '$gray400',
  },
})

const StyledHeader = styled('header', {
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
  gap: '2.5rem',

  '& .text-box h2': {
    fontWeight: 600,
    color: '$gray800',
    fontSize: '2rem',
  },

  '& .text-box p': {
    textAlign: 'center',
    color: '$gray700',
  },
})

const StyledPageContainer = styled('div', {
  minHeight: '100vh',
  backgroundColor: '$blueDark400',
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
  padding: '1rem',
})

const StyledContentWrapped = styled('main', {
  backgroundColor: '$white100',
  padding: '3rem',
  borderTopLeftRadius: 24,
  borderBottomRightRadius: 24,
  display: 'flex',
  flexDirection: 'column',
  gap: '2.5rem',
})
