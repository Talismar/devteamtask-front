import Image from 'next/image'
import { useContext, useEffect } from 'react'
import { styled } from '../styles'

import Button from '@/components/atoms/Button'
import Input from '@/components/atoms/Input'
import { AuthContext } from '@/contexts/AuthContext'
import { AxiosError } from 'axios'
import { useFormik } from 'formik'
import { GetServerSideProps } from 'next'
import Link from 'next/link'
import { useRouter } from 'next/router'
import { destroyCookie, setCookie } from 'nookies'
import { toast } from 'react-toastify'
import * as Yup from 'yup'
import githubSvg from '../../public/icons/github-md.svg'
import logoSvg from '../../public/icons/logo-blue-md.svg'
import illustrationPng from '../../public/imgs/login-illustration-right.png'

const signInSchema = Yup.object().shape({
  email: Yup.string().email().required('Required.'),
  password: Yup.string()
    .required('Password is required.')
    .min(6, 'Password must be at least 6 characters'),
})

interface LoginProps {
  error: string
}

function Login({ error }: LoginProps) {
  const { signIn } = useContext(AuthContext)
  const router = useRouter()

  const formik = useFormik({
    initialValues: {
      email: '',
      password: '',
    },
    validationSchema: signInSchema,
    onSubmit: async (values, { resetForm }) => {
      try {
        await signIn(
          { email: values.email, password: values.password },
          '/dashboard'
        )
        resetForm()
      } catch (error) {
        if (error instanceof AxiosError) {
          const key = Object.keys(error.response?.data)
          toast.error(error.response?.data[key[0]])
        }
      }
    },
  })

  function handleSignInGitHub() {
    const clientID = process.env.NEXT_PUBLIC_GITHUB_CLIENT_ID
    const authorizationEndpoint = 'https://github.com/login/oauth/authorize'
    const redirectURI =
      'http://127.0.0.1:8000/api/user/authentication/github_auth'
    const scope = 'user,repo'
    const githubAuthURL = `${authorizationEndpoint}?client_id=${clientID}&redirect_uri=${redirectURI}&scope=${encodeURIComponent(
      scope
    )}`

    window.location.href = githubAuthURL
  }

  function redirectToGitHubInstallation() {
    const clientId = process.env.NEXT_PUBLIC_GITHUB_CLIENT_ID
    const redirectUri = encodeURIComponent(
      'http://127.0.0.1:8001/api/project/integration/github/installation'
    )
    const scope = 'read:user,repo,write:repo_hook' // Especifique os escopos necessários

    const githubInstallURL = `https://github.com/apps/devteamtask/installations/new?state=SUA_INFORMACAO_DE_ESTADO&client_id=${clientId}&redirect_uri=${redirectUri}&scope=${scope}`

    window.location.href = githubInstallURL
  }

  function handleSignInGoogle() {
    // setCookie(undefined, 'DevTeamTask_REFERER', '/login', { path: '/' })
    // signInNextAuth('google', {
    //   callbackUrl: `${document.location.origin}/dashboard`,
    // })
    redirectToGitHubInstallation()
  }

  useEffect(() => {
    if (error) {
      toast.error(error)
    }
  }, [])

  return (
    <StyledPageContainer>
      <StyledLeftContainer>
        <Image src={logoSvg} alt="Logo svg" className="logo-svg" />

        <StyledFormContainer>
          <header>
            <h1>Sign in to DevTeamTask</h1>
            <p>Log in or register to start building your projects today.</p>
          </header>

          <StyledForm onSubmit={formik.handleSubmit}>
            <Input
              id="email"
              labelText="E-mail"
              type="email"
              name="email"
              autoComplete="username"
              placeholder="Enter the e-mail"
              value={formik.values.email}
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
              errorMessage={
                formik.touched.email && formik.errors.email
                  ? formik.errors.email
                  : ''
              }
            />

            <div className="box-imput">
              <Input
                id="password"
                labelText="Password"
                type="password"
                name="password"
                autoComplete="current-password"
                placeholder="Enter the password"
                value={formik.values.password}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                errorMessage={
                  formik.touched.password && formik.errors.password
                    ? formik.errors.password
                    : ''
                }
              />

              <Link href="/forgot-password">Forgot password?</Link>
            </div>

            <Button
              css={{ backgroundColor: '$blue400', color: '$white100' }}
              type="submit"
            >
              LOGIN
            </Button>
          </StyledForm>

          <footer>
            <p>
              {"Don't"} have an account? <Link href="/signup">Sign Up</Link>
            </p>

            <div>
              <span>Log in with</span>
            </div>

            <section className="group-icons">
              <Image
                src={githubSvg}
                alt="github icon"
                onClick={handleSignInGitHub}
              />
            </section>
          </footer>
        </StyledFormContainer>
      </StyledLeftContainer>
      <StyledRightContainer>
        <Image src={illustrationPng} alt="Illustration" />
      </StyledRightContainer>
    </StyledPageContainer>
  )
}

export default Login

export const getServerSideProps: GetServerSideProps = async (ctx) => {
  const { access_token, refresh_token } = ctx.query as {
    access_token?: string
    refresh_token?: string
  }
  const { error } = ctx.req.cookies

  if (access_token && refresh_token) {
    setCookie(ctx, '@dtt.accessToken', access_token)
    setCookie(ctx, '@dtt.refreshToken', refresh_token)

    return {
      redirect: { destination: '/dashboard', permanent: false },
    }
  }

  destroyCookie(ctx, 'error', { path: '/' })
  destroyCookie(ctx, 'DevTeamTask_REFERER')

  return {
    props: { error: error || '' },
  }
}

const StyledPageContainer = styled('div', {
  height: '100vh',
  display: 'grid',
  gridTemplateColumns: '1fr 1fr',
})

const StyledLeftContainer = styled('section', {
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',

  'img.logo-svg': {
    position: 'absolute',
    left: '2rem',
    top: '2rem',
  },
})

const StyledFormContainer = styled('main', {
  maxWidth: '24.3125rem',
  display: 'flex',
  flexDirection: 'column',
  gap: '2rem',

  'header h1': {
    fontSize: '2rem',
    color: '$gray800',
    marginBottom: '1rem',
  },
  'header p': {
    color: '$gray700',
  },
  footer: {
    display: 'flex',
    flexDirection: 'column',
    gap: '1.5rem',
  },
  'footer p': {
    color: '$gray700',
  },
  'footer p > a': {
    color: '$blue400',
    fontWeight: 600,
    textDecoration: 'none',
  },
  'footer > div': {
    display: 'flex',
    flexWrap: 'nowrap',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: '1rem',
  },
  'footer > div::before, div::after': {
    content: '',
    flex: '1 1 auto',
    borderBottom: 'solid 1px $gray700',
  },
  'footer > div > span': {
    flex: '0 1 auto',
    padding: '0 0.5rem 0 0.5rem',
    color: '$gray700',
  },

  'footer .group-icons': {
    display: 'flex',
    justifyContent: 'center',
    gap: '1rem',

    img: {
      cursor: 'pointer',
    },
  },
})

const StyledForm = styled('form', {
  display: 'flex',
  flexDirection: 'column',
  gap: '1rem',

  '& .box-imput a:last-child': {
    color: '$blue400',
    marginTop: '0.5rem',
    fontWeight: 600,
    display: 'block',
    cursor: 'pointer',
    textAlign: 'right',
    fontSize: '0.875rem',
    textDecoration: 'none',
  },
})

const StyledRightContainer = styled('section', {
  backgroundColor: '$blueDark400',
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',

  img: {
    width: '100%',
    height: 'auto',
    objectFit: 'cover',
  },
})
