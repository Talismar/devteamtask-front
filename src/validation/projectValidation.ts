import { useFormik } from 'formik'
import { useCallback, useState } from 'react'
import * as Yup from 'yup'

const formSchema = Yup.object().shape({
  projectName: Yup.string().required('Required.'),
  estimatedEndDate: Yup.date()
    .required('Required.')
    .min(
      new Date(new Date().setDate(new Date().getDate() - 1)),
      "Must be equal to or after today's date."
    ),
  productOwner: Yup.string().email('Must be a valid email.'),
})

export type ProjectFormikTypes = Omit<
  Yup.InferType<typeof formSchema>,
  'estimatedEndDate'
> & {
  estimatedEndDate: string
}

type ProjectValidationTypes = {
  handleSubmit(values: ProjectFormikTypes): void
}

const projectValidation = ({ handleSubmit }: ProjectValidationTypes) => {
  const [initialValues, setInitialValues] = useState<ProjectFormikTypes>({
    projectName: '',
    estimatedEndDate: '',
    productOwner: '',
  })

  const formik = useFormik({
    initialValues: initialValues,
    validationSchema: formSchema,
    onSubmit: (values) => {
      handleSubmit(values)
    },
  })

  const handleUpdateInitialValues = useCallback(
    (values: ProjectFormikTypes) => {
      setInitialValues(values)
    },
    []
  )

  const projectNameErrors =
    formik.errors.projectName && formik.touched.projectName
      ? formik.errors.projectName
      : ''

  return {
    formik,
    handleUpdateInitialValues,
    projectNameErrors,
  }
}

export default projectValidation
