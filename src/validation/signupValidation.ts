import { phoneRegExp } from '@/utils/regexValidation'
import { useFormik } from 'formik'
import { useCallback, useState } from 'react'
import * as Yup from 'yup'

const formSchema = Yup.object().shape({
  fullName: Yup.string().required('Full name is a required field.'),
  email: Yup.string().email().required('E-mail is a required field.'),
  phoneNumber: Yup.string()
    .matches(phoneRegExp, 'Phone number is not valid')
    .min(10, 'too short')
    .max(10, 'too long'),
  password: Yup.string()
    .min(6)
    .max(6)
    .required('Password is a required field.'),
  confirmPassword: Yup.string()
    .required('Please retype your password.')
    .oneOf([Yup.ref('password')], 'Your passwords do not match.'),
})

export type SignUpFormikTypes = Yup.InferType<typeof formSchema>
type SignUpValidationTypes = {
  handleSubmit(values: SignUpFormikTypes): void
}

const signUpValidation = ({ handleSubmit }: SignUpValidationTypes) => {
  const [initialValues, setInitialValues] = useState<SignUpFormikTypes>({
    fullName: '',
    email: '',
    phoneNumber: '',
    password: '',
    confirmPassword: '',
  })

  const formik = useFormik({
    initialValues: initialValues,
    validationSchema: formSchema,
    onSubmit: (values) => {
      handleSubmit(values)
    },
  })

  const handleUpdateInitialValues = useCallback((values: SignUpFormikTypes) => {
    setInitialValues(values)
  }, [])

  const emailErrors =
    formik.errors.email && formik.touched.email ? formik.errors.email : ''

  return {
    formik,
    handleUpdateInitialValues,
    emailErrors,
  }
}

export default signUpValidation
