import { useFormik } from 'formik'
import { useCallback, useState } from 'react'
import * as Yup from 'yup'

const formSchema = Yup.object().shape({
  email: Yup.string()
    .email('Email must be a valid email')
    .required('Email is a required field.'),
})

export type ForgotPasswordFormikTypes = Yup.InferType<typeof formSchema>
type ForgotPasswordValidationTypes = {
  handleSubmit(values: ForgotPasswordFormikTypes): void
}

const forgotPasswordValidation = ({
  handleSubmit,
}: ForgotPasswordValidationTypes) => {
  const [initialValues, setInitialValues] = useState<ForgotPasswordFormikTypes>(
    { email: '' }
  )

  const formik = useFormik({
    initialValues: initialValues,
    validationSchema: formSchema,
    onSubmit: (values) => {
      handleSubmit(values)
    },
  })

  const handleUpdateInitialValues = useCallback(
    (values: ForgotPasswordFormikTypes) => {
      setInitialValues(values)
    },
    []
  )

  const emailErrors =
    formik.errors.email && formik.touched.email ? formik.errors.email : ''

  return {
    formik,
    handleUpdateInitialValues,
    emailErrors,
  }
}

export default forgotPasswordValidation
