import { useFormik } from 'formik'
import { useCallback, useState } from 'react'
import * as Yup from 'yup'

const formSchema = Yup.object().shape({
  taskName: Yup.string().required('Task name is a required field'),
  taskDescription: Yup.string(),
  taskPriority: Yup.object()
    .shape({
      label: Yup.string().required(),
      value: Yup.number().required(),
    })
    .required(),
  taskStatus: Yup.object().required().shape({
    label: Yup.string().required(),
    value: Yup.number().required(),
  }),
  taskCollaborator: Yup.object().shape({
    label: Yup.string(),
    value: Yup.number(),
  }),
  taskTag: Yup.array().of(
    Yup.object().shape({
      label: Yup.string().required(),
      value: Yup.number().required(),
    })
  ),
})

export type BoardTaskFormikTypes = Yup.InferType<typeof formSchema>

type BoardTaskValidationTypes = {
  handleSubmit(values: BoardTaskFormikTypes): void
}

const boardTaskValidation = ({ handleSubmit }: BoardTaskValidationTypes) => {
  const [initialValues, setInitialValues] = useState<BoardTaskFormikTypes>({
    taskName: '',
    taskDescription: '',
    taskCollaborator: { label: undefined, value: undefined },
    taskStatus: { label: 'To Do', value: 1 },
    taskPriority: { label: 'Medium', value: 2 },
    taskTag: [],
  })

  const formik = useFormik({
    initialValues: initialValues,
    validationSchema: formSchema,
    onSubmit: (values) => {
      handleSubmit(values)
    },
  })

  const handleUpdateInitialValues = useCallback(
    (values: BoardTaskFormikTypes) => {
      setInitialValues(values)
    },
    []
  )

  return {
    formik,
    handleUpdateInitialValues,
  }
}

export default boardTaskValidation
