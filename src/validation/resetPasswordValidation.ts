import { useFormik } from 'formik'
import { useCallback, useState } from 'react'
import * as Yup from 'yup'

const formSchema = Yup.object().shape({
  password: Yup.string().required('Password is required').min(6).max(6),
  retypePassword: Yup.string()
    .oneOf([Yup.ref('password')], 'Passwords must match')
    .required('Password must be at least 6 characters'),
})

export type ResetPasswordFormikTypes = Yup.InferType<typeof formSchema>
type ResetPasswordValidationTypes = {
  handleSubmit(values: ResetPasswordFormikTypes): void
}

const resetPasswordValidation = ({
  handleSubmit,
}: ResetPasswordValidationTypes) => {
  const [initialValues, setInitialValues] = useState<ResetPasswordFormikTypes>({
    password: '',
    retypePassword: '',
  })

  const formik = useFormik({
    initialValues: initialValues,
    validationSchema: formSchema,
    onSubmit: (values) => {
      handleSubmit(values)
    },
  })

  const handleUpdateInitialValues = useCallback(
    (values: ResetPasswordFormikTypes) => {
      setInitialValues(values)
    },
    []
  )

  const passwordErrors =
    formik.errors.password && formik.touched.password
      ? formik.errors.password
      : ''

  const retypePasswordErrors =
    formik.errors.retypePassword && formik.touched.retypePassword
      ? formik.errors.retypePassword
      : ''

  return {
    formik,
    handleUpdateInitialValues,
    passwordErrors,
    retypePasswordErrors,
  }
}

export default resetPasswordValidation
