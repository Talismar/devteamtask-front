import { useFormik } from 'formik'
import { useState } from 'react'
import * as Yup from 'yup'

const formSchema = Yup.object().shape({
  taskName: Yup.string().required('Required.'),
  taskDescription: Yup.string(),
  addTaskIn: Yup.object()
    .shape({
      label: Yup.string().required(),
      value: Yup.number().required(),
    })
    .required(),
  taskPriority: Yup.object()
    .shape({
      label: Yup.string().required(),
      value: Yup.number().required(),
    })
    .required(),
  taskStatus: Yup.object().required().shape({
    label: Yup.string().required(),
    value: Yup.number().required(),
  }),
  taskCollaborator: Yup.object()
    .shape({
      label: Yup.string(),
      value: Yup.number(),
    })
    .nullable(),
  taskTag: Yup.array().of(
    Yup.object().shape({
      label: Yup.string().required(),
      value: Yup.number().required(),
    })
  ),
})

export type BacklogTaskFormikTypes = Yup.InferType<typeof formSchema>

type BacklogTaskValidationTypes = {
  handleSubmit(values: BacklogTaskFormikTypes): void
}

const backlogTaskValidation = ({
  handleSubmit,
}: BacklogTaskValidationTypes) => {
  const [initialValues, setInitialValues] = useState<BacklogTaskFormikTypes>({
    taskName: '',
    taskDescription: '',
    addTaskIn: { label: 'Product backlog', value: 2 },
    taskCollaborator: null,
    taskStatus: { label: '', value: 0 },
    taskPriority: { label: 'Medium', value: 2 },
    taskTag: [],
  })
  const [isUpdating, setIsUpdating] = useState(false)

  const formik = useFormik({
    initialValues: initialValues,
    validationSchema: formSchema,
    onSubmit: (values) => {
      handleSubmit(values)
      setIsUpdating(false)
    },
    onReset() {
      setIsUpdating(false)
    },
  })

  const handleUpdateInitialValues = (values: BacklogTaskFormikTypes) => {
    setInitialValues(values)
    formik.setValues(values)
    setIsUpdating(true)
  }

  return {
    formik,
    isUpdating,
    handleUpdateInitialValues,
  }
}

export default backlogTaskValidation
