import { useFormik } from 'formik'
import { useCallback, useState } from 'react'
import * as Yup from 'yup'

const formSchema = Yup.object().shape({
  old_password: Yup.string().min(6).max(8).required(),
  new_password: Yup.string().min(6).max(8).required(),
  confirm_password: Yup.string().oneOf(
    [Yup.ref('new_password')],
    'Passwords must match'
  ),
})

export type UserPasswordSettingsFormikTypes = Yup.InferType<typeof formSchema>

type UserPasswordSettingsFormValidationTypes = {
  handleSubmit(values: UserPasswordSettingsFormikTypes): void
}

const userPasswordSettingsFormValidation = ({
  handleSubmit,
}: UserPasswordSettingsFormValidationTypes) => {
  const [initialValues, setInitialValues] =
    useState<UserPasswordSettingsFormikTypes>({
      old_password: '',
      new_password: '',
      confirm_password: '',
    })

  const formik = useFormik({
    initialValues: initialValues,
    validationSchema: formSchema,
    onSubmit: (values) => {
      handleSubmit(values)
    },
  })

  const handleUpdateInitialValues = useCallback(
    (values: UserPasswordSettingsFormikTypes) => {
      setInitialValues(values)
      formik.setValues(values)
    },
    []
  )

  return {
    formik,
    handleUpdateInitialValues,
  }
}

export default userPasswordSettingsFormValidation
