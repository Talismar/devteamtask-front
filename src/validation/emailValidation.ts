import * as Yup from 'yup'

export default async function emailValidation(email: string) {
  const validation = Yup.string().email()

  try {
    await validation.validate(email)
    return true
  } catch (error) {
    if (error instanceof Yup.ValidationError) {
      return false
    }
  }
}
