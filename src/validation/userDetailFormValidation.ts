import { useFormik } from 'formik'
import { useCallback, useState } from 'react'
import * as Yup from 'yup'

const formSchema = Yup.object().shape({
  avatar_url: Yup.mixed().nullable(),
  phone_number: Yup.string(),
})

export type UserDetailFormFormikTypes = Omit<
  Yup.InferType<typeof formSchema>,
  'avatar_url'
> & { avatar_url: string | File | null | undefined }
type UserDetailFormValidationTypes = {
  handleSubmit(values: UserDetailFormFormikTypes): void
}

const userDetailFormValidation = ({
  handleSubmit,
}: UserDetailFormValidationTypes) => {
  const [initialValues, setInitialValues] = useState<UserDetailFormFormikTypes>(
    { avatar_url: '', phone_number: '' }
  )

  const formik = useFormik({
    initialValues: initialValues,
    validationSchema: formSchema,
    onSubmit: (values) => {
      if (JSON.stringify(values) !== JSON.stringify(formik.initialValues)) {
        handleSubmit(values)
      }
    },
  })

  const handleUpdateInitialValues = useCallback(
    (values: UserDetailFormFormikTypes) => {
      setInitialValues(values)
      formik.setValues(values)
    },
    []
  )

  return {
    formik,
    handleUpdateInitialValues,
  }
}

export default userDetailFormValidation
