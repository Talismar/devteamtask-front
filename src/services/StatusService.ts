/* eslint-disable @typescript-eslint/no-explicit-any */
import { GetServerSidePropsContext } from 'next'
import ApiService from './ApiService'

type CreateBodyTypes = {
  name: string
  order: number
  project_id: string
}

export class StatusService extends ApiService {
  constructor(context?: GetServerSidePropsContext) {
    super(context)
    this.endpoint = '/project/status'
  }

  getAll() {
    return this.get('')
  }

  getOne(id: string) {
    return this.get(`/${id}`)
  }

  create(data: CreateBodyTypes) {
    return this.post('', data)
  }

  update(statusId: string, data: any) {
    return this.patch(`/${statusId}`, data)
  }
}
