import { GetServerSidePropsContext } from 'next'
import ApiService from './ApiService'

type EventNoteBodyTypes = {
  planning?: string
  review?: string
  retrospective?: string
}

export class EventNoteService extends ApiService {
  constructor(context?: GetServerSidePropsContext) {
    super(context)
    this.endpoint = '/project/event_notes'
  }

  getAll() {
    return this.get('/')
  }

  getOne(id: string) {
    return this.get(`/${id}/`)
  }

  getBySprintId(sprint_id: string) {
    return this.get(`/${sprint_id}`)
  }

  update(id: number, data: EventNoteBodyTypes) {
    return this.patch(`/${id}/`, data)
  }
}
