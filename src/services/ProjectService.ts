/* eslint-disable @typescript-eslint/no-explicit-any */
import { GetServerSidePropsContext } from 'next'
import ApiService from './ApiService'

type ProjectCreateBodyTypes = {
  name: string
  end_date: Date
  product_owner_email?: string
  collaborators_email?: string[]
}

export class ProjectService extends ApiService {
  constructor(context?: GetServerSidePropsContext) {
    super(context)
    this.endpoint = '/project/project'
  }

  getAll() {
    return this.get('')
  }

  getOne(id: string) {
    return this.get(`/${id}`)
  }

  create(data: ProjectCreateBodyTypes) {
    return this.post('', data)
  }

  update(projectId: string, data: any) {
    return this.patch(`/${projectId}`, data, {
      headers: { 'Content-Type': 'multipart/form-data' },
    })
  }

  deleteProject(projectId: string) {
    return this.delete(`/${projectId}`)
  }

  removeProjectTagStatus(
    projectId: string,
    resource_name: 'Tag' | 'Status',
    resource_id: number
  ) {
    return this.delete(
      `/remove_tag_status/${projectId}?resource_name=${resource_name}&resource_id=${resource_id}`
    )
  }

  inviteCollaborators(data: any) {
    return this.post(`/invite_collaborators`, data)
  }
}
