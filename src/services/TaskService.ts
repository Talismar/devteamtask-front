/* eslint-disable @typescript-eslint/no-explicit-any */
import { TaskTypes } from '@/@types/tasks'
import { GetServerSidePropsContext } from 'next'
import ApiService from './ApiService'

type CreateTasksBodyTypes = Omit<Partial<TaskTypes>, 'tag' | 'status'> & {
  tags_ids: number[]
  status_id: number
  assigned_to_user_id?: number | null
}

export class TaksService extends ApiService {
  constructor(context?: GetServerSidePropsContext) {
    super(context)
    this.endpoint = '/project/task'
  }

  getAllByProjectIdWithFilter(projectId: string, taskName?: string) {
    if (typeof taskName === 'undefined') {
      return this.get(`/${projectId}`)
    }
    return this.get(`/${projectId}?task_name=${taskName}`)
  }

  getDashboardData() {
    return this.get('/dashboard_data')
  }

  getOne(id: string) {
    return this.get(`/${id}`)
  }

  create(data: CreateTasksBodyTypes) {
    return this.post('', data)
  }

  update(taskId: number, data: any) {
    return this.patch(`/${taskId}`, data)
  }

  deleteTask(taskId: number) {
    return this.delete(`/${taskId}`)
  }
}
