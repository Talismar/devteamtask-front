/* eslint-disable @typescript-eslint/no-explicit-any */
import { GetServerSidePropsContext } from 'next'
import ApiService from './ApiService'

type CreateTagBodyTypes = {
  name: string
  project_id: string
}

export class TagService extends ApiService {
  constructor(context?: GetServerSidePropsContext) {
    super(context)
    this.endpoint = '/project/tag'
  }

  getAll() {
    return this.get('/')
  }

  getOne(id: string) {
    return this.get(`/${id}/`)
  }

  create(data: CreateTagBodyTypes) {
    return this.post('/', data)
  }
}
