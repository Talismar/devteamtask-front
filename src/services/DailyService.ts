import { GetServerSidePropsContext } from 'next'
import ApiService from './ApiService'

type DailyBodyTypes = {
  id?: number
  note: string
  sprint_id?: number
}

export class DailyService extends ApiService {
  constructor(context?: GetServerSidePropsContext) {
    super(context)
    this.endpoint = '/project/daily'
  }

  getAll() {
    return this.get('')
  }

  getOne(id: string) {
    return this.get(`/${id}`)
  }

  getBySprintId(sprintId: number | string) {
    return this.get(`?sprint_id=${sprintId}`)
  }

  create(data: DailyBodyTypes) {
    return this.post('', data)
  }

  update(id: number, data: DailyBodyTypes) {
    return this.patch(`/${id}`, { note: data.note })
  }
}
