import { apiClient } from '@/libs/apiClient'
import { apiClient as axiosInstance } from '@/libs/api'
import { AxiosInstance, AxiosRequestConfig } from 'axios'
import { GetServerSidePropsContext } from 'next'

class ApiService {
  protected axiosInstance: AxiosInstance
  protected endpoint: string
  protected baseUrl: string

  constructor(context?: GetServerSidePropsContext) {
    this.baseUrl = process.env.NEXT_PUBLIC_DTT_BACKEND_URL
    this.axiosInstance =
      typeof context === 'undefined' ? apiClient : axiosInstance(context)
    this.endpoint = ''
  }

  fullUrl(url: string): string {
    return `${this.endpoint}${url}`
  }

  protected get(url: string, config?: AxiosRequestConfig) {
    const fullUrl = this.fullUrl(url)
    return this.axiosInstance.get(fullUrl, config)
  }

  protected post(url: string, data: any, config?: AxiosRequestConfig) {
    const fullUrl = this.fullUrl(url)
    return this.axiosInstance.post(fullUrl, data, config)
  }

  protected put<T>(url: string, data: T, config?: AxiosRequestConfig) {
    const fullUrl = this.fullUrl(url)
    return this.axiosInstance.put(fullUrl, data, config)
  }

  patch<T>(url: string, data: T, config?: AxiosRequestConfig) {
    const fullUrl = this.fullUrl(url)
    return this.axiosInstance.patch(fullUrl, data, config)
  }

  delete(url: string, config?: AxiosRequestConfig) {
    const fullUrl = this.fullUrl(url)
    return this.axiosInstance.delete(fullUrl, config)
  }
}

export default ApiService
