import axios from 'axios'
import { GetServerSidePropsContext } from 'next'
import ApiService from './ApiService'

type UserTypes = {
  id?: number
  name: string
  email: string
  password: string
  phone_number?: string
  avatar_url?: string | File | null
  notification_state?: boolean
  auth_provider?: string
  is_active?: boolean
}

export type ResetPasswordRequestBodyTypes = {
  email: string
}

export type ChangePasswordByTokenRequestBodyTypes = {
  invite_token: string
  password: string
  password_confirm: string
}

export class UserService extends ApiService {
  constructor(context?: GetServerSidePropsContext) {
    super(context)
    this.endpoint = '/user/user'
  }

  getMeInfo() {
    return this.get('/me/')
  }

  getOne(id: string) {
    return this.get(`/${id}/`)
  }

  create(data: any) {
    return axios.post(`${this.baseUrl}${this.endpoint}/`, data)
  }

  update(id: number, data: Partial<UserTypes>) {
    return this.patch(`/${id}`, data, {
      headers: { 'Content-Type': 'multipart/form-data' },
    })
  }

  deleteAccount(id: number) {
    return this.delete(`/${id}`)
  }

  changePassword(data: { old_password: string; new_password: string }) {
    return this.put(`/change_password`, data)
  }

  changePasswordByToken(data: ChangePasswordByTokenRequestBodyTypes) {
    return this.put('/reset_password_by_token', data, {
      headers: { Authorization: '' },
    })
  }

  resetPassword(data: ResetPasswordRequestBodyTypes) {
    return this.post(`/forgot_password`, data, {
      headers: { Authorization: '' },
    })
  }

  getPermanentTokenByEmail(email: string) {
    return this.axiosInstance.post('/auth/token/permanent/provider/', { email })
  }
}
