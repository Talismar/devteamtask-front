import { GetServerSidePropsContext } from 'next'
import ApiService from './ApiService'

export class NotificationService extends ApiService {
  constructor(context?: GetServerSidePropsContext) {
    super(context)
    this.endpoint = '/user/notification'
  }

  getAll() {
    return this.get('/')
  }

  markAsRead(id: number, data: any) {
    return this.patch(`/mark_as_read/${id}/`, data)
  }
}
