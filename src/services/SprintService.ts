/* eslint-disable @typescript-eslint/no-explicit-any */
import { GetServerSidePropsContext } from 'next'
import ApiService from './ApiService'

type CreateSprintBodyTypes = {
  name: string
  description: string
  project_id: string
}

export class SprintService extends ApiService {
  constructor(context?: GetServerSidePropsContext) {
    super(context)
    this.endpoint = '/project/sprint'
  }

  create(data: CreateSprintBodyTypes) {
    return this.post('', data)
  }

  update(id: number, data: any) {
    return this.patch(`/${id}`, data)
  }
}
