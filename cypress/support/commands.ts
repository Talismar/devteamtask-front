/// <reference types="cypress" />
import { faker } from '@faker-js/faker'

// -- This is a parent command --
Cypress.Commands.add('login', (email, password) => {
  cy.session(
    'login',
    () => {
      cy.visit('/login')

      cy.fixture('user').then((data) => {
        cy.get('#email').type(email ?? data.email)
        cy.get('#password').type(password ?? data.password)
        cy.get('button').contains('LOGIN').click()
      })
    },
    {
      validate: () => {
        cy.wait(1000)
        cy.getCookie('@dtt.accessToken').should('exist')
      },
      cacheAcrossSpecs: true,
    }
  )
})

Cypress.Commands.add('createProject', (name) => {
  cy.get('button').contains('New Project').click()

  cy.fixture('project').then((data) => {
    cy.get('#projectName').type(name || data.name)
    cy.get('#estimatedEndDate').type(
      faker.date.future().toISOString().split('T')[0]
    )
    cy.get('button').contains('Create project').click()

    cy.contains('Project created successfully.').should('be.visible')
  })
})

Cypress.Commands.add('createUser', (name, email, password) => {
  cy.fixture('user').then((data) => {
    const requestData = {
      name: name || data.name,
      email: email || data.email,
      password: password || data.password,
      password_confirm: password || data.password,
    }

    const BASE_URL = Cypress.env('NEXT_PUBLIC_DTT_BACKEND_URL')

    cy.request('POST', `${BASE_URL}/user/user/`, requestData).then(
      (response) => {
        expect(response.status).equal(201)
      }
    )
  })
})

Cypress.Commands.add(
  'createTaskWithRequest',
  (headers, project_id, sprint_id, name) => {
    cy.fixture('user').then((data) => {
      const requestData = {
        name: name || data.name,
        description: faker.word.sample(60),
        priority: 1,
        status_id: 1,
        sprint_id: sprint_id,
        tags_ids: [],
        assigned_to_user_id: 1,
        project_id: project_id,
      }

      const BASE_URL = Cypress.env('NEXT_PUBLIC_DTT_BACKEND_URL')

      cy.request({
        method: 'POST',
        url: `${BASE_URL}/project/task`,
        body: requestData,
        headers: headers,
      }).then((response) => {
        // expect(response.status).equal(201)
      })
    })
  }
)

Cypress.Commands.add('createSprintWithRequest', (headers, project_id, name) => {
  const requestData = {
    name: name,
    description: faker.word.sample(60),
    project_id: project_id,
  }

  const BASE_URL = Cypress.env('NEXT_PUBLIC_DTT_BACKEND_URL')

  return cy.request({
    method: 'POST',
    url: `${BASE_URL}/project/sprint`,
    body: requestData,
    headers: headers,
  })
})

// -- This will overwrite an existing command --
// Cypress.Commands.overwrite('visit', (originalFn, url, options) => { ... })
//
// declare global {
//   namespace Cypress {
//     interface Chainable {
//       login(email: string, password: string): Chainable<void>
//       drag(subject: string, options?: Partial<TypeOptions>): Chainable<Element>
//       dismiss(subject: string, options?: Partial<TypeOptions>): Chainable<Element>
//       visit(originalFn: CommandOriginalFn, url: string, options: Partial<VisitOptions>): Chainable<Element>
//     }
//   }
// }
