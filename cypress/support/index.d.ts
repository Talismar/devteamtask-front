/* eslint-disable prettier/prettier */
/// <reference types="cypress" />

declare namespace Cypress {
  interface Chainable {
    login(email?: string, password?: string): Chainable<void>
    createProject(name?: string): Chainable<void>
    createUser(
      name?: string,
      email?: string,
      password?: string
    ): Chainable<void>
    createTaskWithRequest(
      headers: object,
      project_id: string,
      sprint_id?: number,
      name?: string
    ): Chainable<void>
    createSprintWithRequest(
      headers: object,
      project_id: string,
      name: string
    ): Chainable<Cypress.Response<any>>
    task(
      event: 'reset_database_by_service',
      arg?: 'PROJECT' | 'USER',
      options?: Partial<Cypress.Loggable & Cypress.Timeoutable> | undefined
    ): Cypress.Chainable<unknown>
  }
}
