import { faker } from '@faker-js/faker'

describe('Project events notes', () => {
  beforeEach(() => {
    cy.task('reset_database_by_service', 'USER')
    cy.task('reset_database_by_service', 'PROJECT')

    cy.createUser()
    cy.login()
    cy.visit('/projects')
  })

  it('Deve ser possivel criar uma notas de eventos do tipo planning', () => {
    cy.intercept('POST', '**/project/project').as('projectRequest')
    cy.createProject()

    cy.wait('@projectRequest').then(({ response, request }) => {
      const headers = { authorization: request.headers.authorization }
      const project_id = response?.body.id

      cy.createSprintWithRequest(headers, project_id, 'Sprint 1').then(
        (response) => {
          const sprint_id = response.body.id
          cy.visit(
            '/projects/' +
              project_id +
              '/event-notes/planning?sprint_id=' +
              sprint_id
          )
        }
      )

      cy.get('textarea').type(faker.lorem.sentence(10))
      cy.get('button').contains('Save note').click()
      cy.contains('successfully updated').should('be.visible')
    })
  })

  it.only('Deve ser possivel criar eventos do tipo daily e visualizar a lista de notas', () => {
    cy.intercept('POST', '**/project/project').as('projectRequest')
    cy.createProject()

    cy.wait('@projectRequest').then(({ response, request }) => {
      const headers = { authorization: request.headers.authorization }
      const project_id = response?.body.id

      cy.createSprintWithRequest(headers, project_id, 'Sprint 1').then(
        (response) => {
          const sprint_id = response.body.id
          cy.visit(
            '/projects/' +
              project_id +
              '/event-notes/daily?sprint_id=' +
              sprint_id
          )
        }
      )

      cy.get('button').contains('New note').click()
      cy.contains('New note successfully created').should('be.visible')

      cy.get('textarea', { timeout: 5000 }).type(faker.lorem.sentence(10))
      cy.get('button').contains('Save note').click()

      cy.contains('successfully updated').should('be.visible')

      cy.go('back')
      cy.wait(2000)
      cy.get('header:last').next().should('have.length', 1)
    })
  })
})
