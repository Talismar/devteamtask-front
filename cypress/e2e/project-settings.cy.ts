describe('Project settings', () => {
  it('Deve ser possivel atualizar a logo do projeto', () => {
    cy.task('reset_database_by_service', 'PROJECT')
    cy.task('reset_database_by_service', 'USER')

    cy.createUser()
    cy.login()
    cy.visit('/projects')

    cy.createProject('Project 1')

    cy.get('table > tbody > tr > td:last > svg:last').click()

    cy.get('label[for=logo_url]', { timeout: 5000 }).selectFile(
      'cypress/fixtures/devTeamTaskLogo.png'
    )

    cy.get('button').contains('Save').click()

    cy.contains('Project updated successfully')
    cy.get('label[for=logo_url]').parent().find('img').should('be.visible')
  })

  it('Deve ser possivel criar uma a tag', () => {
    cy.task('reset_database_by_service', 'PROJECT')
    cy.task('reset_database_by_service', 'USER')

    cy.createUser()
    cy.login()
    cy.visit('/projects')

    cy.createProject('Project 1')

    cy.get('table > tbody > tr > td:last > svg:last').click()

    cy.get('#tag').type('User')
    cy.get('button').contains('New Tag').click()

    cy.contains('Tag created successfully').should('be.visible')
  })

  it.only('Deve ser possivel remover uma a tag', () => {
    cy.task('reset_database_by_service', 'PROJECT')
    cy.task('reset_database_by_service', 'USER')

    cy.createUser()
    cy.login()
    cy.visit('/projects')

    cy.createProject('Project 1')

    cy.get('table > tbody > tr > td:last > svg:last').click()

    cy.get('#tag').type('User')
    cy.get('button').contains('New Tag').click()

    cy.get('label[for="tag"]').find('svg').click()
    cy.contains('Tag deleted successfully').should('be.visible')
  })
})
