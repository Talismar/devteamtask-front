describe('Signup', () => {
  beforeEach(() => {
    cy.task('reset_database_by_service', 'USER')
    cy.clearAllSessionStorage()
    cy.visit('/signup')
  })

  it('Deve ser possivel cadastrar novos usuarios no sistema', () => {
    const endpoint = Cypress.env('NEXT_PUBLIC_DTT_BACKEND_URL') + '/user/user'

    cy.fixture('user.json').then((file) => {
      cy.intercept('POST', endpoint).as('UserCreate')

      cy.get('#fullName').type(file.name)
      cy.get('#email').type(file.email)
      cy.get('button').contains('Continue', { matchCase: false }).click()

      cy.get('#password').type(file.password)
      cy.get('#confirmPassword').type(file.password)

      cy.get('button').contains('Register Now', { matchCase: false }).click()

      cy.wait('@UserCreate').then(({ response }) => {
        expect(response?.statusCode).equal(201)
        expect(response?.body).to.have.property('name')
        expect(response?.body).to.have.property('email')
      })
    })
  })

  it('Deve logar o usuário após o cadastro no sistema', () => {
    const endpoint =
      Cypress.env('NEXT_PUBLIC_DTT_BACKEND_URL') + '/user/authentication/token'

    cy.fixture('user.json').then((file) => {
      cy.intercept('POST', endpoint).as('UserAuthentication')

      cy.get('#fullName').type(file.name)
      cy.get('#email').type(file.email)
      cy.get('button').contains('Continue', { matchCase: false }).click()

      cy.get('#password').type(file.password)
      cy.get('#confirmPassword').type(file.password)

      cy.get('button').contains('Register Now', { matchCase: false }).click()

      cy.wait('@UserAuthentication').then(({ response }) => {
        expect(response?.statusCode).equal(200)
      })

      cy.url().should('include', 'dashboard')
    })
  })

  it('Deve exibir uma mensagem de alerta caso já existe um usuário cadastro com o mesmo email', () => {
    cy.createUser()

    cy.fixture('user.json').then((file) => {
      cy.get('#fullName').type(file.name)
      cy.get('#email').type(file.email)
      cy.get('button').contains('Continue', { matchCase: false }).click()
      cy.get('#password').type(file.password)
      cy.get('#confirmPassword').type(file.password)

      cy.get('button').contains('Register Now', { matchCase: false }).click()
    })

    cy.contains('Account already exists').should('be.visible')
  })
})
