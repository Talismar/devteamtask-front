describe('Project list', () => {
  beforeEach(() => {
    cy.task('reset_database_by_service', 'PROJECT')
    cy.task('reset_database_by_service', 'USER')

    cy.createUser()
    cy.login()
    cy.visit('/projects')

    cy.createProject('Project 1')
  })

  it('Deve ser possivel obter a lista de projetos', () => {
    cy.createProject('Project 2')
    cy.wait(2000)
    cy.createProject('Project 3')

    cy.get('table > tbody > tr').should(($lis) => {
      expect($lis).to.have.length(3)

      expect($lis.eq(0)).to.contain('Project 1')
      expect($lis.eq(1)).to.contain('Project 2')
      expect($lis.eq(2)).to.contain('Project 3')
    })
  })

  it('Deve ser criar um novo projeto', () => {
    cy.contains('Project created successfully')

    cy.get('table > tbody > tr').should(($lis) => {
      expect($lis).to.have.length(1)
      expect($lis.eq(0)).to.contain('Project 1')
      expect($lis.eq(0)).to.contain('No Sprint Started')
    })

    cy.contains('Project created successfully')
  })

  it('Deve ser possivel deletar um projeto', () => {
    cy.contains('Project created successfully').should('be.visible')

    cy.get('table > tbody > tr > td:last-child > svg:first').click()
    cy.get('button').contains('Yes').click()
    cy.contains('Project deleted successfully').should('be.visible')
  })

  it('Deve ser possivel ir para tela de atualizar projeto e atualizar o nome', () => {
    cy.contains('Project created successfully').should('be.visible')

    cy.get('table > tbody > tr > td:last > svg:last').click()

    cy.get('#name', { timeout: 10000 }).clear().type('Project name update')
    cy.get('button').contains('Save Changes').click()
    cy.contains('Project updated successfully').should('be.visible')

    cy.go('back')
    cy.get('table > tbody > tr > td:nth-of-type(2) > a')
      .contains('Project name update')
      .should('be.visible')
  })
})
