describe('Project board', () => {
  it('Deve ser possivel deletar uma tarefa', () => {
    cy.task('reset_database_by_service', 'PROJECT')
    cy.task('reset_database_by_service', 'USER')

    cy.createUser()
    cy.login()
    cy.visit('/projects')

    cy.intercept('POST', '**/project/project').as('projectRequest')
    cy.createProject()

    cy.wait('@projectRequest').then(({ response, request }) => {
      const headers = { authorization: request.headers.authorization }
      const project_id = response?.body.id

      cy.createSprintWithRequest(headers, project_id, 'Sprint 1').then(
        (response) => {
          const sprint_id = response.body.id

          cy.createTaskWithRequest(headers, project_id, sprint_id)
        }
      )

      cy.visit('/projects/' + response?.body.id + '/boards')
      cy.get('#task-1').find('svg:first').click()
      cy.contains('Delete Task')
        .parent()
        .parent()
        .parent()
        .find('button')
        .contains('Yes')
        .click()

      cy.get('#task-1').should('not.exist')
      cy.contains('Task deleted successfully').should('be.visible')
    })
  })

  it('Deve ser possivel criar um novo status', () => {
    cy.task('reset_database_by_service', 'PROJECT')
    cy.task('reset_database_by_service', 'USER')

    cy.createUser()
    cy.login()
    cy.visit('/projects')

    cy.intercept('POST', '**/project/project').as('projectRequest')
    cy.createProject()

    cy.wait('@projectRequest').then(({ response, request }) => {
      const headers = { authorization: request.headers.authorization }
      const project_id = response?.body.id

      cy.createSprintWithRequest(headers, project_id, 'Sprint 1').then(
        (response) => {
          const sprint_id = response.body.id

          cy.createTaskWithRequest(headers, project_id, sprint_id)
        }
      )

      cy.visit('/projects/' + response?.body.id + '/boards')
    })

    cy.get('button').contains('New Board').click()
    cy.get('input[id=statusName]').type('Test')
    cy.get('button').contains('Create board').click()

    cy.contains('New board successfully added', { matchCase: false }).should(
      'be.visible'
    )

    cy.get('[id^=wrapper-status-]')
      .should('have.length', 4)
      .each(($element, index) => {
        if (index === 3) cy.wrap($element).invoke('text').should('eq', 'Test')
      })
  })

  it.only('Deve ser possivel buscar uma tarefa pelo nome', () => {
    cy.task('reset_database_by_service', 'PROJECT')
    cy.task('reset_database_by_service', 'USER')

    cy.createUser()
    cy.login()
    cy.visit('/projects')

    cy.intercept('POST', '**/project/project').as('projectRequest')
    cy.createProject()

    cy.wait('@projectRequest').then(({ response, request }) => {
      const headers = { authorization: request.headers.authorization }
      const project_id = response?.body.id

      cy.createSprintWithRequest(headers, project_id, 'Sprint 1').then(
        (response) => {
          const sprint_id = response.body.id

          cy.createTaskWithRequest(headers, project_id, sprint_id)
          cy.createTaskWithRequest(
            headers,
            project_id,
            sprint_id,
            'Task to searching'
          )
        }
      )

      cy.visit('/projects/' + response?.body.id + '/boards')
    })

    cy.get('input[placeholder="Search by task name..."]')
      .type('Task to searching')
      .next()
      .click()

    cy.get('[id^=task-]').should('have.length', 1)
  })
})
