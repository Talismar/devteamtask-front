describe('User settings', () => {
  beforeEach(() => {
    cy.task('reset_database_by_service', 'USER')
    cy.createUser()
    cy.login()
    cy.visit('/user/settings')
  })

  it('Deve ser possivel atualizar o avatar do usuario autenticado', () => {
    cy.wait(2000)
    const endpoint = Cypress.env('NEXT_PUBLIC_DTT_BACKEND_URL') + '/user/user/1'

    cy.intercept('PATCH', endpoint).as('UserUpdate')

    cy.get('input[type=file]')
      .prev()
      .selectFile('cypress/fixtures/MyAvatar.png')

    cy.get('button').contains('Save').click()

    cy.wait('@UserUpdate').then(({ response }) => {
      expect(response?.statusCode).to.equal(200)
      expect(response?.body.avatar_url).to.contain('MyAvatar.png')
    })

    cy.get('label[for=avatar_url]').parent().find('img').should('be.visible')
  })

  it('Deve ser possivel alterar a senha do usuário logado', () => {
    const endpoint =
      Cypress.env('NEXT_PUBLIC_DTT_BACKEND_URL') + '/user/user/change_password'

    cy.intercept('PUT', endpoint).as('UserChangePassword')

    cy.contains('Password Settings').click()

    cy.fixture('user.json').then((data) => {
      cy.get('#old_password').type(data.password)
    })

    cy.get('#new_password').type('change')
    cy.get('#confirm_password').type('change')

    cy.get('button').contains('Save').click()

    cy.wait('@UserChangePassword').then(({ response }) => {
      expect(response?.statusCode).equal(200)
    })

    cy.contains('Password updated successfully').should('be.visible')
  })

  it('Deve ser possivel mudar o estado da notificação de um usuário', () => {
    cy.wait(1000)

    cy.contains('Notification Settings').click()
    cy.contains('General email notifications').parent().find('button').click()
    cy.contains('Save Changes').click()

    cy.contains('Successfully saved changes').should('be.visible')
  })
})
