describe('Dashboard', () => {
  it('Validar se os dados do dashboard estão corretos', () => {
    cy.task('reset_database_by_service', 'PROJECT')
    cy.task('reset_database_by_service', 'USER')

    cy.createUser()
    cy.login()
    cy.visit('/projects')

    cy.intercept('POST', '**/project/project').as('projectRequest')

    cy.createProject()

    cy.wait('@projectRequest').then(({ response, request }) => {
      const headers = { authorization: request.headers.authorization }
      const project_id = response?.body.id

      cy.createSprintWithRequest(headers, project_id, 'Sprint 1').then(
        (response) => {
          const sprint_id = response.body.id

          cy.createTaskWithRequest(headers, project_id, sprint_id)
        }
      )

      cy.visit('/dashboard')
    })

    cy.contains('Completed Tasks')
      .parent()
      .parent()
      .contains('0')
      .should('be.visible')
    cy.contains('Assigned Tasks')
      .parent()
      .parent()
      .contains('1')
      .should('be.visible')
    cy.contains('Scheduled Tasks')
      .parent()
      .parent()
      .contains('0')
      .should('be.visible')
  })
})
