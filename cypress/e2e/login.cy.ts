import { faker } from '@faker-js/faker'

describe('Login', () => {
  it('Usuário com registro no sistema deve poder efetuar login', () => {
    cy.task('reset_database_by_service', 'USER')

    const endpoint =
      Cypress.env('NEXT_PUBLIC_DTT_BACKEND_URL') + '/user/authentication/token'

    cy.createUser('Talismar', 'talismar788.una@gmail.com', 'asdasd')
    cy.visit('/login')

    cy.clearAllSessionStorage()

    cy.fixture('user.json').then((file) => {
      cy.intercept('POST', endpoint).as('UserAuthentication')

      cy.get('input[name=email]').type(file.email)
      cy.get('input[name=password]').type(file.password)
      cy.get('button').contains('login', { matchCase: false }).click()

      cy.wait('@UserAuthentication').then(({ response }) => {
        expect(response?.statusCode).equal(200)
        expect(response?.body).to.have.property('access_token')
        expect(response?.body).to.have.property('refresh_token')
        expect(response?.body).to.have.property('token_type')
      })
    })
  })

  it('Usuário sem registro no sistema não deve poder efetuar login', () => {
    const endpoint =
      Cypress.env('NEXT_PUBLIC_DTT_BACKEND_URL') + '/user/authentication/token'

    cy.clearAllSessionStorage()

    cy.visit('/login')

    cy.intercept('POST', endpoint).as('UserAuthentication')

    cy.get('input[name=email]').type(faker.internet.email())
    cy.get('input[name=password]').type('fakerpa')
    cy.get('button').contains('login', { matchCase: false }).click()

    cy.wait('@UserAuthentication').then(({ response }) => {
      expect(response?.statusCode).equal(401)
      cy.contains('Account already exists')
    })
  })
})
