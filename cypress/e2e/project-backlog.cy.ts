import { faker } from '@faker-js/faker'

describe('Project backlog', () => {
  it('Deve ser possivel criar uma tarefa e adicionar a uma sprint', () => {
    cy.task('reset_database_by_service', 'PROJECT')
    cy.task('reset_database_by_service', 'USER')

    cy.createUser()
    cy.login()
    cy.visit('/projects')

    cy.intercept('POST', '**/project/project').as('projectRequest')
    cy.createProject()

    // cy.get('table > tbody > tr > td:first-child > a').click()

    cy.wait('@projectRequest').then(({ response, request }) => {
      const headers = { authorization: request.headers.authorization }
      const project_id = response?.body.id

      cy.createSprintWithRequest(headers, project_id, 'Sprint 1').then(
        (response) => {
          const sprint_id = response.body.id

          cy.createTaskWithRequest(headers, project_id, sprint_id)
        }
      )

      cy.visit('/projects/' + response?.body.id + '/backlog')

      cy.get('table:first > tbody > tr').should('have.length', 1)
    })
  })

  it('Deve ser possivel criar uma sprint', () => {
    cy.task('reset_database_by_service', 'PROJECT')
    cy.task('reset_database_by_service', 'USER')

    cy.createUser()
    cy.login()
    cy.visit('/projects')

    cy.intercept('POST', '**/project/project').as('projectRequest')
    cy.createProject()

    cy.wait('@projectRequest').then(({ response }) => {
      cy.visit('/projects/' + response?.body.id + '/backlog')

      cy.get('button').contains('Start Sprint').click()
      cy.get('#name').type(faker.word.words())
      cy.get('#description').type(faker.word.words(10))
      cy.get('button').contains('Create sprint').click()
      cy.contains('Sprint created successfully').should('be.visible')
    })
  })
})
